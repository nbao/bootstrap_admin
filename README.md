Make project Drupal 7

mkdir sites/all/modules/contrib

mkdir sites/all/modules/custom

drush en -y php comment update locale   admin_menu_toolbar admin_menu addressfield addressfield_phone multiupload_filefield_widget mailsystem email swiftmailer calendar field_collection field_group ctools feeds entity libraries views views_ui date_views field_collection_views  field_hidden views_between_dates_filter views_bulk_operations views_php views_aggregator double_field work_calendar porterstemmer tablefield block_class entityreference_filter feeds_ui feeds_tamper feeds_tamper_ui field_collection_feeds administerusersbyrole entity_token icon_menu icon path_breadcrumbs_ui path_breadcrumbs link r4032login job_scheduler entity_dependency ds ds_forms ds_ui fontawesome autocomplete_widgets markup  user_reference node_reference references datatables  select_or_other entityreference_view_widget pathauto moment user_dashboard field_group_table  better_exposed_filters unique_field tablefield_chart ckeditor bdtpicker  field_permissions views_bootstrap bootstrap_fieldgroup schema date_api date phone  data data_ui feeds_data cshs  jquery_update login_emailusername content_access  views_simplechart  chain_menu_access masquerade name ocupload entityreference file_entity  views_conditional  media_wysiwyg media conditional_fields  date_ical charts_google charts_highcharts charts  field_group_multiple field_collection_table  views_data_export views_data_export_phpspreadsheet  token  field_collection_datatables fc_tabs 

optional 

drush en -y redis bootstrap clone password_policy views_gantt devel quick_data taxonomy_manager leaflet leaflet_views leaflet_markercluster geolocation geofield geophp geolocation_proximity ip_geoloc geolocation_osm countries countryicons_shiny countryicons shortcode shortcode_bootstrap sign_widget 

drush dis -y overlay help comment toolbar shortcut

drush vset theme_default bootstrap_admin
drush vset admin_theme bootstrap_admin


—————————------- Download Libraries —————————-------

curl -O https://fontawesome.com/v4.7.0/assets/font-awesome-4.7.0.zip

unzip font-awesome-4.7.0.zip -d sites/all/libraries/

mv sites/all/libraries/font-awesome-4.7.0 sites/all/libraries/fontawesome

rm -f font-awesome-4.7.0.zip


curl -O https://datatables.net/download/builder?bs-3.3.7/jszip-2.5.0/pdfmake-0.1.32/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5

unzip sl-1.2.5 -d sites/all/libraries/datatables

rm -f sl-1.2.5

curl -O https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.8.0/ckeditor_4.8.0_standard.zip

unzip ckeditor_4.8.0_standard.zip -d sites/all/libraries/

rm -f ckeditor_4.8.0_standard.zip

(function ($, Drupal) {
  /*global jQuery:false */
  /*global Drupal:false */
  "use strict";

  /**
   * Fixed menu views add. copy from views-admin.js
   */
  delete Drupal.behaviors.viewsUiRenderAddViewButton;

  Drupal.behaviors.viewsUiRenderAddViewButton = {};
  Drupal.behaviors.viewsPHPVariables = {};
  Drupal.behaviors.viewsUiRenderAddViewButton.attach = function (context, settings) {
    var $ = jQuery;
    // Build the add display menu and pull the display input buttons into it.
    var $menu = $('#views-display-menu-tabs', context).once('views-ui-render-add-view-button-processed');

    if (!$menu.length) {
      return;
    }
    var $addDisplayDropdown = $('<li class="add btn-group btn-group-xs"><button type="submit" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><span class="icon add glyphicon glyphicon-plus"></span>' + Drupal.t('Add') + ' <span class="caret"></span></button><ul class="action-list dropdown-menu"  role="menu"></ul></li>');

    var $displayButtons = $menu.nextAll('.add-display').detach();
    $displayButtons.appendTo($addDisplayDropdown.find('.dropdown-menu')).wrap('<li>')
      .parent().first().addClass('first').end().last().addClass('last');
    // Remove the 'Add ' prefix from the button labels since they're being palced
    // in an 'Add' dropdown.
    // @todo This assumes English, but so does $addDisplayDropdown above. Add
    //   support for translation.
    $displayButtons.each(function () {
      $(this)
        .addClass('btn-sm')
        .addClass('btn-block');
      var label = $(this).val();
      if (label.substr(0, 4) == 'Add ') {
        $(this).val(label.substr(4));
      }
    });
    $addDisplayDropdown.appendTo($menu);

    $('.views-ui-render-add-view-button-processed-processed').addClass('pull-left');

    // Add the click handler for the add display button
    $('li.add > a', $menu).bind('click', function (event) {
      event.preventDefault();
      var $trigger = $(this);
      Drupal.behaviors.viewsUiRenderAddViewButton.toggleMenu($trigger);
    });
    // Add a mouseleave handler to close the dropdown when the user mouses
    // away from the item. We use mouseleave instead of mouseout because
    // the user is going to trigger mouseout when she moves from the trigger
    // link to the sub menu items.
    //
    // We use the 'li.add' selector because the open class on this item will be
    // toggled on and off and we want the handler to take effect in the cases
    // that the class is present, but not when it isn't.
    $menu.delegate('li.add', 'mouseleave', function (event) {
      var $this = $(this);
      var $trigger = $this.children('a[href="#"]');
      if ($this.children('.action-list').is(':visible')) {
        Drupal.behaviors.viewsUiRenderAddViewButton.toggleMenu($trigger);
      }
    });
  };
  Drupal.behaviors.BefFix = {
    attach: function (context, settings) {
      var views_exposed_form = $('.views-exposed-form', context);
      views_exposed_form.hide();
      views_exposed_form.find('.form-control').not('.form-text, .form-select').removeClass('form-control');
      views_exposed_form.find('.bef-tree').addClass('list-unstyled');
      views_exposed_form.show();
    }
  };
})(jQuery, Drupal);

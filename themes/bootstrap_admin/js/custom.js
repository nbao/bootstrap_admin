(function ($, Drupal) {
  $(document).ready(function () {
    $(".view-id-annuaire td.views-field-field-tel-portable").each(function (index) {
      var phone = $.trim($(this).text());
      if (phone != "") {
        $(this).html("<a href='tel:" + phone + "'><i class='glyphicon glyphicon-phone'></i> " + phone + "</a>");
      }
    });
    $(".field-type-number-decimal input.form-text").bind("keyup change", function () {
      $(this).val($(this).val().replace(",", ".").replace(" ", ""));
    });

    /* view carte */
    $(".view-id-projets.view-display-id-map .view-content:first").addClass("col-md-9");
    $(".view-id-projets.view-display-id-map .attachment-after").addClass("col-md-3");

    $(".view-id-projets.view-display-id-part_eolfi .view-content:first").addClass("col-md-9");
    $(".view-id-projets.view-display-id-part_eolfi .attachment-after").addClass("col-md-3");

  });
})(jQuery, Drupal);

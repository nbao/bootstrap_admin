<?php
/**
 * Implements hook_views_data_export_feed_icon().
 */
function bootstrap_admin_views_data_export_feed_icon($variables)
{
  extract($variables, EXTR_SKIP);
  $url_options = array('html' => TRUE, 'attributes' => array('class' => 'btn btn-danger', 'target' => '_blank'));
  if (!empty($query)) {
    $url_options['query'] = $query;
  }
  switch (strtolower($text)) {
    case 'csv':
      $text = '<i class="fa fa-table"></i> ' . $text;
      break;
    case 'xls':
      $text = '<i class="fa fa-file-excel-o"></i> ' . $text;
      break;
    case 'txt':
    case 'plain text document':
      $text = '<i class="fa fa-file-text"></i> ' . $text;
      break;
    case 'doc':
    case 'word document':
      $text = '<i class="fa fa-file-word-o"></i> ' . $text;
      break;
  }
  return l($text, $url, $url_options);
}

/*
 * implement hook_textfield
 */
function bootstrap_admin_textfield($variables)
{
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array('id', 'name', 'value', 'maxlength'));
  _form_set_class($element, array('form-text'));

  $extra = '';
  if ($element['#autocomplete_path'] && !empty($element['#autocomplete_input'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#autocomplete_input']['#id'];
    $attributes['value'] = $element['#autocomplete_input']['#url_value'];
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}
function bootstrap_admin_preprocess_node(&$vars) {
  if($vars['view_mode'] == 'teaser') {
    array_unshift($vars['theme_hook_suggestions'], 'node__' . $vars['node']->type . '__teaser');
    array_unshift($vars['theme_hook_suggestions'], 'node__' . $vars['node']->nid . '__teaser');
  }
}

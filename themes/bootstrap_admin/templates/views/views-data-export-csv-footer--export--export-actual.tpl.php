<?php
$separator = "|";
foreach (['psf', 'supplementaire'] as $export_view_id) {
  $view_result = views_get_view_result('export', $export_view_id);
  if (!empty($view_result)) {
    foreach ($view_result as $result) {
      $paiement_date = NULL;
      $range = 1;
      if (!empty($result->field_field_paiement_date)) {
        $paiement_date = strtotime($result->field_field_paiement_date[0]["raw"]["value"]);
        $range = ceil(date('n', $paiement_date) / 3);
      }
      $category_name = $export_view_id == 'salaires' ? 'HR & Communication' . $separator . 'Salaires' : drupal_render($result->field_field_category[0]["rendered"]);
      if (!empty($result->field_field_code_tresor[0]["raw"]["taxonomy_term"]->field_tags["und"][0]["tid"]) && empty($category_name)) {
        $nature = taxonomy_term_load($result->field_field_code_tresor[0]["raw"]["taxonomy_term"]->field_tags["und"][0]["tid"]);
        $parent = current(taxonomy_get_parents($nature->tid));
        $cat = [];
        if (!empty($parent)) {
          $cat[] = $parent->name;
        }
        $cat[] = $nature->name;
        $category_name = implode($separator, $cat);
      }
      $negative = erp_rule_amount($result->_field_data["field_paiement_lignes_field_collection_item_nid"]["entity"]);
      $montant = $negative * abs($result->field_field_montant[0]["raw"]["value"]);
      $item_row = [
        'codeprojet' => NULL,
        'nomprojet' => NULL,
        'societe' => !empty($result->field_field_paiement_site) ? $result->field_field_paiement_site[0]["raw"]["entity"]->name : NULL,
        'fournisseur' => !empty($result->field_field_compte_beneficiaire) ? $result->field_field_compte_beneficiaire[0]["raw"]["value"] : NULL,
        'Categorie' => html_entity_decode($category_name),
        'Annee' => !empty($paiement_date) ? date('Y', $paiement_date) : NULL,
        'Trimestre' => 'q' . $range,
        'NumInvoice' => !empty($result->field_paiement_lignes_field_collection_item_title) ? $result->field_paiement_lignes_field_collection_item_title : NULL,
        'DateComptable' => !empty($paiement_date) ? date('d/m/Y', $paiement_date) : NULL,
        'Montant' => $montant,
        'Exercice' => !empty($paiement_date) ? date('Y', $paiement_date) : NULL,
        'Version' => 'Actual',
        'GROUPE_CAT' => 'OFFICE MANAGER',
        'SUPRA' => NULL,
        'ACTIVITE' => NULL,
        'ChefProjet' => NULL,
      ];
      print implode($options["separator"], $item_row) . "\r\n";
    }
  }
}

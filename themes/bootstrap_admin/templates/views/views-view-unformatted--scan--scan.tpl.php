<?php

$name_dir_scan = '/scans/';
$dir = drupal_realpath('public://') . $name_dir_scan;
?>
<form enctype="multipart/form-data" method="post" action="/scans/files">
  <?php
  $form['file'] = array(
    '#type' => 'file',
    '#name' => 'files[]',
    '#id' => 'scans',
    '#title' => t('Upload facture'),
    '#attributes' => array('multiple' => 'multiple'),
  );
  drupal_add_css(libraries_get_path('bootstrap-datetimepicker') . '/build/css/bootstrap-datetimepicker.css');
  drupal_add_js(libraries_get_path('moment') . '/min/moment-with-locales.min.js');
  drupal_add_js(libraries_get_path('bootstrap-datetimepicker') . '/build/js/bootstrap-datetimepicker.min.js');
  echo render($form);
  ?>
</form>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<form class="row" method="post" action="/scans/files">
  <div class="col-md-4">
    <?php foreach ($rows as $id => $row): ?>
      <div<?php if ($classes_array[$id]): ?> class="<?php print $classes_array[$id]; ?>"<?php endif; ?>>
        <?php print $row; ?>
      </div>
    <?php endforeach; ?>
  </div>
  <div class="col-md-8">
    <p class="thumbnail droppable ui-droppable"><i class="glyphicon glyphicon-trash" style="font-size:30px"></i> <input
        type="hidden" name="delete"></p>
    <?php
    $listeScans = glob($dir . "*.{jpg,jpeg,jpe,png,gif,tif,JPG,JPEG,JPE,PNG,GIF,TIF}", GLOB_BRACE);
    foreach ($listeScans as $file) {
      $file = str_replace($dir, '', $file);
      $variables = [
        'path' => 'public:/' . $name_dir_scan . $file,
        'attributes' => [
          'width' => 400
        ]
      ];
      $img = theme_image($variables);
      $items[] = ['data' => '<span class="caption">' . $file . $img . '</span>', 'class' => ['thumbnail', 'draggable', 'text-center']];
    }
    $attributes = ['id' => 'list-files'];
    echo !empty($items) ? theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ul', 'attributes' => $attributes)) : '';
    ?>
  </div>
  <?php if (!empty($items)): ?>
    <div class="col-md-8">
      <button class="btn btn-success" name="op" value="save"><i class="glyphicon glyphicon-floppy-saved"></i>
        Enregistrer
      </button>
    </div>
  <?php endif; ?>
</form>

<style type="text/css">
  .draggable:hover {
    cursor: move;
    position: relative;
  }

  .no-drop:hover {
    cursor: no-drop;
  }

  .no-drop {
    background: #eee;
  }

  .caption img {
    position: absolute;
    left: 50%;
    transform: translate(-50%);
    bottom: 2em;
    opacity: 0;
    pointer-events: none;
    transition-duration: 800ms;
    z-index: 1000;
  }
  li.thumbnail:hover img,
  .caption:hover > img {
    opacity: 1;
    transition-duration: 400ms;
  }

  .thumbnail.droppable .glyphicon-trash {
    cursor: pointer;
  }
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" media="all"
      rel="stylesheet" type="text/css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/locales/fr.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script type="application/x-javascript">
  (function ($) {
    $('.views-exposed-widget input').attr('autocomplete', 'off');
    $("#scans").fileinput();
    runDrapable();
    var lock_icon = "<i title='Supprimer facture' class='glyphicon glyphicon-lock'></i>";
    $(".droppable").droppable({
      greedy: true,
      classes: {
        "ui-droppable-active": "bg-success",
        "ui-droppable-hover": "bg-warning"
      },
      drop: function (event, ui) {
        $(this).append(ui.helper.context);
        var input = $(this).find('input');
        input.val("");
        var that = $(this);
        $(this).find('.draggable').text(function (index) {
          input.val($(this).text() + '/' + input.val());
        });
        recycleImage(ui.draggable);
        $(".droppable .glyphicon-trash").bind('click', function () {
          $(this).closest(".draggable").find(".glyphicon-trash").remove().end().appendTo("#list-files");
          input.val("");
          that.find(".draggable").text(function (index) {
            input.val($(this).text() + '/' + input.val());
          });
        });
      }
    });

    function recycleImage($item) {
      // Image recycle function
      var trash_icon = "<i title='Supprimer facture' class='glyphicon glyphicon-trash'></i>";
      $item.fadeOut(function () {
        $item
          .css("width", "100%")
          .find('.glyphicon-trash').remove().end()
          .append(trash_icon)
          .find("img").css("width", "350px").end()
          .fadeIn();

        $(".thumbnail.droppable .glyphicon-trash").on("click", function () {
          var parent = $(this).closest('.thumbnail.droppable');
          var input = parent.find('input');
          $(this).closest(".draggable").find(".glyphicon-trash").remove().end().addClass('no-drop').append(lock_icon).appendTo("#list-files");
          input.val("");
          parent.find("li").text(function (index) {
            input.val($(this).text() + '/' + input.val());
          });
          runDrapable();
        });
      });
    }

    function runDrapable() {
      $(".draggable").draggable({
        cancel: "a.ui-icon", // clicking an icon won't initiate dragging
        revert: "invalid", // when not dropped, the item will revert back to its initial position
        containment: "document",
        helper: "clone",
        cursor: "move"
      });
    }

    $('#edit-field-invoice-date-comptable-value-min input').datetimepicker({
      locale: 'fr',
      format: 'DD/MM/YYYY'
    });
    $('#edit-field-invoice-date-comptable-value-max input').datetimepicker({
      locale: 'fr',
      format: 'DD/MM/YYYY'
    });
  }(jQuery));
</script>

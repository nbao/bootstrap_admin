<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

$field_info = field_info_field('field_state')["settings"]["allowed_values"];
$bg_panel = [
  'panel-success',
  'panel-danger',
  'panel-info',
  'panel-warning',
  'panel-primary',
];
if (empty($view->result)) {
  return;
}
ctools_include('ajax');
ctools_include('modal');
ctools_modal_add_js();
foreach ($view->result as $result) {
  $state = $result->field_field_state[0]["raw"]["value"];
  if (empty($result->users_node_picture)) {
    $temp = explode(' ', $result->users_node_name);
    $letter = $temp[0][0];
    if (!empty($temp[1])) {
      $letter .= $temp[1][0];
    }
    else {
      $letter .= $temp[0][1];
    }
    $letter = strtoupper($letter);
    $avantar = '<div class="bg-primary img-circle avatar-text" title="' . $result->users_node_name . '" data-uid="' . $result->users_node_uid . '">' . $letter . '</div>';
  }
  elseif(!empty($result->users_node_uid)) {
    $demandeur = user_load($result->users_node_uid);
    $avantar = theme('user_picture', array('account' =>$demandeur));
    /*
    $file = file_load($result->users_node_picture);
    $image_url = image_style_url('thumbnail', $file->uri);
    $avantar = '<img src="' . $image_url . '" class="media-object" style="width:60px">';
    */
  }
  $assigned_avatar = '';
  if (!empty($result->field_field_assigned[0]["raw"]["entity"])) {
    $assigne = $result->field_field_assigned[0]["raw"]["entity"];
  }
  if (empty($assigne) && !empty($result->_field_data["nid"]["entity"]->field_assigned["und"][0]["target_id"])) {
    $assigne = user_load($result->_field_data["nid"]["entity"]->field_assigned["und"][0]["target_id"]);
  }
  if (!empty($assigne)) {
    if (empty($assigne->picture)) {
      $temp = explode(' ', $assigne->name);
      $letter = $temp[0][0];
      if (!empty($temp[1])) {
        $letter .= $temp[1][0];
      }
      else {
        $letter .= $temp[0][1];
      }
      $letter = strtoupper($letter);
      $assigned_avatar = '<div class="bg-danger img-circle avatar-text" title="' . $assigne->name . '" data-uid="' . $assigne->uid . '">' . $letter . '</div>';
    }
    else {
      $assigned_avatar = theme('user_picture', array('account' =>$assigne));
      //$file = file_load($assigne->picture);
      //$assigned_avatar = theme('image_style', array('style_name' => 'thumbnail', 'path' => $file->uri));
      //$assigned_avatar = '<img src="' . $image_url . '" class="media-object img-circle">';
    }
  }
  $link_view = l("<i class='glyphicon glyphicon-eye-open'></i>", "support/".$result->nid, [
      "html" => TRUE,
      "attributes" => [
        "class" => [
          "ctools-use-modal",
        ],
      ],
    ]
  );
  $ticket[$state][$result->nid] = '
    <div class="media-left">' .
    $avantar
    . '<time class="text-center" datetime="' . date('Y-m-d H:i:s', $result->node_created) . '">' . date('d M', $result->node_created) . '</time>
    </div>
    <div class="media-body">
      <h4 class="media-heading">' . l($result->node_title, '/node/' . $result->nid) . '</h4>
      <p>' . drupal_render($result->field_body[0]["rendered"]) . '</p>' .
    $link_view
    . '</div>
    <div class="media-right">' .
    $assigned_avatar
    . '<time class="text-center" datetime="' . date('Y-m-d H:i:s', $result->node_comment_statistics_last_updated) . '">' . date('d M', $result->node_comment_statistics_last_updated) . '</time>
    </div>';
}
foreach ($field_info as $status => $label_status):
  $bg = array_pop($bg_panel);
  ?>
  <div class="col-md-3" id="<?php echo $status?>">
    <div class="panel droppable <?php echo $bg ?>" data-status="<?php echo $status ?>">
      <?php if (!empty($label_status)): ?>
        <div class="panel-heading"><?php print $label_status; ?></div>
      <?php endif; ?>
      <div class="panel-body" data-status="<?php echo $status ?>">
        <?php
        if (!empty($ticket[$status])) {
          foreach ($ticket[$status] as $nid => $row): ?>
            <div class="media shadow rounded draggable"
                 data-nid="<?php echo $nid ?>"
                 data-status="<?php echo $status ?>">
              <?php print $row; ?>
            </div>
          <?php
          endforeach;
        } ?>
      </div>
    </div>
  </div>
<?php
endforeach;
drupal_add_library('system', 'ui.draggable');
drupal_add_js(drupal_get_path('module', 'erp') . '/js/support.js');
?>

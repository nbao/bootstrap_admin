<?php
$year = current($view->args);
$separator = "|";
foreach (['psf', 'salaires', 'supplementaire'] as $export_view_id) {
  $view_result = views_get_view_result('export', $export_view_id, $year);
  if (!empty($view_result)) {
    foreach ($view_result as $result) {
      $paiement_date = NULL;
      $range = 1;
      if (!empty($result->field_field_paiement_date)) {
        $paiement_date = strtotime($result->field_field_paiement_date[0]["raw"]["value"]);
        $range = ceil(date('n', $paiement_date) / 3);
      }
      $category_name = $export_view_id == 'salaires' ? 'HR & Communication' . $separator . 'Salaires' : drupal_render($result->field_field_category[0]["rendered"]);
      if (!empty($result->field_field_code_tresor[0]["raw"]["taxonomy_term"]->field_tags["und"][0]["tid"]) && empty($category_name)) {
        $nature = taxonomy_term_load($result->field_field_code_tresor[0]["raw"]["taxonomy_term"]->field_tags["und"][0]["tid"]);
        $parent = current(taxonomy_get_parents($nature->tid));
        $cat = [];
        if (!empty($parent)) {
          $cat[] = $parent->name;
        }
        $cat[] = $nature->name;
        $category_name = implode($separator, $cat);
      }
      $negative = erp_rule_amount($result->_field_data["field_paiement_lignes_field_collection_item_nid"]["entity"]);
      $montant = $negative * abs($result->field_field_montant[0]["raw"]["value"]);
      $item_row = [
        'codeprojet' => NULL,
        'nomprojet' => NULL,
        'societe' => !empty($result->field_field_paiement_site) ? $result->field_field_paiement_site[0]["raw"]["entity"]->name : NULL,
        'fournisseur' => !empty($result->field_field_compte_beneficiaire) ? $result->field_field_compte_beneficiaire[0]["raw"]["value"] : NULL,
        'Categorie' => html_entity_decode($category_name),
        'Annee' => !empty($paiement_date) ? date('Y', $paiement_date) : NULL,
        'Trimestre' => 'q' . $range,
        'NumInvoice' => !empty($result->field_paiement_lignes_field_collection_item_title) ? $result->field_paiement_lignes_field_collection_item_title : NULL,
        'DateComptable' => !empty($paiement_date) ? date('d/m/Y', $paiement_date) : NULL,
        'Montant' => number_format($montant, 2, ',', ' '),
        'Exercice' => !empty($paiement_date) ? date('Y', $paiement_date) : NULL,
        'Version' => 'Actual',
        'GROUPE_CAT' => 'OFFICE MANAGER',
        'SUPRA' => NULL,
        'ACTIVITE' => NULL,
        'ChefProjet' => NULL,
        'PostDate' => !empty($result->field_paiement_lignes_field_collection_item_created) ?
          date('d/m/Y', $result->field_paiement_lignes_field_collection_item_created) : NULL,
      ];
      print implode($options["separator"], $item_row) . "\r\n";
    }
  }
}

$view_result = views_get_view_result('budget', 'budget', $year);
if (!empty($view_result)) {
  foreach ($view_result as $result) {
    $activite = !empty($result->field_field_activite) ? html_entity_decode(drupal_render($result->field_field_activite[0]["rendered"])) : NULL;
    $supra = !empty($result->field_field_projet_supra) ? $result->field_field_projet_supra[0]["raw"]["taxonomy_term"]->name : NULL;
    $ChefProjet = !empty($result->field_field_projet_chef) ? $result->field_field_projet_chef[0]["raw"]["user"]->name : NULL;
    foreach (range(1, 4) as $range) {
      $q = "budget_q$range";
      if (!empty($result->$q)) {
        $term = taxonomy_term_load($result->budget_category);
        $parents = taxonomy_get_parents_all($term->tid);
        $parents = array_reverse($parents);
        $terms = [];
        foreach ($parents as $parent) {
          $terms[] = $parent->name;
        }
        $category = !empty($terms) ? implode($separator, $terms) : NULL;
        $item_row = [
          'codeprojet' => !empty($result->field_field_projet_racine) ? $result->field_field_projet_racine[0]["raw"]["value"] : NULL,
          'nomprojet' => !empty($result->node_budget_title) ? $result->node_budget_title : NULL,
          'societe' => !empty($result->taxonomy_term_data_budget_name) ? $result->taxonomy_term_data_budget_name : NULL,
          'fournisseur' => NULL,
          'Categorie' => html_entity_decode($category),
          'Annee' => !empty($result->budget_years) ? $result->budget_years : NULL,
          'Trimestre' => 'q' . $range,
          'NumInvoice' => NULL,
          'DateComptable' => NULL,
          'Montant' => number_format($result->$q, 2, ',', ' '),
          'Exercice' => !empty($result->budget_years) ? $result->budget_years : NULL,
          'Version' => !empty($result->budget_version) ? $result->budget_version : NULL,
          'GROUPE_CAT' => !empty($result->budget_groups) ? $result->budget_groups : NULL,
          'SUPRA' => $supra,
          'ACTIVITE' => $activite,
          'ChefProjet' => $ChefProjet,
          'PostDate' => NULL,
        ];
        print implode($options["separator"], $item_row) . "\r\n";
      }
    }
  }
}


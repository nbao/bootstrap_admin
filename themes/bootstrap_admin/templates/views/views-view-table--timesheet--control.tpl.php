<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 *
 * @ingroup views_templates
 */
$motif = [
  "Absence justifiée (En déplacement)",
  "Absence justifiée (Travail à domicile)",
  "Congé Payé",
  "Convention collective",
  "Formation",
  "Maternité/Paternité",
  "RTT",
  "Travail weekend, jours féries"
];
$total = array_pop($header);
$year = $view->args[0];
$month_start = '01';
$range_month = 12;
$month = (int)date("m");
setlocale(LC_TIME, "fr_FR");
if ($year == date("Y")) {
  $range_month = (int)date('m');
}
foreach (range($month_start, $range_month) as $m) {
  $year_month = date('Y-m',strtotime($year . '-' . $m));
  $month_name = strftime("%B ", strtotime($year_month));
  array_push($header, $month_name);
  $months[$year_month] = $month_name;
}
array_push($header, $total);
if (!empty($result)) {
  $sql = "
SELECT
	    uid,
	    MONTH(start) AS CalMonth,
	    YEAR(start) AS CalYear,
	    SUM(IFNULL (datediff(end, start)*pointage,pointage)) AS Total
	FROM time_sheet
        WHERE
            end IS NULL AND
            start BETWEEN '$year-$month_start-01' AND LAST_DAY('$year-$range_month-01')
	GROUP BY
	    uid,
	    MONTH(start),
	    YEAR(start)";
  $records = db_query($sql);
  foreach ($records as $record) {
    $year_month = date('Y-m',strtotime($record->CalYear . '-' . $record->CalMonth));
    $time[$record->uid][$year_month] = $record->Total;
  }
}
$networkday = erp_networkday_by_year($year);

drupal_add_js('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js', 'external');
drupal_add_js('https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js', 'external');
drupal_add_js('https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js', 'external');
drupal_add_js('https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js', 'external');
drupal_add_js('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js', 'external');
drupal_add_js('https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js', 'external');
drupal_add_css('https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css', 'external');
drupal_add_css('https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css', 'external');

drupal_add_js(drupal_get_path('module', 'erp') . '/js/copy.js', 'file');

?>


<table
  id="table-copy-<?php print $id ?>" <?php if ($classes): ?> class="btn-export <?php print $classes; ?>"<?php endif ?><?php print $attributes; ?>
  tableexport-key="<?php echo strip_tags($title) ?>" data-name="<?php echo strip_tags($title) ?>">
  <?php if (!empty($title) || !empty($caption)): ?>
    <caption><?php print $caption . $title; ?></caption>
  <?php endif; ?>
  <?php if (!empty($header)) : ?>
    <thead>
    <tr>
      <th>Nbr Jours ouvrés</th>
      <?php foreach (range($month_start, $range_month) as $m) { ?>
        <th><?php echo $networkday[$m]["networkday"] ?></th>
      <?php } ?>
      <th></th>
    </tr>
    <tr>
      <th>Nbr heures théoriques</th>
      <?php foreach (range($month_start, $range_month) as $m) { ?>
        <th><?php echo $networkday[$m]["hour"] ?></th>
      <?php } ?>
      <th></th>
    </tr>
    <tr>
      <?php foreach ($header as $field => $label): ?>
        <th <?php if (!empty($header_classes[$field])): ?> class="<?php print $header_classes[$field]; ?>"<?php endif; ?>
          scope="col">
          <?php print $label; ?>
        </th>
      <?php endforeach; ?>
    </tr>
    </thead>
  <?php endif; ?>
  <tbody>
  <?php foreach ($rows as $row_count => $row):
    if (!empty($result[$row_count])) {
      $total = array_pop($row);
      foreach (range($month_start, $range_month) as $m) {
        $pointage = 0;
        $year_month = date('Y-m', strtotime($year . "-" . $m));
        if (!empty($time[$result[$row_count]->users_time_sheet_uid][$year_month])) {
          $pointage = $time[$result[$row_count]->users_time_sheet_uid][$year_month];
        }
        $nbr_jour = _count_day_conges($result[$row_count]->users_time_sheet_uid, $motif, "$year_month-01",date("Y-m-t", strtotime("$year_month-01")) );
        if($nbr_jour){
          $pointage += $nbr_jour * 8;
        }
        $row[$m] = $pointage;
      }
      $row['pointage'] = $total;
    }
    ?>
    <tr <?php if ($row_classes[$row_count]): ?> class="<?php print implode(' ', $row_classes[$row_count]); ?>"<?php endif; ?>>
      <?php foreach ($row as $field => $content):
        $warning = '';
        if ($content && !empty($networkday[$field]) && $networkday[$field]["hour"] != $content) {
          $warning = " bg-danger ";
        }
        if (!empty($field_classes[$field][$row_count])) {
          $warning .= $field_classes[$field][$row_count];
        }
        ?>
        <td
          class="<?php print $warning ?>" <?php print !empty($field_attributes[$field][$row_count]) ? drupal_attributes($field_attributes[$field][$row_count]) : '' ?>>
          <?php print $content; ?>
        </td>
      <?php endforeach; ?>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<hr/>

<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="col-md-4">
  <div class="panel panel-default">
    <?php if (!empty($title)): ?>
      <div class="panel-heading"><?php print $title; ?></div>
    <?php endif; ?>
    <div class="panel-body">
      <?php foreach ($rows as $id => $row): ?>
        <div<?php if ($classes_array[$id]): ?> class="<?php print $classes_array[$id]; ?>"<?php endif; ?>>
          <?php print $row; ?>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>


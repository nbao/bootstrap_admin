<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
if (empty($view->result)) {
  return;
}
ctools_include('ajax');
ctools_include('modal');
ctools_modal_add_js();
foreach ($view->result as $result) {
  $state = $result->field_field_follow_status[0]["raw"]["tid"];
  if (empty($result->users_node_picture)) {
    $temp = explode(' ', $result->users_node_name);
    $letter = $temp[0][0];
    if (!empty($temp[1])) {
      $letter .= $temp[1][0];
    }
    else {
      $letter .= $temp[0][1];
    }
    $letter = strtoupper($letter);
    $avantar = '<div class="bg-primary img-circle avatar-text" title="' . $result->users_node_name . '" data-uid="' . $result->users_node_uid . '">' . $letter . '</div>';
  }
  elseif(!empty($result->users_node_uid)) {
    $demandeur = user_load($result->users_node_uid);
    $avantar = theme('user_picture', array('account' =>$demandeur));
  }
  $assigned_avatar = [];
  if (!empty($result->field_field_follow_assigne)) {
    foreach ($result->field_field_follow_assigne as $follow_assigne){
      $assigneds[] = $follow_assigne["raw"]["user"];
    }
  }
  if (empty($assigneds) && !empty($result->_field_data["nid"]["entity"]->field_follow_assigne["und"][0]["uid"])) {
    $assigneds = user_load($result->_field_data["nid"]["entity"]->field_follow_assigne["und"][0]["uid"]);
  }
  if (!empty($assigneds)) {
    foreach ($assigneds as $assigned) {
      if (empty($assigned->picture)) {
        $temp = explode(' ', $assigned->name);
        $letter = $temp[0][0];
        if (!empty($temp[1])) {
          $letter .= $temp[1][0];
        }
        else {
          $letter .= $temp[0][1];
        }
        $letter = strtoupper($letter);
        $assigned_avatar[$assigned->uid] = '<div class="bg-danger img-circle avatar-text" title="' . $assigned->name . '" data-uid="' . $assigned->uid . '">' . $letter . '</div>';
      }
      else {
        $assigned_avatar[$assigned->uid] = theme('user_picture', ['account' => $assigned]);
      }
    }
  }
  $link_view = l("<i class='glyphicon glyphicon-eye-open'></i>", "support/".$result->nid, [
      "html" => TRUE,
      "attributes" => [
        "class" => [
          "ctools-use-modal",
        ],
      ],
    ]
  );
  $ticket[$state][$result->nid] = '
    <div class="media-left">' .
    $avantar
    . '<time class="text-center" datetime="' . date('Y-m-d H:i:s', $result->node_created) . '">' . date('d M', $result->node_created) . '</time>
    </div>
    <div class="media-body">
      <h4 class="media-heading">' . l($result->node_title, '/node/' . $result->nid) . '</h4>
      <p>' . drupal_render($result->field_body[0]["rendered"]) . '</p>' .
    $link_view
    . '</div>
    <div class="media-right">' .
    implode('',$assigned_avatar)
    . '<time class="text-center" datetime="' . date('Y-m-d H:i:s', $result->node_comment_statistics_last_updated) . '">' . date('d M', $result->node_comment_statistics_last_updated) . '</time>
    </div>';
}


//$field_info = field_info_field('field_follow_status')["settings"]["allowed_values"];
//foreach ($field_info as $status => $label_status):
$follow_voc = taxonomy_vocabulary_machine_name_load('follow_status');
$tree = taxonomy_get_tree($follow_voc->vid);
$col = 3;
$col_status = ceil(12 / count($tree));
if ($col_status > $col) {
  $col = $col_status;
}
foreach ($tree as $term):
  $status = $term->tid;
  $label_status = $term->name;
  $term = taxonomy_term_load($term->tid);
  $bg = 'panel-'.$term->field_follow_color[LANGUAGE_NONE][0]['value'];
  ?>
  <div class="col-md-<?php echo $col?>" id="<?php echo $status?>">
    <div class="panel droppable <?php echo $bg ?>" data-status="<?php echo $status ?>">
      <?php if (!empty($label_status)): ?>
        <div class="panel-heading"><?php print $label_status; ?></div>
      <?php endif; ?>
      <div class="panel-body" data-status="<?php echo $status ?>">
        <?php
        if (!empty($ticket[$status])) {
          foreach ($ticket[$status] as $nid => $row): ?>
            <div class="media shadow rounded draggable"
                 data-nid="<?php echo $nid ?>"
                 data-status="<?php echo $status ?>">
              <?php print $row; ?>
            </div>
          <?php
          endforeach;
        } ?>
      </div>
    </div>
  </div>
<?php
endforeach;
drupal_add_library('system', 'ui.draggable');
drupal_add_js(drupal_get_path('module', 'erp') . '/js/follow.js');
?>

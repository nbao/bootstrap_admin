<?php
/**
 * @file
 * template.php
 */

/**
 * Implements theme_admin_block().
 * Adding classes to the administration blocks
 */
function bootstrap_admin_admin_block($variables)
{
    $block = $variables['block'];
    $output = '';
    $icon = [
        'workflow' => 'glyphicon glyphicon-retweet',
        'messaging' => 'glyphicon glyphicon-envelope',
        'group' => 'fa fa-users',
        'services' => 'glyphicon glyphicon-signal',
        'development' => 'fa fa-bug',
        'user-interface' => 'glyphicon glyphicon-hand-up',
        'system' => 'glyphicon glyphicon-cog',
        'administration' => 'glyphicon glyphicon-wrench',
        'regional' => 'glyphicon glyphicon-globe',
        'search' => 'glyphicon glyphicon-search',
        'media' => 'glyphicon glyphicon-picture',
        'date' => 'glyphicon glyphicon-calendar',
        'content' => 'glyphicon glyphicon-pencil',
        'people' => 'glyphicon glyphicon-user',
        'fields' => 'glyphicon glyphicon-th-list',
        'elasticsearch-connector' => 'glyphicon glyphicon-list-alt'
    ];
    // Don't display the block if it has no content to display.
    if (empty($block['show'])) {
        return $output;
    }
    $class_tilte = $title = $href = '';
    $output .= '<div class="panel panel-default">';
    if (!empty($block['path'])) {
        $class_tilte = check_plain(str_replace(array("admin", "/"), " ", $block['path']));
        $href = check_plain(str_replace("/", "-", $block['path']));
    }
    $temp = explode(' ', $class_tilte);
    $icon_key = end($temp);
    $icon_val = !empty($icon[$icon_key]) ? $icon[$icon_key] : $icon['system'];
    $icon_string = "<i class='{$icon_val}'></i>";
    if (!empty($block['title'])) {
        $title = $block['title'];
    }
    $output .= '<a class="panel-heading ' . $class_tilte . '" data-toggle="collapse" href="#' . $href . '">' . $icon_string . ' ' . $title . ' <span class="caret"></span></a>';

    if (!empty($block['content'])) {
        $output .= '<div class="body panel-body" id="' . $href . '">' . $block['content'] . '</div>';
    } else {
        $output .= '<div class="description">' . $block['description'] . '</div>';
    }

    $output .= '</div>';

    return $output;
}

/**
 * Returns HTML for the content of an administrative block.
 *
 * @param $variables
 *   An associative array containing:
 *   - content: An array containing information about the block. Each element
 *     of the array represents an administrative menu item, and must at least
 *     contain the keys 'title', 'href', and 'localized_options', which are
 *     passed to l(). A 'description' key may also be provided.
 *
 * @ingroup themeable
 */
function bootstrap_admin_admin_block_content($variables)
{
    $content = $variables['content'];
    $output = '';

    if (!empty($content)) {
        $class = 'admin-list ';
        if ($compact = system_admin_compact_mode()) {
            $class .= ' compact';
        }
        $output .= '<dl class="' . $class . '">';
        $i = 1;
        foreach ($content as $item) {
            $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
            if (!$compact && isset($item['description'])) {
                $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
            }
            if (count($content) != $i++) $output .= '<hr/>';
        }
        $output .= '</dl>';
    }
    return $output;
}
<?php
/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function digest_form_digest_node_form_alter(&$form, &$form_state, $form_id) {
  if (!user_access('administer')) {
    $form['title']['#access'] = FALSE;
  }
  $form["body"][LANGUAGE_NONE][0]["#format"] = 'filtered_html';
  $form['#prefix'] = '<div id="' . $form_id . '">';
  $form['#suffix'] = '<div>';
  $form['field_url'][LANGUAGE_NONE][0]['#ajax'] = [
    'callback' => '_field_url',
    'wrapper' => $form_id,
    'event' => 'change',
  ];
  if(!empty($form_state["values"])){
    $form_state["values"]["language"] = LANGUAGE_NONE;
  }
  array_unshift($form['actions']['submit']['#submit'], '_digest_form_submit');
}

/**
 * Implements hook_element_info_alter().
 */
function digest_element_info_alter(&$types) {
  if (isset($types['link_field'])) {
    $types['link_field']['#process'][] = 'digest_link_field_ajax_process';
  }
}

function digest_link_field_ajax_process($element, &$form_state, $complete_form) {
  // Inherit for both 'url' and 'title'
  if (!empty($element["#bundle"]) && $element["#bundle"] == 'digest') {
    $element['url']['#ajax'] = isset($element['#ajax']) ? $element['#ajax'] : NULL;
  }
  return $element;
}

function _field_url(&$form, &$form_state) {
  if (!empty($form_state["values"]["field_url"][LANGUAGE_NONE])) {
    $form_state["values"]["language"] = LANGUAGE_NONE;
    $url = trim($form_state["values"]["field_url"][LANGUAGE_NONE][0]["url"]);

    //check if url is exist
    $hasURL = db_select('field_data_field_url', 'n')
      ->fields('n')
      ->condition('field_url_url',$url,'=')
      ->execute()
      ->fetchAssoc();
    if($hasURL) {
      drupal_set_message('Merci, mais cet article a déjà été ajouté par un autre utilisateur','error');
      return $form;
    }

    $command = "mercury-parser " . trim($url);
    $output = shell_exec($command);
    if (strpos($output, 'Mercury Parser encountered a problem trying to parse that resource') !== FALSE) {
      form_set_error('field_url[und', t("Votre lien n'existe pas"));
      $output = NULL;
    }
    elseif (!empty($output)) {
      $output = json_decode($output);
    }
    if (!empty($output->title)) {
      $form["title"]['#value'] =
      $form_state["values"]["field_url"][LANGUAGE_NONE][0]["title"] =
      $form["field_url"][LANGUAGE_NONE][0]["title"]["#value"] = $output->title;
    }
    if (!empty($output->lead_image_url)) {
      $form["field_image_url"]["und"][0]["value"]["#value"] = $output->lead_image_url;
    }
    if (!empty($output->content)) {
      $form["body"][LANGUAGE_NONE][0]["value"]["#value"] = html_entity_decode($output->content);
      if (!empty($output->excerpt)) {
        $form["body"][LANGUAGE_NONE][0]["summary"]["#value"] = $output->excerpt;
      }
      $info_url = parse_url($url);
      $host_external = $info_url['scheme'].'://'.$info_url['host'];
      $form["body"][LANGUAGE_NONE][0]["value"]["#value"] = str_replace(
        [' href="/',' src="/'],
        [' href="'.$host_external.'/',' src="'.$host_external.'/'],
        $form["body"][LANGUAGE_NONE][0]["value"]["#value"]
      );
    }
    if(!empty($form["field_country"][LANGUAGE_NONE]["#options"])){
      $country_detected = detect_country($form["body"][LANGUAGE_NONE][0]["value"]["#value"], $form["field_country"][LANGUAGE_NONE]["#options"]);
      if ($country_detected) {
        $form["field_country"][LANGUAGE_NONE]["#default_value"] = $country_detected;
        foreach ($country_detected as $iso2) {
          if (!empty($form["field_category_digest"][LANGUAGE_NONE][$iso2])) {
            $form["field_category_digest"][LANGUAGE_NONE][$iso2]["#checked"] = TRUE;
          }
        }
      }
    }

    if($cat = detect_category($form["body"][LANGUAGE_NONE][0]["value"]["#value"])){
      foreach ($cat as $tid){
        $form["field_category_digest"][LANGUAGE_NONE]["#value"] = $tid;
        if(!empty($form["field_category_digest"][LANGUAGE_NONE][$tid])){
          $form["field_category_digest"][LANGUAGE_NONE][$tid]["#checked"] = true;
        }
        break;
      }
    }
    if (!empty($output->date_published)) {
      $form_state["values"]["created"] =
      $form["created"]["#value"] = strtotime($output->date_published);
    }
  }
  return $form;
}
function detect_category($content){
  $cat = [
    'offshore' => 'Floating offshore wind',
    'onshore' => 'Onshore wind',
    'solar' => 'Solar',
    'marine' => 'Marine renewable energies',
    'eolfi' => 'EOLFI',
  ];
  $out = FALSE;
  $content = strip_tags(html_entity_decode($content));
  /* remove <= 4 letter words */
  $content =  preg_replace("/\b[^-]{1,4}\b/u",' ', strtolower($content));
  foreach ($cat as $detect=>$term_name){
    if (strpos($content, $detect) !== false) {
      $term = taxonomy_get_term_by_name($term_name,'category_digest');
      $tid = reset($term)->tid;
      $out[$tid] = $tid;
    }
  }
  return $out;
}
function detect_country($content,$option = false){
  $out = false;
  $content = strtolower(strip_tags($content));
  foreach ($option as $iso2=>$name_country){

    if ( strpos( $content, strtolower($name_country) ) !== FALSE ){
      $out[] = $iso2;
    }
  }
  return $out;
}
function _digest_form_submit(&$form, &$form_state) {
  $form_state['values']['title'] = $form_state["values"]["field_url"][LANGUAGE_NONE][0]["title"];
}

function _digest_week(){
  drupal_add_js(drupal_get_path('module', 'digest') . '/js/digest.js', 'file');
  $date = date("Y-m-d",strtotime("now"));
  $get = drupal_get_query_parameters();
  if(!empty($get["changed"]["min"])){
    $date = $get["changed"]["min"];
  }
  $nextMonday = new \DateTime($date.' next Monday');
  $previousMonday = new \DateTime($date.' Monday ago');
  $nextSaturday = new \DateTime($nextMonday->format('Y-m-d').' next Saturday');
  $previousSaturday = new \DateTime($date.' Saturday ago');
  $get["changed"]["min"] = $previousMonday->format('Y-m-d');
  $get["changed"]["max"] = $previousSaturday->format('Y-m-d');
  echo l("<i class='glyphicon glyphicon-chevron-left'></i> ".t("Previous Week"),'digest',[
    'attributes' =>[
      'class' => ['btn btn-success']
    ],
    'query'=>$get,
    'html' =>true
  ]);
  $get["changed"]["min"] = $nextMonday->format('Y-m-d');
  $get["changed"]["max"] = $nextSaturday->format('Y-m-d');
  echo l(t("Next Week ")."<i class='glyphicon glyphicon-chevron-right'></i> ",'digest',[
    'attributes' =>[
      'class' => ['btn btn-success']
    ],
    'query'=>$get,
    'html' =>true
  ]);
}


<?php
/**
 * Implements hook_drush_command().
 */
function digest_drush_command() {

  $commands['send-digest'] = [
    'description' => 'Send digest in week for all user .',
    'aliases' => ['sd'],
    'examples' => [
      'drush sd' => 'Send email digest to salary.',
    ],
  ];

  return $commands;
}

/**
 * Drush command logic.
 * drush_[COMMAND_NAME]().
 */
function drush_digest_send_digest() {

  $term_name = 'Weekly EOLFI Digest';
  $model_load = taxonomy_get_term_by_name($term_name, 'modele');
  $term = end($model_load);
  if(!empty($term)){
    $to = variable_get('digest_email');
    $view_digest = views_embed_view('digest', 'pdf');

    $week_num = date('W-Y');
    $model = str_replace(
      [
        "[week_num]",
        "[view_digest]",
      ],
      [
        $week_num,
        $view_digest
      ],
      $term->description);
    $message = [$model];

    //generate pdf
    $paper_size = 'A4';
    $dpi = 96 ;
    $temp_html = file_unmanaged_save_data($model, drupal_tempnam('temporary://', 'c_html_') . '.html', FILE_EXISTS_RENAME);
    if ($temp_html === FALSE) {
      watchdog('digest', 'wkhtmltopdf: could not create temporary html file: %file', array('%file' => $temp_html));
      return NULL;
    }
    $html_input_parameter = drupal_realpath($temp_html);

    $file_pdf = drupal_realpath('public://')."/digest/digest-{$week_num}.pdf";
    $cmd = "wkhtmltopdf -q --page-size $paper_size --dpi $dpi --images $html_input_parameter $file_pdf";

    shell_exec($cmd);
    $users_email = [variable_get('site_mail')];
    $params = [
      'subject' => $term_name.' Week: '.date('W / Y'),
      'body' => $message,
      'headers' => array(
        'Bcc' => implode(',',$users_email),
      )
    ];
    drupal_mail('erp', 'Digest', $to, language_default(), $params);
  }
}

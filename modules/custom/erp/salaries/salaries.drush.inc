<?php
/**
 * Implements hook_drush_command().
 */
function salaries_drush_command() {

  $commands['notice-timesheet'] = [
    'description' => 'Notiifcation user has to score in 10 days.',
    'aliases' => ['nt'],
    'examples' => [
      'drush nt' => 'Send email to salary.',
    ],
  ];

  return $commands;
}

/**
 * Drush command logic.
 * drush_[COMMAND_NAME]().
 */
function drush_salaries_notice_timesheet() {
  drush_print("Begin: ". date("d/m/Y H:i") );
  $interval = 10;
  $sql = "SELECT uid,start, sum(pointage) as total
FROM `time_sheet` 
WHERE start BETWEEN (NOW() - INTERVAL :interval DAY) AND NOW()
Group by uid,start";

  $user_pointage_count = db_query($sql, [':interval' => $interval+1])->fetchAll();
  foreach ($user_pointage_count as $obj_time) {
    $user_pointage[$obj_time->uid][$obj_time->start] = $obj_time->total;
  }

  $sql = 'SELECT u.uid, max_date
FROM `users` u
LEFT JOIN (SELECT uid,MAX(start) as  max_date FROM time_sheet GROUP BY uid) t ON u.uid = t.`uid`
WHERE u.`status` = 1 AND u.uid > 1';
  $user_date = db_query($sql)->fetchAllKeyed();
  foreach ($user_date as $uid=>$date){
    if(empty($date)){
      unset($user_date[$uid]);
      continue;
    }
    if(strtotime($date) > strtotime("-$interval days")){
      $begin = new DateTime(date('Y-m-d',strtotime("-$interval days")));
      $end = new DateTime( date('Y-m-d',strtotime('yesterday')) );
      //$end = $end->modify( '+1 day' );

      $day = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $day ,$end);
      $ok = TRUE;
      foreach($daterange as $date){
        $check_day = $date->format("Y-m-d");
        if (!isWeekendOrHolidays($check_day)) {
          if (empty($user_pointage[$uid][$check_day]) || (!empty($user_pointage[$uid][$check_day]) && $user_pointage[$uid][$check_day] < 8)) {
            //check conges
            $sql = "SELECT c.entity_id,d.field_conges_date_value,d.field_conges_date_value2
FROM `field_data_field_conges` c
LEFT JOIN field_data_field_conges_date d ON d.`entity_id` = c.`field_conges_value`
WHERE c.`entity_id` = :uid AND
            (d.`field_conges_date_value` >= :date AND d.`field_conges_date_value2` <= :date)";
            $user_conges = db_query($sql, [ ':uid' => $uid, ':date' => $check_day])->fetchAllKeyed();
            if (!empty($user_conges)) {
              continue;
            }
            $ok = FALSE;
            $user_date[$uid] = $check_day;
            break;
          }
        }
      }
      if($ok){
        unset($user_date[$uid]);
      }
    }
  }
  $term_name = 'Rappel automatique de pointage Timesheet';
  $model_load = taxonomy_get_term_by_name($term_name, 'modele');
  $term = end($model_load);
  if(!empty($user_date)){
    foreach ($user_date as $uid=>$date) {
      $collaborateur = user_load($uid);
      $model = str_replace(
        [
          "[user:civilite]",
          "[user:name]",
          "[user:date]"
        ],
        [
          $collaborateur->field_nom[LANGUAGE_NONE][0]['title'],
          $collaborateur->name,
          !empty($date)?date('d/m/Y',strtotime($date)):'',
        ],
        $term->description);
      $message = [$model];
      $params = [
        'subject' => $term_name,
        'body' => $message,
      ];
      $to = $collaborateur->name." <".$collaborateur->mail.">";
      drupal_mail('erp', 'Timesheet', $to, language_default(), $params);
      drush_print("Email to $to : $date");
    }
  }

  drush_print("End: ". date("d/m/Y H:i") );
}

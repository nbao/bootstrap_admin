(function ($) {
  Drupal.behaviors.salaries = {
    attach: function (context, settings) {
      function block_field_montant(){
        $(".field-deplace-montant input").attr("readonly","readonly");
        $(".field-deplace-detail-frais input").attr("required","true");
        $(".field-deplace-detail-type select").each(function () {
          if ($(this).find("option:selected").text() == "Remboursement KM") {
            $(".field-deplace-detail-km, .field-deplace-detail-cv").css("display", "table-cell");
            $(this).closest("tr").find(".field-deplace-detail-frais input").attr("required",false).prop("readonly",true);
            $(this).closest("tr").find(".field-name-field-deplace-detail-km input").attr("required",true).prop("readonly",false);
          }else {
            $(this).closest("tr").find(".field-deplace-detail-frais input").attr("required","true").prop("readonly",false);
            $(this).closest("tr").find(".field-name-field-deplace-detail-km input").attr("required",false).prop("readonly",true);
          }
        });
      }
      $(".field-deplace-detail-type select").on("change", function () {
        block_field_montant();
      });

      $(".field-deplace-detail-type select").each(function () {
        block_field_montant();
      });
      block_field_montant();

      $(".field-devis select").on("change", function () {
        var taux = $(this).closest("tr").find(".field-taux input").val();
        if ($(this).val() != "EUR" && taux == 1) {
          $(this).closest("tr").find(".field-taux input").val("");
        }
        if ($(this).val() == "EUR") {
          $(this).closest("tr").find(".field-taux input").val(1);
        }
      });

      function count_total() {
        var sum = 0;
        $(".field-deplace-montant input").each(function () {
          var montant = $(this).val().replace(",", ".").replace(" ", "");
          if ($.isNumeric(montant)) {
            sum += parseFloat(montant);
          }
        });
        if ($.isNumeric(sum)) {
          $(".field-name-field-deplace-total input").val(sum.toFixed(2));
        }
      }

      if (!$.isEmptyObject(settings.salaries)) {
        $(".field-deplace-detail-cv select, .field-deplace-detail-km input").on("keyup change", function () {
          var cv = $(this).closest("tr").find(".field-deplace-detail-cv select").val();
          var d = parseFloat($(this).closest("tr").find(".field-deplace-detail-km input").val());
          var montant = null;

          if ($.isNumeric(cv) && d) {
            $.each(settings.salaries.tarif[cv], function (key, value) {
              if (key < d) {
                montant = eval(value);
              }
            });
          }
          if (montant) {
            $(this).closest("tr").find(".field-deplace-montant input").val(montant.toFixed(2));
            $(this).closest("tr").find(".field-deplace-detail-frais input").val(montant.toFixed(2));
            count_total();
          }
        });
      }
      //Linges
      $(".field-deplace-detail-frais input, .field-taux input").bind("keyup change", function () {
        var frais = $(this).closest("tr").find(".field-deplace-detail-frais input").val();
        var taux = $(this).closest("tr").find(".field-taux input").val();
        var montant = parseFloat(frais) * parseFloat(taux);
        if ($.isNumeric(montant)) {
          $(this).closest("tr").find(".field-deplace-montant input").val(montant.toFixed(2));
        }
        count_total();
      });
      //On submit page
      $(".page-field-collection-field-deplacement #edit-submit").hover(function() {
        count_total();
      });

      /*view congès validée*/
      $("tbody .views-field-field-justificatif").each(function () {
        var require = $.trim($(this).text());
        if (require !== "") {
          var recu = $.trim($(this).closest("tr").find(".views-field-field-conges-justificatif-recu").text());
          if (require !== recu) {
            $(this).closest("tr").find("input").remove();
          }
        }
      });

      $(".field-name-field-conges-date .checkbox input").click();

      /* create user */
      $("#edit-field-nom .name-family,#edit-field-nom .name-given").on("keyup", function () {
        var name = $("#edit-field-nom .name-given").val() + " " + $("#edit-field-nom .name-family").val().toUpperCase();
        $("#edit-name").val(name);
        $("#edit-mail").val(name.replace(/ /g, ".").toLowerCase() + "@eolfi.com");
      });

      /*Comptabiliser congés*/
      $(".views-field-field-conges-reponse").each(function () {
        var is_accepte = $.trim($(this).text());
        if (is_accepte != "Acceptée") {
          $(this).closest("tr").find(".vbo-select").remove();
        }
      });

      /* create ticket-restaurant/stock */
      $(".field-name-field-stock-value input,.field-name-field-stock-unite input").on("change",function () {
        let value = parseFloat($(".field-name-field-stock-value input").val());
        let unite = parseFloat($(".field-name-field-stock-unite input").val());
        if($.isNumeric(value) && $.isNumeric(unite)){
          let couts = value*unite;
          $(".field-name-field-stock-couts input").val(couts);
        }
      });
      /* view salaire*/
      function sumSalary() {
        var sumSalary = 0;
        $(".salary").each(function () {
          var val = $(this).val().replace(",", ".").replace(" ", "");
          if ($.isNumeric(val)) {
            sumSalary += parseFloat(val);
          }
        });
        if (sumSalary !== 0) {
          var sum = new Intl.NumberFormat("fr-FR", { style: "currency", currency: "EUR" }).format(sumSalary);
          $(".sum_salary").html("<b>Total</b>: " + sum);
        }
      }
      sumSalary();
      $(".salary").on("keyup", function () {
        sumSalary();
      });
      $(".view-salary table thead input,.view-salary table thead select").on("change", function () {
        sumSalary();
      });

      /*Salaire du mois a valider*/
      if($(".month_salary_validate").length) {
        let current_month = $.trim($(".month_salary_validate").data("date"));
        $(".view-id-salaire table tbody td.views-field-field-salaire-date").each(function () {
          let date_colum = $.trim($(this).text());
          if(date_colum !== current_month){
            $(this).closest("tr").addClass("bg-danger");
          }
        });
      }
      /*Count ticket restaurant*/
      var sumtotal = function(selector) {
        let sum = 0;
        $(selector).each(function() {
          let value = Number($(this).text());
          if($.isNumeric(value)){
            sum += value;
          }
        });
        return sum;
      }
      $(".total-ticket").text(sumtotal(".views-field-field-ticket-resto-qty"));
      $(".ticket_resto_num").on("change",function () {
        let ticket_num = eval($(this).val());
        if($.isNumeric(ticket_num)){
          ticket_num = Math.abs(ticket_num) + 1;
          $(this).closest("tr").find(".ticket_resto_qty").val(ticket_num);
        }
      });
    }
  };
}(jQuery));

(function ($, Drupal) {
  $(document).ready(function () {
    $(".form-item-title  input").bind("keyup change", function () {
      var $val = $(this).val().replace(" ", "").toUpperCase().substr(0, 10);
      $(".field-name-field-supplier-code input").val($val);
    });
    $(".field-name-field-no-siret input").on("change",function () {
      var siret = $(this).val().replace(" ","");
      var $this = $(this);
      if(siret.length > 9){
        siret = siret.slice(0,9);
      };
      if(siret.length == 9){
        var dataAPI = "https://entreprise.data.gouv.fr/api/sirene/v2/siren/" + siret;
        $.getJSON(dataAPI)
          .done(function (datas) {
            if(datas.sirene !== undefined){
              let data = datas.sirene.data.siege_social;
              if(data.nom_raison_sociale !== undefined){
                let nom_raison_sociale = $.trim(data.nom_raison_sociale.replace('*',' ').replace('/',' '));
                $(".form-item-title input").val(nom_raison_sociale);
                nom_raison_sociale = nom_raison_sociale.replace(" ", "").toUpperCase().substr(0, 10);
                $(".field-name-field-supplier-code input").val(nom_raison_sociale);
              }
              if(data.code_postal !== undefined){
                $(".field-name-field-supplier-adresse .postal-code").val(data.code_postal);
              }
              if(data.libelle_commune !== undefined){
                $(".field-name-field-supplier-adresse .locality").val(data.libelle_commune);
              }
              if(data.libelle_commune !== undefined){
                $(".field-name-field-supplier-adresse .locality").val(data.libelle_commune);
              }
              let adresse = [
                data.numero_voie,
                data.type_voie,
                data.libelle_voie,
                data.cedex
              ];
              if(data.libelle_voie !== undefined){
                $(".field-name-field-supplier-adresse .thoroughfare").val(adresse.join(' '));
              }
              if(data.email !== undefined){
                $(".field-name-field-societe-mail input").val(data.email);
              }
              if(data.siret !== undefined){
                $this.val(data.siret);
              }
              /*
              if(data.nom !== undefined){
                $(".field-contact-nom .name-family-wrapper input").val(data.nom);
              }
              if(data.prenom !== undefined){
                $(".field-contact-nom .name-given-wrapper input").val(data.prenom);
              }
              if(data.civilite !== undefined){
                if(data.civilite === "1"){
                  $(".field-contact-nom .name-title-wrapper select").val("Monsieur");
                }
                if(data.civilite === "2"){
                  $(".field-contact-nom .name-title-wrapper select").val("Madame");
                }
              }
               */
            }
            if(datas.computed !== undefined){
              $(".field-name-field-id-cee input").val(datas.computed.data.numero_tva_intra);
            }
          });
      }
    });

  });
})(jQuery, Drupal);

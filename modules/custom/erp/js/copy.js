(function ($, Drupal) {
  $(document).ready(function () {
    $('.btn-export').DataTable({
      dom: 'frtBip',
      paging: false,
      info: false,
      buttons: [
        'copyHtml5', 'excelHtml5'
      ]
    });
  });
})(jQuery, Drupal);


(function ($) {
  Drupal.behaviors.pointage = {
    attach: function (context, settings) {
      /*
      remove pointage timesheet
       */
      $(".pointage").on("click", function () {
        var hour_limit = 8;
        var hour = parseFloat($("input[type='radio']:checked").val());
        var day = $(this).data("day");
        if (!$(this).hasClass("disabled")) {
          $(this).find("span").text("");
        }
        var total = 0;
        $(".day-" + day).not(".demi-total").each(function () {
          var temp = parseFloat($(this).text());
          if ($.isNumeric(temp)) {
            total += temp;
          }
        });
        $(".table #total-" + day).html(total);

        if ((total + hour) > hour_limit) {
          hour = hour_limit - total;
        }
        if ($(this).hasClass("disabled")) {
          return;
        }
        $(this).html("");
        if (hour) {
          var input = "<input type='hidden' name='timesheet[" + $(this).data("nid") + "][" + $(this).data("date") +"]' value='" + hour + "'/>"
                      + "<span class='day-" + $(this).data("day") + "'>" + hour + "</span>";
          $(this).html(input);
          $(this).addClass("active");
        } else {
          $(this).removeClass("active");
          $(".pointage[data-day='" + day + "']").each(function () {
            $(this).removeClass("disabled btn-default active");
          });
        }
        // Count total hour
        total = 0;
        $(".day-" + day).each(function () {
          var temp = parseFloat($(this).text());
          if ($.isNumeric(temp)) {
            total += temp;
          }
        });
        $(".table #total-" + day).html(total);

        if (total >= hour_limit) {
          $(".pointage[data-day='" + day + "']").each(function () {
            if (!$(this).hasClass("active")) {
              $(this).addClass("disabled btn-default active");
            }
          });
        }
      });

      $(".id_timesheet").on("click", function () {
        var hour = parseFloat($("input[type='radio']:checked").val());
        var input = "<input type='hidden' name='delete[" + $(this).data("id_timesheet") + "]' value='" + hour + "'/>";
        if(hour == 0){
          $(this).addClass("bg-danger");
          $(this).html(input + "X");
        }else {
          $(this).html(input + "<span class='day-" + $(this).data("day") + "'>" + hour + "</span>");
        }
        return;
      });
      /*
      allow pointage on holiday and weekend
       */
      $(".mark").on("click", function () {
        if ($(this).hasClass("disabled")) {
          var confirme = confirm("Vous êtes sur le point de faire une déclaration de travail en weekend/ jour férié, qui nécessite une validation de votre responsable. Cliquer sur OK pour aller remplir le formulaire, si non, cliquer sur Annuler pour continuer le pointage");
          if (confirme == true) {
            $(this).text("X");
            var uid = $(this).data("uid");
            var date = $(this).data("date");
            var nid = $(this).data("nid");
            var win = window.open("/field-collection/field-conges/add/user/" + uid + "?destination=collaborateur/timesheet&date=" + date + "&nid=" + nid, "_blank");
            if (win) {
              //Browser has allowed it to be opened
              win.focus();
            } else {
              //Browser has blocked it
              alert("Please allow popups for this website");
            }
          }
          return;
        }
      });
      /*  View pointage */
      $(".views-table caption").each(function () {
        var sum = 0;
        $(this).parent().find("tbody tr td:last-child").each(function () {
          var val = $.trim($(this).text());
          if ($.isNumeric(val)) {
            sum += parseInt(val);
          }
        });
        if (sum) {
          $(this).parent().append(
            $("<tfoot/>").append($(this).parent().find("thead tr").clone(true))
          );
          $(this).parent().find("tfoot th:last-child").text(sum);
        }
        var caption = $(this).html();
        $(this).parent().find("thead th:first-child").addClass("text-center").html(caption);
        $(this).remove();
      });
    }
  };
}(jQuery));

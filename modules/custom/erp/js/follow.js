(function ($, Drupal) {
  $(document).ready(function () {
    $(".draggable").each(function () {
      var width = $(this).outerWidth( true );
      $(this).css("min-width",width);
    });
    $(".droppable .panel-body").each(function () {
      var height = $( window ).height()/2;
      $(this).css("min-height",height);
    });
    function calculate_max_height(){
      var max = 100;
      var height = 0;
      $(".panel.droppable").height("auto");
      $(".panel.droppable").each(function () {
        height = $(this).height();
        if(height>max){
          max = height;
        }
      });
      $(".panel.droppable").height(max);
    }
    calculate_max_height();
    $(window).resize(function() {
      calculate_max_height();
    });

    $(".draggable").draggable({
      cancel: "a.ui-icon", // clicking an icon won't initiate dragging
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "clone",
      cursor: "move",
    });

    $(".droppable .panel-body").droppable({
      greedy: true,
      classes: {
        "ui-droppable-active": "bg-success",
        "ui-droppable-hover": "bg-warning"
      },
      drop: function (event, ui) {
        $(this).append(ui.helper.context);
        if(ui.helper.data("status") !== $(this).data("status")){
          $.ajax({
            context: this,
            type: "POST",
            url: "/follow/change",
            dataType: "json",
            data: {
              nid: ui.helper.data("nid"),
              status: $(this).data("status"),
            },
            success: function (data) {
              //$(this).attr("disabled", "disabled");
            }
          });
        }
        calculate_max_height();
      }
    });

  });
})(jQuery, Drupal);

(function ($) {
  $("form.signature").submit(function () {
    return false;
  });
  Drupal.behaviors.signature = {
    attach: function (context, settings) {
      $(".ticket_resto_qty").bind("keyup change", function () {
        $(this).closest("tr").find(".signature-submit").data("qty", $(this).val());
      });
      $(".ticket_resto_num").bind("keyup change", function () {
        $(this).closest("tr").find(".signature-submit").data("num", $(this).val());
      });
      $(".signature-submit").on("click", function () {
        $(this).attr("disabled", "disabled");
        var valide_check = 0;
        if ($(this).closest("tr").find(".views-field-field-ticket-valide input").prop("checked")){
          valide_check = 1;
        }
        $.ajax({
          context: this,
          type: "POST",
          url: "/ticket-restaurant/signature",
          dataType: "json",
          data: {
            uid: $(this).data("uid"),
            date: $(this).data("date"),
            qty: $(this).data("qty"),
            num: $(this).data("num"),
            item_id: $(this).data("item_id"),
            valide: valide_check,
            ticket: $(this).closest("tr").find(".views-field-field-ticket-resto-qty input").val(),
            sign: $(this).closest("form").find(".esign_container input").val()
          },
          success: function (data) {
            $(this).attr("disabled", "disabled");
          }
        });
      });

    }
  };

  var confirm_nav_ignore = [];

  function confirm_nav() {
    function save_form_state($form) {
      var old_state = $form.serialize();
      $form.data("old_state", old_state);
    }

    // On load, save form current state
    $("form").each(function () {
      save_form_state($(this));
    });

    // On submit, save form current state
    var on_submit = true;
    $(".region-content").on("click", ".form-actions button", function () {
      if (!confirm("Êtes vous sure d'enregistrer les modifications ?")) {
        $(window).off("beforeunload");
        return false;
      }
      on_submit = false;
    });

    // strip fields that we should ignore
    function strip(form_data) {
      for (var i = 0; i < confirm_nav_ignore.length; i++) {
        var field = confirm_nav_ignore[i];
        form_data = form_data.replace(new RegExp("\\b" + field + "=[^&]*"), '');
      }
      return form_data;
    }

    // Before unload, confirm navigation if any field has been edited
    $(window).on("beforeunload", function (e) {
      var rv;
      if (on_submit) {
        $("form").each(function () {
          var $form = $(this);
          var old_state = strip($form.data('old_state'));
          var new_state = strip($form.serialize());
          if (new_state != old_state) {
            rv = "Êtes vous sure de fermer l'application ?";
          }
        });
        return rv;
      } else return rv;
    });
  }

// I call it at the end of the on-load handler:
  $(function () {
    confirm_nav();
  });
}(jQuery));

(function ($) {
  Drupal.behaviors.budget = {
    attach: function (context, settings) {

      $("input").on('click', function () {
        if ($(this).val() == "") {
          $(this).focus().val("-");
        }
      });
      $("input").bind("keyup change", function () {
        var total_saisir = 0;
        if ($(this).hasClass("q_saisir")) {
          var temp = $(this).closest("td").find('.q_consulter').text();
          if ($.isNumeric(temp)) {
            var totalEcart = parseFloat(temp) - parseFloat($(this).val());
            $(this).closest("td").find('.ecart').text(totalEcart.toFixed(2));
          }
          total_saisir = 0;
          $(this).closest("tr").find('input.q_saisir').each(function () {
            if ($.isNumeric($(this).val())) {
              total_saisir += parseFloat($(this).val());
            }
          });
          $(this).closest("tr").find('.total_saisir').text(total_saisir);
        }
        if ($(this).hasClass("q_budget")) {
          total_saisir = 0;
          $(this).closest("tr").find('input.q_budget').each(function () {
            if ($.isNumeric($(this).val())) {
              total_saisir += parseFloat($(this).val());
            }
          });
          $(this).closest("tr").find('.total_budget').text(total_saisir);
        }
      });
    }
  };

}(jQuery));

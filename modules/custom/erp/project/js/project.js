(function ($) {
  Drupal.behaviors.project = {
    attach: function (context, settings) {
      if (!$.isEmptyObject(settings.erp)) {
        if (!$.isEmptyObject(settings.erp.activite)) {
          let supra = [];
          $.each(settings.erp.activite, function (key, term) {
            if (!$.isEmptyObject(term.field_activite.und)) {
              supra[term.tid] = term.field_activite.und[0].tid;
            }
          });
          $(".field-name-field-projet-supra select").on("change", function () {
            let activite = supra[$(this).val()];
            $(".field-name-field-activite select").val(activite);
            $(".field-name-field-projet-phase select option").show();
          });
        }
        if (!$.isEmptyObject(settings.erp.pharse)) {
          let pharse = [];
          $.each(settings.erp.pharse, function (key, term) {
            if (!$.isEmptyObject(term.field_project_phase.und)) {
              pharse[term.tid] = term.field_project_phase.und[0].value;
            }
          });
          $(".field-name-field-project-etape select").on("change", function () {
            let step = pharse[$(this).val()];
            $(".field-name-field-project-phase select").val(step);
          });
        }
      }

      function calculate_general() {
        let net_mw_part_eolfi = null;
        let net_mw = null;
        if ($(".field-name-field-potentiel-installe").length) {
          net_mw_part_eolfi = parseFloat($(".field-name-field-eolfi-ownership input").val()) / 100 *
            parseFloat($(".field-name-field-potentiel-installe input").val());
        }
        if ($(".field-name-field-total-mw").length) {
          net_mw_part_eolfi = parseFloat($(".field-name-field-eolfi-ownership input").val()) / 100 *
            parseFloat($(".field-name-field-total-mw input").val());
        }
        if ($.isNumeric(net_mw_part_eolfi)) {
          $(".field-name-field-owned-net-mw input").val(net_mw_part_eolfi.toFixed(2));
          net_mw = parseFloat($(".field-name-field-succes-rate input").val()) / 100 * net_mw_part_eolfi;
          $(".field-name-field-developped-net-mw input").val(net_mw.toFixed(2));
        }
      }

      $(".field-name-field-potentiel-installe input,.field-name-field-succes-rate input,.field-name-field-eolfi-ownership input,.field-name-field-owned-net-mw input").on("change keyup", function () {
        calculate_general();
      });

      function calculate_total_mw() {
        let p = $(".field-name-field-puissance-nominale input").val();
        let n = $(".field-name-field-nom-machine input").val();
        if ($.isNumeric(p) && $.isNumeric(p)) {
          let total = p * n;
          $(".field-name-field-total-mw input").val(total.toFixed(2));
        }
      }

      $(".field-name-field-nom-machine input,.field-name-field-puissance-nominale input").on("change keyup", function () {
        calculate_total_mw();
      });
      $(".field-name-field-puissance-nominale-machine input").on("change keyup", function () {
        $(".field-name-field-puissance-nominale input").val($(this).val());
        calculate_total_mw();
      });

      function calculate_choix_machine() {
        var hauteur_bout_pale_max = parseFloat($(".field-name-field-hauteur-moyeu-max input").val()) + parseFloat($(".field-name-field-diametre-max input").val())/2;
        if($.isNumeric(hauteur_bout_pale_max)){
          $(".field-name-field-hauteur-bout-pale-max input").val(hauteur_bout_pale_max.toFixed(2));
        }

        var hauteur_bout_pale = parseFloat($(".field-name-field-hauteur-moyeu input").val()) + parseFloat($(".field-name-field-diametre input").val())/2;
        if($.isNumeric(hauteur_bout_pale)){
          $(".field-name-field-hauteur-bout-pale input").val(hauteur_bout_pale.toFixed(2));
        }

        var quote_part_totale = parseFloat($(".field-name-field-quote-part-s3r-k-mw input").val()) * parseFloat($(".field-name-field-developped-net-mw input").val());
        if($.isNumeric(quote_part_totale)){
          $(".field-name-field-quote-part-totale input").val(quote_part_totale.toFixed(2));

          var raccordement = parseFloat($(".field-name-field-tech-raccordement input").val()) + quote_part_totale;
          if($.isNumeric(raccordement)){
            $(".field-name-field-cout-total input").val(raccordement.toFixed(2));
          }
        }
      }

      $(".field-name-field-hauteur-moyeu-max input,.field-name-field-diametre-max input,.field-name-field-hauteur-moyeu input,.field-name-field-diametre input,.field-name-field-quote-part-s3r-k-mw input, .field-name-field-tech-raccordement input").on("change keyup", function () {
        calculate_choix_machine();
      });

      // button changement phase et passer au spv
      $("#edit-button-spv,#edit-button-phase").hide();
      $(".field-name-field-projet-phase select").on("change", function () {
        $("#edit-button-phase").show();
      });
      $(".field-name-field-societe select").on("change", function () {
        $("#edit-button-spv").show();
      });

      /*
      localisation project
       */
      function replace(context,features) {
        if (context[0] !== undefined) {
          $(".field-name-field-code-departement input").val(context[0]);
        }
        if (context[1] !== undefined) {
          $(".field-name-field-department input").val(context[1]);
        }
        if (context[2] !== undefined) {
          $(".field-name-field-projet-region input").val(context[2]);
        }

        $(".field-name-field-commun input").val(features[0].properties.city);
        if (features[0].properties.population !== undefined) {
          let population = features[0].properties.population;
          $(".field-name-field-projet-num-habitant input").val(parseFloat(population)*1000);
        }
      }
      function localisation(geo_name){
        let type = "municipality";
        let context = false;
        let data = false;
        let dataAPI = "https://api-adresse.data.gouv.fr/search/?type="+type+"&q=" + geo_name;
        $.getJSON(dataAPI)
          .done(function (data) {
            if(data.features[0] == undefined){
              dataAPI = "https://api-adresse.data.gouv.fr/search/?type=street&q=" + geo_name;
              $.getJSON(dataAPI)
                .done(function (data) {
                  geo_name =  data.features[0].properties.postcode + " " + data.features[0].properties.city;
                  dataAPI = "https://api-adresse.data.gouv.fr/search/?type="+type+"&q=" + geo_name;
                  $.getJSON(dataAPI)
                    .done(function (data) {
                      context = data.features[0].properties.context.split(", ");
                      replace(context, data.features);
                    });
                });
            }else {
              context = data.features[0].properties.context.split(", ");
              replace(context, data.features);
            }
          });
      }
      $(".field-name-field-geo .geolocation-address input").on("change", function () {
        let geo = $(this).val();
        localisation(geo);
      });
      $(".field-name-field-geo .geolocation-address .geolocation-address-geocode").on("click", function () {
        let geo_name = $(".field-name-field-geo .geolocation-address input").val();
        localisation(geo_name);
      });

      /*Solar*/
      function production_mwh(){
        let total = parseFloat($(".field-name-field-production-annuelle-heures input").val()) *
                    parseFloat($(".field-name-field-potentiel-installe input").val());
        if($.isNumeric(total)){
          $(".field-name-field-production-annuelle-mwh input").val(total.toFixed(2));
        }
      }

      function equivalent_foyer() {
        let var_equivalent_foyer = settings.solar.equivalent_foyer;
        let val = parseFloat($(".field-name-field-production-annuelle-mwh input").val()) / var_equivalent_foyer;
        if($.isNumeric(val)){
          $(".field-name-field-equivalent-foyer input").val(val.toFixed(2));
        }
      }
      function equivalent_hahbitant(){
        let var_equivalant_habitant = settings.solar.equivalant_habitant;
        let val = parseFloat($(".field-name-field-equivalent-foyer input").val())*var_equivalant_habitant;
        if($.isNumeric(val)){
          $(".field-name-field-equivalent-hahbitant input").val(val.toFixed(2));
        }
      }
      function tonnes_co2(){
        let var_tonnes_co2 = settings.solar.co2_evite;
        let val = parseFloat($(".field-name-field-production-annuelle-mwh input").val())*var_tonnes_co2;
        if($.isNumeric(val)){
          $(".field-name-field-tonnes-co2-evitee input").val(val.toFixed(2));
        }
      }

      function depotpcdp() {
        let extract_date = $(".field-name-field-plan-depot input").val().split("/");
        let d = new Date(extract_date.reverse().join("-"));
        d.setMonth(d.getMonth() + 12);
        $(".field-name-field-pcdp-obtention input").val(d.toLocaleDateString());

        d.setMonth(d.getMonth() + 3);
        $(".field-name-field-pcdp-purge input").val(d.toLocaleDateString());

        d.setMonth(d.getMonth() - 3 + 4);
        $(".field-name-field-obtention-ptf input").val(d.toLocaleDateString());

        d.setMonth(d.getMonth() -4 + 10);
        $(".field-name-field-solar-closing input").val(d.toLocaleDateString());
      }

      function periode_ao_cre() {
        let extract_date = $(".field-name-field-periode-ao-cre input").val().split("/");
        let d = new Date(extract_date.reverse().join("-"));
        d.setMonth(d.getMonth() + 2);
        $(".field-name-field-obtention-tarif input").val(d.toLocaleDateString());

        d.setMonth(d.getMonth() - 2 + 10);
        $(".field-name-field-solar-mes input").val(d.toLocaleDateString());
      }

      function irradiation() {
        let irradiation_horizontale = $(".field-name-field-irradiation-horizontale input").val();
        if(settings.solar != undefined) {
          let max = settings.solar.max_irradiation;
          let min = settings.solar.min_irradiation;
          let irradia = irradiation_horizontale * (1 - (5 - max / min) / (1 - max / min)) / min + (5 - max / min) / (1 - max / min);
          if ($.isNumeric(irradia)) {
            $(".field-name-field-irradiation input").val(irradia.toFixed(2));
          }
        }
      }

      function potentiel() {
        let potentiel_installe = $(".field-name-field-potentiel-installe input").val();
        if(settings.solar != undefined) {
          let max = settings.solar.max_potentiel;
          let min = settings.solar.min_potentiel;
          let installe = potentiel_installe * (1 - (5 - max / min) / (1 - max / min)) / min + (5 - max / min) / (1 - max / min);
          if ($.isNumeric(installe)) {
            $(".field-name-field-potentiel-installee input").val(installe.toFixed(2));
          }
        }
      }

      function aeorodrome(){
        let val = 0;
        if($(".field-name-field-aeorodrome-3-km input").prop("checked")){
          val = -2;
        }
        $(".field-name-field-aerodrome input").val(val);
      }

      function sncf(){
        let val = 0;
        if($(".field-name-field-solar-proprietaire input").val() == "SNCF"){
          val = 2;
        }
        $(".field-name-field-sncf input").val(val);
      }

      function priorite(){
        let sub_step_select = $(".field-name-field-solar-etape-sub select").val();
        if(settings.solar != undefined) {
          let val = settings.solar.sub_step[sub_step_select];
          if($.isNumeric(val)){
            $(".field-name-field-priorite input").val(val);
          }
        }
      }
      function eligibility_case(){
        let eligibility = $(".field-name-field-cas-eligibilite select").val();
        if(settings.solar != undefined) {
          if (settings.solar.eligibility_case != undefined) {
            let val = settings.solar.eligibility_case[eligibility];
            if($.isNumeric(val)){
              $(".field-name-field-ao-cre input").val(val);
            }
          }
        }
      }
      function compatibility_urbanism(){
        let urbanism = $(".field-name-field-urbanisme-compatibilite select").val();
        if(settings.solar != undefined) {
          if (settings.solar.compatibility_urbanism != undefined) {
            let val = settings.solar.compatibility_urbanism[urbanism];
            if($.isNumeric(val)){
              $(".field-name-field-urba input").val(val);
            }
          }
        }
      }

      function note_global(){
        let note = null;
        $("#group_notes input").each(function () {
          let value = $(this).val();
          if(value != '' && $.isNumeric(value)){
            note += parseFloat(value);
          }
        });
        if($.isNumeric(note)){
          $(".field-name-field-projet-notes input").val(note.toFixed(2));
        }
      }
      $(".field-name-field-plan-depot input").on("dp.change", function () {
        depotpcdp();
      });
      $(".field-name-field-periode-ao-cre input").on("dp.change", function () {
        periode_ao_cre();
      });
      $(".field-name-field-aeorodrome-3-km input").on("change", function () {
        aeorodrome();
      });
      $(".field-name-field-solar-proprietaire input").on("change", function () {
        sncf();
      });

      $(".field-name-field-production-annuelle-heures input, .field-name-field-potentiel-installe input").on("change keyup",function () {
        production_mwh();
        equivalent_foyer();
        tonnes_co2();
        equivalent_hahbitant();
        note_global();
      });
      $(".field-name-field-irradiation-horizontale input").on("change keyup",function () {
        irradiation();
        note_global();
      });
      $(".field-name-field-potentiel-installe input").on("change keyup",function () {
        potentiel();
        note_global();
      });

      $(".field-name-field-solar-etape-sub select").on("change",function () {
        priorite();
        note_global();
      });
      $(".field-name-field-cas-eligibilite select").on("change",function () {
        eligibility_case();
        note_global();
      });
      $(".field-name-field-urbanisme-compatibilite select").on("change",function () {
        compatibility_urbanism();
        note_global();
      });
      irradiation();
      potentiel();
      priorite();
      eligibility_case();
      compatibility_urbanism();
      calculate_general();
      note_global();
    }
  };
}(jQuery));

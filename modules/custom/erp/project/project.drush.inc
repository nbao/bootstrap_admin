<?php

/**
 * Implementation of hook_drush_command().
 */
function project_drush_command() {
  $items['update-reference-save'] = [
    'callback' => 'drush_update_reference_save',  // Callback function
    'description' => 'This is my update table reference save.',
    'aliases' => ['upp'],
    'arguments' => [
      'year' => 'Run by Year',
    ],
    'examples' => [
      'drush upp 2018' => 'drush upp Update reference save.',
    ],
  ];

  return $items;
}

/**
 * Drush command logic.
 * drush_[COMMAND_NAME]().
 */
function drush_update_reference_save($year = NULL) {
  if (empty($year)) {
    $year = date('Y');
  }
  drush_print('Update reference save. Argument:' . $year);
}

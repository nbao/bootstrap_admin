(function ($) {
  Drupal.behaviors.rapprochement = {
    attach: function (context, settings) {
      /* detect format bank */
      $("#field_bancaire_import").on("change", function () {
        var filename = $(this).val();
        if (filename.includes("SG")) {
          $("#field_bancaire_format").val("SG");
        }
        if (filename.includes("LCL")) {
          $("#field_bancaire_format").val("LCL");
        }
      });
      /*
       Passer écriture sup
       */
      $("input.item_id[type=checkbox]").on("click", function () {
        var n = $("input.item_id:checked").length;
        $(".view-id-paiement_lignes form .btn-primary").remove();
        if (n) {
          var list_item = [];
          $("input.item_id:checked").each(function () {
            list_item.push($(this).data("item_id"));
          });
          var searchParams = new URLSearchParams(window.location.search);

          var href = "/paiement/sup/" + list_item.join("+");
          if (searchParams.has("field_paiement_site_target_id")) {
            var site = searchParams.get("field_paiement_site_target_id");
            href += "?field_paiement_site_target_id=" + site;
          }
          var sup = $("<a class='btn btn-primary' href='" + href + "' target='_blank'>Passer écriture sup</a>");

          $(".view-id-paiement_lignes form").append(sup);
        }
      });
      /*
      rapprochement bancaire
       */
      $(".auto").on("click", function () {
        $(".draggable").each(function () {
          var montant = convertMontant($(this).find(".montant").text());
          var that = $(this);
          $(".view-display-id-transaction tbody tr").each(function () {
            var montant_destination = convertMontant($(this).find(".montant").text());
            if (montant_destination === montant) {
              $(this).after(that);
            }
          });
        });
        check_transaction_validate();
      });
      if ($(".view-display-id-transaction table tbody").length) {
        $(".view-display-id-transaction table tbody").sortable({
          revert: true
        });
      }
      if ($(".draggable").length) {
        $(".draggable").draggable({
          helper: "clone",
          revert: false,
          cursor: 'move',
          start: function () {
            var flag = true;
            if (flag) {
              var montant_source = convertMontant($(this).find(".montant").text());
              $(".droppable").find(".montant").text(function () {
                if (montant_source === convertMontant($(this).text())) {
                  if ($(this).closest("tr").hasClass("bg-success")) {
                    $(this).closest("tr").addClass("bg-warning");
                  } else {
                    $(this).closest("tr").addClass("bg-success");
                  }
                  flag = false;
                }
              });
            }
          },
        });
      }
      if ($(".droppable").length) {
        $(".droppable").droppable({
          accept: ".draggable",
          drop: function (event, ui) {
            $(this).after(ui.helper.context);
            check_transaction_validate();
          }
        });
      }
      $(".remove").on("click", function () {
        $(this).closest("tr").appendTo("#approach");
        check_transaction_validate();
      });

      //activer button annuler transaction
      $("td .item_id").on("change", function () {
        var montant = 0;
        $("td .item_id:checked").each(function () {
          var currency = $(this).closest("tr").find(".montant").text();
          montant += convertMontant(currency);
        });
        if (montant === 0) {
          $(".btn-warning").removeAttr("disabled");
        } else {
          $(".btn-warning").attr("disabled", "disabled");
        }
      });

      if ($(".view-display-id-rapprocher form table").length) {
        var header = $(".view-display-id-rapprocher form table thead tr").clone();
        var footer = $("<tfoot/>").append(header);
        footer.find("th").html("");
        footer.find("th").last().addClass("text-right").html("Total : <b class='total'></b>");
        $(".view-display-id-rapprocher form table").append(footer);

        $(".vbo-select,.vbo-table-select-all").on("change", function () {
          var total = false;
          $.each($("input.vbo-select:checked"), function () {
            var montant = convertMontant($(this).closest("tr").find("td").last().text());
            if ($.isNumeric(montant)) {
              total += parseFloat(montant);
            }
          });
          if (total) {
            $(".total").text(total.toLocaleString("fr-FR"));
          }
        });

      }

      function check_transaction_validate() {
        $(".view-display-id-transaction tr.bg-success").removeClass("bg-success");
        $(".view-display-id-transaction tbody tr.droppable").each(function () {
          var that = $(this);
          var item_id = $(this).find(".item_id").data("item_id");
          if ($(this).next().hasClass("draggable")) {
            var montant = convertMontant(that.find(".montant").text());
            var temp_montant = 0;
            that.nextAll().each(function () {
              temp_montant += convertMontant($(this).find(".montant").text());
              $(this).find("input").val(item_id);
              temp_montant = parseFloat(temp_montant.toFixed(2));
              if (temp_montant === montant) {
                that.addClass("bg-success");
              }
              if ($(this).next().hasClass("draggable")) {
                return;
              }
            });
          }
        });
      }

      function convertMontant(currency) {
        currency = currency.replace(" ", "").replace(",", ".");
        var number = Number(currency.replace(/[^0-9\.-]+/g, ""));
        return  parseFloat(number.toFixed(2));
      }
    }
  };
}(jQuery));

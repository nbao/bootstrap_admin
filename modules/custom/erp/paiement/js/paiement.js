(function ($) {
  Drupal.behaviors.paiement = {
    attach: function (context, settings) {
      $("#edit-field-type-paiement-value").prop("required", true);
      $("#edit-field-mode-paiement-value").on("change", function () {
        $("#edit-field-type-paiement-value").removeAttr("required");
        if ($(this).val() === "VRM") {
          $("#edit-field-type-paiement-value").prop("required", true);
        }
      });
      $("#views-form-paiements-facture #edit-operation").on("change", function () {
        if ($(this).val() === "action::erp_valider_paiement_action") {
          if ($("#edit-field-mode-paiement-value").val() === "VRM" && $("#edit-field-type-paiement-value").val() === "All") {
            alert("Type Paiement est obligatoire");
            $("#edit-field-type-paiement-value").prop("required", true).focus();
          }
        }
      });
      /*
      View paiement facture
       */
      $(".views-field-field-compte-rib, .views-field-status").each(function () {
        var urlParams = new URLSearchParams(window.location.search);
        var isNDF = false;
        if(urlParams.has('type') && urlParams.get('type') == "NDF" ){
          isNDF = true;
        }
        if ($.trim($(this).text()) === "") {
          let type = $(this).closest("tr").find(".views-field-field-mode-paiement").text();
          if($.trim(type) == "Virement" && !isNDF){
            $(this).closest("tr").find(".views-field-views-bulk-operations div").remove();
          }
        }
      });

      if ($(".field-name-field-compte-banque input").length) {
        if (!$.isEmptyObject(settings.erp.societe)) {
          $(".field-name-field-paiement-site select").on("change", function () {
            var id = $(this).val();
            $("#bank").html("");
            $(".field-name-field-compte-banque input").val("");
            //$(".field-name-field-compte-bic input").val("");
            if (!$.isEmptyObject(settings.erp.societe[id])) {
              var check = [];
              $.each(settings.erp.societe[id], function (key, value) {
                $("#bank").append($("<option>",
                  {
                    value: key,
                    text: value
                  }));
                check.push(key);
              });
              if (check.length === 1) {
                $(".field-name-field-compte-banque input").val(check[0]);
                //$(".field-name-field-compte-bic input").val(settings.erp.societe[id][check[0]]);
              }
            }
          });
          /* auto search compte banque
          $(".field-name-field-compte-banque input").on("change", function () {
            var id = $(".field-name-field-paiement-site select").val();
            if (!$.isEmptyObject(settings.erp.societe[id])) {
              var check = $(this).val();
              $(".field-name-field-compte-bic input").val(settings.erp.societe[id][check]);
            }
          });*/
        }
      }
      $(".entity-reference-view-widget-select").on("click", function () {
        var montant_paye = parseFloat($.trim($(this).closest("tr").find(".views-field-field-paiemnt-mt-payer").text().replace(" ", "").replace(",", ".")));
        if ($.isNumeric(montant_paye)) {
          montant_paye = Math.abs(montant_paye).toFixed(2);
          $(".field-name-field-montant input").val(montant_paye).attr("type", "number").attr("step", 0.01).attr("max", montant_paye).attr("min", 0.01);
        }
      });

      /*
      chagement code tresor by nature
       */
      $(".simpler-select").on("change", function () {
        $(this).closest(".form-item").find(".simpler-select").on("change", function () {
          var code_tresors = false;
          if (!$.isEmptyObject(settings.erp.code_tresors)) {
            code_tresors = settings.erp.code_tresors;
          }
          if (code_tresors[$(this).val()] !== undefined) {
            $(".form-item-field-code-tresor-und select").val(code_tresors[$(this).val()]);
          }
        });
      });

      //replace number , to .
      $(".field-type-number-decimal input.form-text").bind("keyup change", function () {
        $(this).val($(this).val().replace(",", ".").replace(" ", ""));
      });

      /* view gestion de facture a payer*/
      if($(".montant-invoice").length) {
        var total = 0;
        $(".montant-invoice").each(function () {
          var amount =$.trim($(this).text());
          if (amount !== "") {
            amount = amount.replace(",", ".").replace(" ", "");
            if($.isNumeric(amount)){
              total += parseFloat(amount);
            }
          }
        });
        $("#montant-invoice").text(
          new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(total)
        );
      }

    }
  };
}(jQuery));

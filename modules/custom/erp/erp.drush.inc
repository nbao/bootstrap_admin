<?php

/**
 * Implementation of hook_drush_command().
 */
function erp_drush_command() {
  $items['alert-daily'] = [
    'callback' => 'drush_alert_dailly',  // Callback function
    'description' => 'This is cron send email alert.',
    'aliases' => ['alert'],
    'examples' => [
      'drush alert' => 'drush send alert.',
    ],
  ];

  return $items;
}

/**
 * Drush command logic.
 * drush_[COMMAND_NAME]().
 */
function drush_alert_dailly() {
  // run as admin
  global $user;
  $user = user_load(1);
  _erp_cron_delete_conges_refuse();
  _erp_cron_send_email_commande_facture();
}

function _erp_cron_delete_conges_refuse() {
  $conges_results = views_get_view_result('field_conges', 'conges_refuse');
  if (!empty($conges_results)) {
    foreach ($conges_results as $result) {
      $item_ids[] = $result->item_id;
    }
    entity_delete_multiple('field_collection_item', $item_ids);
  }
}

function _erp_cron_send_email_commande_facture() {
  // commande
  $view_name = 'dashboard';
  $comandes_results = views_get_view_result($view_name, 'tous_commandes');
  $factures_results = views_get_view_result($view_name, 'tous_factures');
  $field_validation = 'field_field_order_validation';
  $body = [];
  foreach ($comandes_results as $result) {
    $uid = FALSE;
    if ($result->$field_validation[0]['raw']['value'] == 'à Valider:CDP') {
      if (!empty($result->field_field_projet_chef_2[0])) {
        $uid = $result->field_field_projet_chef_2[0]['raw']['uid'];
      }
      if (!empty($result->field_field_projet_chef[0])) {
        $uid = $result->field_field_projet_chef[0]['raw']['uid'];
      }
    }
    if ($result->$field_validation[0]['raw']['value'] == 'à Valider:RES' && !empty($result->field_field_projet_responsable[0])) {
      $uid = $result->field_field_projet_responsable[0]['raw']['uid'];
    }
    if ($result->$field_validation[0]['raw']['value'] == 'à Valider:DG' && !empty($result->field_field_projet_directeur[0])) {
      $uid = $result->field_field_projet_directeur[0]['raw']['uid'];
    }
    if ($uid) {
      $row = [
        'title' => l($result->node_title, 'node/' . $result->nid, ['absolute' => TRUE]),
        'projet' => !empty($result->field_field_order_projet) ? render($result->field_field_order_projet[0]['rendered']) : '',
        'status' => render($result->field_field_order_validation[0]['rendered']),
      ];
      $body[$uid]['commandes'][] = implode(' - ', $row);
    }
  }
  $field_validation = 'field_field_invoice_validation_status';
  foreach ($factures_results as $result) {
    $uid = FALSE;
    if ($result->$field_validation[0]['raw']['value'] == 'à Valider:CDP') {
      if (!empty($result->field_field_projet_chef_2[0])) {
        $uid = $result->field_field_projet_chef_2[0]['raw']['uid'];
      }
      if (!empty($result->field_field_projet_chef[0])) {
        $uid = $result->field_field_projet_chef[0]['raw']['uid'];
      }
      if( !empty($result->field_field_invoice_chef_projet[0]["raw"]["uid"])){
        $uid = $result->field_field_invoice_chef_projet[0]["raw"]["uid"];
      }
    }
    if ($result->$field_validation[0]['raw']['value'] == 'à Valider:RES' && !empty($result->field_field_projet_responsable[0])) {
      $uid = $result->field_field_projet_responsable[0]['raw']['uid'];
    }
    if ($result->$field_validation[0]['raw']['value'] == 'à Valider:DG' && !empty($result->field_field_projet_directeur[0])) {
      $uid = $result->field_field_projet_directeur[0]['raw']['uid'];
    }
    if ($uid) {
      $row = [
        'title' => l($result->node_title, 'node/' . $result->nid, ['absolute' => TRUE]),
        'projet' => !empty($result->field_field_invoice_projet) ? render($result->field_field_invoice_projet[0]['rendered']) : '',
        'status' => render($result->field_field_order_validation[0]['rendered']),
      ];
      $body[$uid]['factures'][] = implode(' - ', $row);
    }
  }
  //fix default
  $search = 'http://default/';
  $replace = 'https://erp.eolfi.com/';
  if (!empty($body)) {
    $salution = "Bonjour ";
    foreach ($body as $uid => $message) {
      if (empty($messages[$uid])) {
        $messages[$uid][0] = $salution;
      }
      if (!empty($message['commandes'])) {
        $messages[$uid][] = "Vous avez (" . count($message['commandes']) . ") commande(s) en attente de votre validation\n<br/>";
        $messages[$uid][] = str_replace($search, $replace, theme('item_list', ['items' => $message['commandes']]));
      }
      if (!empty($message['factures'])) {
        $messages[$uid][] = "Vous avez (" . count($message['factures']) . ") facture(s) en attente de votre validation\n<br/>";
        $messages[$uid][] = str_replace($search, $replace, theme('item_list', ['items' => $message['factures']]));
      }
    }
  }
  foreach ($messages as $uid => $message) {
    // Send out the e-mail.
    $collaborateur = user_load($uid);
    $message[0] .= $collaborateur->name . ',<br/>';
    $email = $collaborateur->name . ' <' . $collaborateur->mail . '>';
    drush_print( $email);
    $params = [
      'subject' => 'Message automatiquement ERP',
      'body' => $message,
    ];
    drupal_mail('erp', 'alert', $email, language_default(), $params);
  }
}

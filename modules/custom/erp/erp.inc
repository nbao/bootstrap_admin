<?php

function _generate_sign($uid, $date, $item_id=FALSE) {

  $field_name = 'field_ticket_resto_sign' ;
  $field_collection_name = 'field_ticket_restaurant';
  $element = [ '#field_name' => $field_name ];
  $form_state = [];
  $element = sign_field_widget_process($element, $form_state);

  return '<form method="post" class="signature" >
          <div class="col-md-9">
              ' . render($element) . '
          </div>
          <div class="col-md-1">
              <button id="' . $uid . '" class="btn btn-success center-block signature-submit" data-qty="20"  data-num="" data-uid="' . $uid . '" data-date="' . $date . '" data-item_id="'.$item_id.'">OK</button>
           </div>
          </form>';
}

function ticket_restaurant_signature() {
  $data = $_POST;
  if (!empty($data['uid'])) {
    $user = user_load($data['uid']);

    $date = date("Y-m", strtotime($data["date"]));
    if ($date == date("Y-m")) {
      $data["date"] = date("Y-m-d");
    }
    else {
      $data["date"] = date("Y-m-d", strtotime($data["date"]));
    }
    // field collection items.
    if(empty($data['item_id'])){
      $field_collection_item = entity_create('field_collection_item', ['field_name' => 'field_ticket_restaurant']);
      $field_collection_item->setHostEntity('user', $user);
    }else{
      $field_collection_item = field_collection_item_load($data['item_id']);
    }

    $field_collection_item->field_ticket_resto_date[LANGUAGE_NONE][] = ['value' => $data["date"]];
    if(!empty($data["qty"])){
      $field_collection_item->field_ticket_resto_qty[LANGUAGE_NONE][] = ['value' => $data["qty"]];
    }
    if(!empty($data["num"])){
      $field_collection_item->field_ticket_resto_num[LANGUAGE_NONE][] = ['value' => $data["num"]];
    }
    if(!empty($data["valide"])){
      $field_collection_item->field_ticket_valide[LANGUAGE_NONE] = [['value' => TRUE]];
      $field_collection_item->field_societe = $user->field_societe;
    }
    if(!empty($data['sign'])){
      $instance = field_info_instance('field_collection_item', 'field_ticket_resto_sign', 'field_ticket_restaurant');
      $field = field_info_field('field_ticket_resto_sign');
      $uri = file_field_widget_uri($field, $instance);
      $files_save = _sign_widget_image_to_file($uri, [['signature'=>$data['sign']]]);
      if($files_save){
        $field_collection_item->field_ticket_resto_sign[LANGUAGE_NONE][0]["fid"] = current($files_save)->fid;
      }
      $field_collection_item->field_societe = $user->field_societe;
      $field_collection_item->field_ticket_resto_reception[LANGUAGE_NONE][] = ['value' => date('Y-m-d')];
    }
    $field_collection_item->revision = FALSE;
    $field_collection_item->save();
    $societe = 'EOL';
    if (!empty($user->field_societe['und'][0]['tid'])) {
      $term = taxonomy_term_load($user->field_societe['und'][0]['tid']);
      $societe = $term->name;
    }
    $stock_load = taxonomy_get_term_by_name('stock', 'ticket');
    $stock = end($stock_load);
    if(!empty($stock->field_stock[LANGUAGE_NONE])){
      foreach ($stock->field_stock[LANGUAGE_NONE] as $delta => $field_stock) {
        if ($field_stock['first'] == $societe) {
          $stock->field_stock['und'][$delta]['second'] -= $data["qty"];
          taxonomy_term_save($stock);
          break;
        }
      }
    }

  }
  if (isset($data['sign'])) {
    unset($data['sign']);
  }
  return drupal_json_encode($data);
}

(function ($) {
  Drupal.behaviors.commande = {
    attach: function (context, settings) {
      function cal_ttc(mt_ht, line_taxe) {
        var tax = 0;
        var taxe = [];
        if (!$.isEmptyObject(settings.erp.term_taxe)) {
          $.each(settings.erp.term_taxe, function (key, term) {
            taxe[term.tid] = term.field_taux.und["0"].value;
          });
          if (typeof taxe[line_taxe] !== "undefined") {
            tax = taxe[line_taxe];
          }
        }
        return parseFloat(mt_ht) * (parseFloat(tax) + 1);
      }

      function update_ecart() {
        var diff = 0,
            diff_ttc = 0;
        if ($(".field-name-field-lines-total-ht input").val() != "" && $(".field-name-field-echeance-total-ht input").val() != "") {
          diff = parseFloat($(".field-name-field-lines-total-ht input").val()) - parseFloat($(".field-name-field-echeance-total-ht input").val());
          if (typeof diff !== "undefined"){
            $(".field-name-field-echeances-ecart-ht input").val(diff.toFixed(2));
          }
        }
        if ($(".field-name-field-lines-total-ttc input").val() != "" && $(".field-name-field-echeances-total-ttc input").val() != "") {
          diff_ttc = parseFloat($(".field-name-field-lines-total-ttc input").val()) - parseFloat($(".field-name-field-echeances-total-ttc input").val());
          if (typeof diff_ttc !== "undefined"){
            $(".field-name-field-echeances-ecart-ttc input").val(diff_ttc.toFixed(2));
          }
        }
        diff = Math.abs(diff);
        diff_ttc = Math.abs(diff_ttc);
        $("#edit-submit").prop("disabled", false);
        $("#edit-devalider").prop("disabled", false);
        $("#edit-valider").prop("disabled", false);
        if (diff > 0.1 || diff_ttc > 0.1) {
          $("#edit-submit").prop("disabled", true);
          $("#edit-devalider").prop("disabled", true);
          $("#edit-valider").prop("disabled", true);
        }
      }

      function update_mt(source, sum_to) {
        var sum = 0;
        $(source).each(function () {
          if ($(this).val() != "") {
            sum += parseFloat($(this).val());
          }
        });
        $(sum_to).val(sum.toFixed(2));
        update_ecart();
      }

      function update_first_echeance(edit = false) {
        var line_taxe = $(".field-linges-taxe select").first().val();
        var duree = $(".field-name-field-contrat-duree input").val();
        var frequence = $(".field-name-field-contrat-frequence select").val();
        var num_echeances = Math.ceil(parseInt(duree) / parseInt(frequence));

        var mt_ht = $(".field-name-field-lines-total-ht input").val();
        var echeances_ht = mt_ht / num_echeances;
        var mt_ttc = $(".field-name-field-lines-total-ttc input").val();
        var echeances_ttc = mt_ttc / num_echeances;

        if (!edit) {
          if (frequence > 0 && duree > 0) {
            $(".field-name-field-order-echeances table tbody tr").each(function () {
              $(this).find(".field-echeances-mt-ht input").val(echeances_ht.toFixed(2));
              $(this).find(".field-echeances-mt-ttc input").val(echeances_ttc.toFixed(2));
              $(this).find(".field-linges-taxe select").val(line_taxe);
            });
          } else {
            $(".field-echeances-mt-ht input").first().val(mt_ht);
            $(".field-echeances-mt-ttc input").first().val(mt_ttc);
            $(".field-name-field-order-echeances .field-linges-taxe select").first().val(line_taxe);
          }
        }
        update_mt(".field-name-field-echeances-mt-ht input", ".field-name-field-echeance-total-ht input");
        update_mt(".field-name-field-echeances-mt-ttc input", ".field-name-field-echeances-total-ttc input");
      }

      //replace number , to .
      $(".field-type-number-decimal input.form-text").bind("keyup change", function () {
        $(this).val($(this).val().replace(",", ".").replace(" ", ""));
      });


      //Linges
      $(".field-name-field-linges-mt-ht input,.field-name-field-order-linges .field-linges-taxe select").bind("keyup change", function () {
        var mt_ht = parseFloat($(this).closest("tr").find(".field-name-field-linges-mt-ht input").val());
        var line_taxe = parseFloat($(this).closest("tr").find(".field-name-field-linges-taxe select").val());
        var mt_ttc = cal_ttc(mt_ht, line_taxe);
        $(this).closest("tr").find(".field-name-field-linges-mt-ttc input").val(mt_ttc.toFixed(2));
        update_mt(".field-name-field-order-linges .field-name-field-linges-mt-ht input", ".field-name-field-lines-total-ht input");
        update_mt(".field-name-field-order-linges .field-name-field-linges-mt-ttc input", ".field-name-field-lines-total-ttc input");
        if (settings.commande != undefined && settings.commande.variable['is_new']) {
          update_first_echeance();
        }
      });
      $(".field-name-field-linges-taxe select").bind("keyup change", function () {
        var mt_ht = parseFloat($(this).closest("tr").find(".field-name-field-linges-mt-ht input").val());
        var line_taxe = parseFloat($(this).val());
        var mt_ttc = cal_ttc(mt_ht, line_taxe);
        $(this).closest("tr").find(".field-name-field-linges-mt-ttc input").val(mt_ttc.toFixed(2));
      });

      //echeances
      $(".field-name-field-echeances-mt-ht input, .field-name-field-order-echeances .field-name-field-linges-taxe select").bind("keyup change", function () {

        var mt_ht = parseFloat($(this).closest("tr").find(".field-name-field-echeances-mt-ht input").val());
        var line_taxe = parseFloat($(this).closest("tr").find(".field-name-field-linges-taxe select").val());
        var mt_ttc = cal_ttc(mt_ht, line_taxe);
        $(this).closest("tr").find(".field-name-field-echeances-mt-ttc input").val(mt_ttc.toFixed(2));
        update_mt(".field-name-field-echeances-mt-ht input", ".field-name-field-echeance-total-ht input");
        update_mt(".field-name-field-echeances-mt-ttc input", ".field-name-field-echeances-total-ttc  input");
      });
      $(".field-echeances-mt-ttc input").bind("keyup change", function () {
        update_mt(".field-name-field-echeances-mt-ttc input", ".field-name-field-echeances-total-ttc  input");
      });
      $(".field-linges-mt-ttc input").bind("keyup change", function () {
        update_mt(".field-name-field-linges-mt-ttc input", ".field-name-field-lines-total-ttc input");
      });

      //pourcent
      $(".field-name-field-echeances-pourcent input").bind("keyup change", function () {

        var mt_total_ht = parseFloat($(".field-name-field-lines-total-ht input").val());
        var per_cent = $(this).closest("tr").find(".field-name-field-echeances-pourcent input").val() * mt_total_ht / 100;
        $(this).closest("tr").find(".field-name-field-echeances-mt-ht input").val(per_cent.toFixed(2));

        var mt_ht = parseFloat($(this).closest("tr").find(".field-name-field-echeances-mt-ht input").val());
        var line_taxe = parseFloat($(this).closest("tr").find(".field-name-field-linges-taxe select").val());
        var mt_ttc = cal_ttc(mt_ht, line_taxe);
        $(this).closest("tr").find(".field-name-field-echeances-mt-ttc input").val(mt_ttc.toFixed(2));
        update_mt(".field-name-field-echeances-mt-ht input", ".field-name-field-echeance-total-ht input");
        update_mt(".field-name-field-echeances-mt-ttc input", ".field-name-field-echeances-total-ttc input");

      });

      //echeances update date by date order
      $(".field-name-field-contrat-date .form-item-field-contrat-date-und-0-value-date input").bind("change", function () {
        if ($(".field-name-field-echeances-date").length > 1) {
          $(".form-item-field-order-echeances-und-0-field-echeances-date-und-0-value-date input").val($(this).val());
        }
      });

      //echeance show facture
      $(".field-echeances-no-facture .tabledrag-hide .form-type-select.form-group").hide();
      $(".field-echeances-no-facture .tabledrag-hide").show().find(".element-invisible").removeClass("element-invisible").find("+ input").css("margin-left", 0);

      $(".field-name-field-echeance-total-ht,.field-name-field-echeances-total-ttc").hover(function () {
        update_mt(".field-name-field-echeances-mt-ht input", ".field-name-field-echeance-total-ht input");
        update_mt(".field-name-field-echeances-mt-ttc input", ".field-name-field-echeances-total-ttc input");
      });
    }
  };

}(jQuery));

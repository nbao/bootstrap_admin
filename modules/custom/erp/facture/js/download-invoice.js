(function ($, Drupal) {
  Drupal.behaviors.invoice = {
    attach: function (context, settings) {
      function generateZIP(links_invoice, limit) {

        var zip = new JSZip();
        var zipFilename = "facture.zip";
        var count = 0;
        for (let filename in links_invoice) {
          if (filename === "length" || !links_invoice.hasOwnProperty(filename)) {
            continue;
          }
          var links = links_invoice[filename];
          links.forEach(function (url, i) {
            let filename_old = url;
            var split_name_image = filename_old.split("/");
            filename_old = split_name_image.pop();
            var split_filename_extention = filename_old.split(".");
            var ext = split_filename_extention.pop();
            var file_name = filename + "-" + i + "." + ext;
            // loading a file and add it in a zip file
            JSZipUtils.getBinaryContent(url, function (err, data) {
              if (err) {
                throw err; // or handle the error
              }
              zip.file(file_name, data, { binary: true });
              count++;
              if (count == limit) {
                zip.generateAsync({ type: 'blob' }).then(function (content) {
                  saveAs(content, zipFilename);
                });
              }
            });
          });
        }
      }

      $(".download-invoice").on("click", function () {
        var links = [];
        var count = 0;
        $('.images_down').each(function () {
          var title = $(this).data("title");
          if (links[title] === undefined) {
            links[title] = [];
          }
          let src = $(this).find("img").attr("src");
          links[title].push(src);
          count++;
        });
        generateZIP(links, count);
      });
    }
  };

}(jQuery, Drupal));

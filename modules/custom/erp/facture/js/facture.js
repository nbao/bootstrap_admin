(function ($, Drupal) {
  Drupal.behaviors.facture = {
    attach: function (context, settings) {

      //Linges
      $(".field-linges-mt-ht input,.field-linges-taxe select").bind("keyup change", function () {
        var mt_ht = parseFloat($(this).closest("tr").find(".field-linges-mt-ht input").val());
        var line_taxe = parseFloat($(this).closest("tr").find(".field-name-field-linges-taxe select").val());
        var mt_ttc = cal_ttc(mt_ht, line_taxe);
        $(this).closest("tr").find(".field-linges-mt-ttc input").val(mt_ttc.toFixed(2));
        update_mt(".field-linges-mt-ht input", ".field-name-field-facture-total-ht input");
        update_mt(".field-linges-mt-ttc input", ".field-name-field-facture-total-ttc input");
        $(".field-name-field-line-accounts").addClass("bg-danger");
      });
      $(".field-linges-mt-ttc input").bind("keyup change", function () {
        update_mt(".field-linges-mt-ttc input", ".field-name-field-facture-total-ttc input");
        $(".field-name-field-line-accounts").addClass("bg-danger");
      });
      $(".field-comptable-montant input").bind("keyup change", function () {
        ecart();
      });

      $(".field-name-field-facture-total-ttc input").bind("click", function () {
        update_mt(".field-linges-mt-ht input", ".field-name-field-facture-total-ht input");
        update_mt(".field-linges-mt-ttc input", ".field-name-field-facture-total-ttc input");
      });
      $(".field-comptable-compte input").bind("change", function () {
        var val = $(this).val();
        var title = $("#compte_comptable option[value='" + val + "']").attr("label");
        $(this).closest("tr").find(".field-comptable-libelle input").val(title);
      });
      $(".field-name-field-invoice-type select").bind("change", function () {
        if ($(this).val() === "FAC" || $(this).val() === "AVC") {
          $(".field-name-field-invoice-supplier label").first().html("Client <span class='form-required' title='Ce champ est requis.'>*</span>");
        } else {
          $(".field-name-field-invoice-supplier label").first().html("Fournisseur <span class='form-required' title='Ce champ est requis.'>*</span>");
        }
      });
      $(".field-name-field-order-linges table,.get_field_line_accounts").hover(function () {
        update_mt(".field-linges-mt-ht input", ".field-name-field-facture-total-ht input");
        update_mt(".field-linges-mt-ttc input", ".field-name-field-facture-total-ttc input");
      });
      $(".field_facture_ecart_montant").hover(function () {
        ecart();
      });

      function hidden_periode() {
        $(".field-name-field-facture-periode").hide();
        if ($(".field-name-field-invoice-type select").val() === "FAC" || $(".field-name-field-invoice-type select").val() === "AVC") {
          $(".field-name-field-facture-periode").show();
        }
      }

      hidden_periode();
      $(".field-name-field-invoice-type select").on("change", function () {
        hidden_periode();
      });
      function mont_a_payer() {
        if($(".field_invoice_paiement .entityreference-view-widget-checkbox.form-checkbox").length){
          return;
        }
        var type = $(".field-name-field-invoice-type select").val();
        var negative = 1;
        if (type === "FAF" || type === "AVC") {
          negative = -1;
        }
        if ($(".field-name-field-invoice-avoir table input").length) {
          negative = 0;
        }
        var sum = 0;
        $(".field-name-field-linges-mt-ttc input").each(function () {
          if ($.isNumeric($(this).val())){
            sum += parseFloat($(this).val());
          }
        });
        var mt_payer = sum * negative;
        $(".field_paiemnt_mt_payer input").val(mt_payer.toFixed(2));
      }

      $(".field_paiemnt_mt_payer").hover(function () {
        mont_a_payer();
      });

      function update_mt(source, sum_to) {
        var sum = 0;
        $(source).each(function () {
          if ($.isNumeric($(this).val())){
            sum += parseFloat($(this).val());
          }
        });
        $(sum_to).val(sum.toFixed(2));
      }

      function cal_ttc(mt_ht, line_taxe) {
        var tax = 0;
        var taxe = [];
        if (!$.isEmptyObject(settings.erp.term_taxe)) {
          $.each(settings.erp.term_taxe, function (key, term) {
            taxe[term.tid] = term.field_taux.und["0"].value;
          });
          if (typeof taxe[line_taxe] !== undefined) {
            tax = taxe[line_taxe];
          }
        }
        var mt_ttc = parseFloat(mt_ht) * (parseFloat(tax) + 1);
        mont_a_payer();
        return mt_ttc;
      }

      function ecart() {
        var sum_c = 0;
        var sum_d = 0;
        $(".field-comptable-montant input").each(function () {
          if ($.isNumeric($(this).val())) {
            var sens = $(this).closest("tr").find(".field-sens select").val();
            if (sens === "C"){
              sum_c += parseFloat($(this).val());
            }
            if (sens === "D"){
              sum_d += parseFloat($(this).val());
            }
          }
        });
        $(".field-name-field-line-accounts").removeClass("bg-danger");
        var ecart_montant = parseFloat(sum_c - sum_d);
        if(ecart_montant !== 0){
          $(".field_facture_ecart_montant input").val(ecart_montant.toFixed(2));
        }
      }

      $(".form-item-field-condition-paiement-und select").bind("change", function () {
        date_echeance();
      });
      $(".form-item-field-invoice-date-supplier-und-0-value-date input, .form-item-field-invoice-date-reception-und-0-value-date input, .form-item-field-invoice-date-comptable-und-0-value-date input, .form-item-field-invoice-echeance-date-und-0-value-date input").hover(
        function () {
          date_echeance();
        }
      );

      function date_echeance() {
        var explode = $(".form-item-field-condition-paiement-und select").val().split(" ");
        var date_fournisseur = $(".form-item-field-invoice-date-supplier-und-0-value-date input").val();
        if (explode[0] === 45 && explode[1] === "FDM") {
          date_fournisseur = $(".form-item-field-invoice-date-comptable-und-0-value-date input").val();
        }
        var extract_date = date_fournisseur.split("/");
        var date_reception = $(".form-item-field-invoice-date-reception-und-0-value-date input").val();

        var d = new Date(extract_date[2], extract_date[1], 0);
        var endOfMonth = d.getDate();

        if (explode[0] === "CPT") {
          $(".form-item-field-invoice-echeance-date-und-0-value-date input").val(date_reception);
        } else if (explode[0] === "FDM") {
          if ($.isNumeric(explode[1])) {
            d = new Date(extract_date[2] + "-" + extract_date[1] + "-" + endOfMonth);
            d.setDate(d.getDate() + parseInt(explode[1]));
          }
          var date_echeance = ("0" + d.getDate()).slice(-2) + "/" + ("0" + (d.getMonth() + 1)).slice(-2) + "/" + d.getFullYear();
          $(".form-item-field-invoice-echeance-date-und-0-value-date input").val(date_echeance);
        } else if ($.isNumeric(explode[0])) {
          if (explode[1] === "Net") {
            d = new Date(extract_date[2] + "-" + extract_date[1] + "-" + extract_date[0]);
            d.setDate(d.getDate() + parseInt(explode[0]));
          }
          if (explode[1] === "FDM") {
            d = new Date(extract_date[2] + "-" + extract_date[1] + "-" + endOfMonth);
            if ($.isNumeric(extract_date[1])) {
              d.setDate(d.getDate() + parseInt(explode[0]));
            }
          }
          $(".form-item-field-invoice-echeance-date-und-0-value-date input").val(("0" + d.getDate()).slice(-2) + "/" + ("0" + (d.getMonth() + 1)).slice(-2) + "/" + d.getFullYear());
        }
      }

      //replace number , to .
      $(".field-type-number-decimal input.form-text").bind("keyup change", function () {
        $(this).val($(this).val().replace(",", ".").replace(" ", ""));
      });

    }
  };

}(jQuery, Drupal));

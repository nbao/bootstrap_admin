<?php
/**
 * @file
 * Defines the templated used to produce the the Gantt Chart.
 */
?>
<?php
$height = !empty($view->style_options['height']) ? $view->style_options['height'] : '300';
$zoom_default = !empty($view->style_options["zoom_default"]) ? $view->style_options["zoom_default"]: 5;
$style = "style='height:{$height}px; width: 100%;'";
// Added statement to add display of zoom controls if selected in config.
if ($view->style_options['zoom']) {
  $default_zoom = views_gantt_default_zoom_scales();
  $max = count($default_zoom) - 1;
  $zoom = t('Zoom').'<input type="range" class="custom-range"  id="gantt-zoom" name="gantt_zoom" min="0" max="'.$max.'" value="'.$zoom_default.'" step="1"/>';
}
else {
  $zoom = '';
}
?>
<div class="gantt-wrapper default">
  <ul class="gantt-controls">

    <li class="gantt-menu-item"><a data-action="collapseAll">
        <i class="fa fa-chevron-up"></i> <?php echo t('Collapse All')?></a>
    </li>
    <li class="gantt-menu-item gantt-menu-item-last"><a data-action="expandAll">
        <i class="fa fa-chevron-down"></i> <?php echo t('Expand All')?></a>
    </li>

    <li class="gantt-menu-item"><a data-action="toggleCriticalPath">
        <i class="fa fa-arrow-right"></i> <?php echo t('Critical Path')?></a>
    </li>
    <li class="gantt-menu-item gantt-menu-item-right"><a data-action="fullscreen">
        <i class="fa fa-desktop"></i> <?php echo t('Fullscreen')?></a>
    </li>
    <li class="gantt-menu-item gantt-menu-item-right gantt-menu-item-last">
      <a data-action="zoomToFit"><i class="fa fa-expand"></i> <?php echo t('Zoom to Fit')?></a>
    </li>
    <li class="gantt-menu-item gantt-menu-item-right"><?php echo $zoom ?></li>
  </ul>

	<div <?php print $style; ?> id="GanttDiv" class="gantt-div">
	</div>
</div>

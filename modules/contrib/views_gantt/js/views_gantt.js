(function ($) {
  var $window = $(window);

  /**
   * Our dialog object. Can be used to open a dialog to anywhere.
   */
  Drupal.GanttDialog = {
    dialog_open: false,
    open_dialog: null
  }

  /**
   * Open a dialog window.
   * @param string href the link to point to.
   */
  Drupal.GanttDialog.open = function (href, title, parent, project) {
    if (!this.dialog_open) {
      // Add render gantt dialog, so that we know that we should be in a
      // dialog.
      href += (href.indexOf('?') > -1 ? '&' : '?') + 'render=gantt-dialog&parent=' + parent + '&project=' + project;
      // Get the current window size and do 70% of the width and 85% of the height.
      var window_width = $window.width() / 100 * 70;
      var window_height = $window.height() / 100 * 85;
      this.open_dialog = $('<iframe class="gantt-dialog-iframe" src="' + href + '"></iframe>').dialog({
        width: window_width,
        height: window_height,
        modal: true,
        resizable: true,
        position: ['center', 50],
        title: title,
        close: function () { 
          Drupal.GanttDialog.dialog_open = false;
          $('.gantt-dialog-iframe').remove();
          refreshGantt();
        }
      }).width(window_width - 10).height(window_height);

      $window.bind('resize scroll', function () {
        // Move the dialog the main window moves.
        if (typeof Drupal.GanttDialog == 'object' && Drupal.GanttDialog.open_dialog != null) {
          Drupal.GanttDialog.open_dialog.dialog('option', 'position', ['center', 10]);
          Drupal.GanttDialog.setDimensions();
        }
      });
      this.dialog_open = true;
    }
  }

  /**
   * Set dimensions of the dialog dependning on the current window size
   * and scroll position.
   */
  Drupal.GanttDialog.setDimensions = function () {
    if (typeof Drupal.GanttDialog == 'object') {
      var window_width = $window.width() / 100 * 70;
      var window_height = $window.height() / 100 * 85;
      this.open_dialog.
        dialog('option', 'width', window_width).
        dialog('option', 'height', window_height).
        width(window_width - 10).height(window_height);
    }
  }

  /**
   * Close the dialog and provide an entity id and a title
   * that we can use in various ways.
   */
  Drupal.GanttDialog.close = function (entity_type, entity_id, title) {
    this.open_dialog.dialog('close');
    this.open_dialog.dialog('destroy');
    this.open_dialog = null;
    this.dialog_open = false;

    refreshGantt();
  }

  Drupal.behaviors.views_gantt = {
    attach: function (context) {
      var settings = Drupal.settings.views_gantt;
      gantt.config.xml_date = "%Y-%m-%d";
      gantt.config.details_on_dblclick = true;
      gantt.config.links = {"finish_to_start" : "0"};
      gantt.config.order_branch = true;
      gantt.config.order_branch_free = true;
      gantt.config.grid_resize = true;

      // Loop through settings array, configure gantt chart settings from config.
      for (var setting in settings.config) {
        gantt.config[setting] = settings.config[setting];
      }
      // Add Task name column.
      gantt.config.columns = [
        {name: "text", label: "Task name", tree: true, width: '*', min_width: 200},
      ];
      // Add aditional columns from gantt views style settings.
      $.each(Drupal.settings.views_gantt.columns, function(key, val) {
        column = {name:key, label: val, align: 'center', resize:true};
        if (key == 'progress') {
          column.template = function(obj) {
            return Math.round(obj.progress * 100) + "%";
          }
        }
        gantt.config.columns.push(column);
      });
      // Add column 'Add new Task'.
      if (Drupal.settings.views_gantt.add_task) {
        column = {name:"add", label:"", width:44};
        gantt.config.columns.push(column);
      }
      // Add type name to css classes line for tasks and project.
      gantt.templates.task_class = function(start, end, obj){
        return obj.type;
      }

      var value = $.cookie("ganttScale");
      if (value == null) {
        value =  $("#gantt-zoom").val();
        if(value == null){
          value = gantt.config.zoom_default;
        }
      }
      $("#gantt-zoom").val(value);
      var newSettings = gantt.config.zoom_options[value];
      $.each(newSettings, function (key, value) {
        gantt.config[key] = value;
      });

      gantt.init("GanttDiv");

      ganttModules.menu.setup();

      gantt.clearAll(); 
      gantt.load(ganttUrl(), "json");

      var dp = new gantt.dataProcessor(Drupal.settings.basePath + 'views_gantt/update');
      dp.init(gantt);
      $('body',context).once(function() {
        // Update chart after dragging.
        gantt.attachEvent("onAfterTaskUpdate", function(id, item) {
          if (item['parent']) {
            progressUpdate(id, gantt, dp);
          }
        });

        // Task click event, show custom dialog.
        gantt.attachEvent("onTaskDblClick", function(id,e){
          if (!Drupal.settings.views_gantt.edit_task) {
            return false;
          }

          if (id == null) {
            return false;
          }

          task = gantt.getTask(id);
          if (task["type"] === "project") {
            return false;       
          } 
          else {
            Drupal.GanttDialog.open(
              Drupal.settings.basePath + 'node/' + id + '/edit',
              task['text'],
              task.parent, Drupal.settings.views_gantt.project_id
            );

            return false;
          }
        });

        // Disable click event for header add column.
        gantt.attachEvent("onGridHeaderClick", function(name, e){
          if (name == 'add') {
            return false;
          }
        });

        gantt.attachEvent("onBeforeLinkAdd", function (id, item) {
          item.id = item.source + '_' + item.target;
        });
        gantt.attachEvent("onColumnResizeStart", function(ind, column) {
          if(!column.tree || ind == 0) return;

          setTimeout(function() {
            var marker = document.querySelector(".gantt_grid_resize_area");
            if(!marker) return;
            var cols = gantt.getGridColumns();
            var delta = cols[ind - 1].width || 0;
            if(!delta) return;

            marker.style.boxSizing = "content-box";
            marker.style.marginLeft = -delta + "px";
            marker.style.paddingRight = delta + "px";
          },1);
        });

        gantt.attachEvent("onCollapse", function() {
          var el = document.querySelector(".dhx-navigation");
          el.removeAttribute("style");

          var chatapp = document.getElementById("chat-application");
          chatapp.style.visibility = "visible";
        });

        gantt.attachEvent("onExpand", function() {
          var el = document.querySelector(".dhx-navigation");
          el.style.position = "static";

          var chatapp = document.getElementById("chat-application");
          chatapp.style.visibility = "hidden";
        });


        // This functionality added to introduce "zoom" functionality for Gantt
        // chart display.

        $('#gantt-zoom').on('change', function (e) {
          gantt.config.zoom_level = $(this).val();
          e = e || window.event;
          var el = e.target || e.srcElement;
          $.cookie("ganttScale", el.value);
          var newSettings = gantt.config.zoom_options[gantt.config.zoom_level];
          $.each(newSettings, function (key, value) {
            gantt.config[key] = value;
          });
          gantt.render();
        });

        //add marker to day
        var date_to_str = gantt.date.date_to_str(gantt.config.task_date);
        var today = new Date();
        gantt.addMarker({
          start_date:  new Date(),
          css: "today",
          text: "Today",
          title:"Today: " + date_to_str(today)
        });
        // Dragging Tasks Together with Their Dependent Tasks
        if(gantt.config.move_child){
          gantt.eachSuccessor = function(callback, root){
            if(!this.isTaskExists(root))
              return;

            // remember tasks we've already iterated in order to avoid infinite loops
            var traversedTasks = arguments[2] || {};
            if(traversedTasks[root])
              return;
            traversedTasks[root] = true;

            var rootTask = this.getTask(root);
            var links = rootTask.$source;
            if(links){
              for(var i=0; i < links.length; i++){
                var link = this.getLink(links[i]);
                if(this.isTaskExists(link.target)){
                  callback.call(this, this.getTask(link.target));

                  // iterate the whole branch, not only first-level dependencies
                  this.eachSuccessor(callback, link.target, traversedTasks);
                }
              }
            }
          };

          gantt.attachEvent("onTaskDrag", function(id, mode, task, original){
            var modes = gantt.config.drag_mode;
            if(mode == modes.move){
              var diff = task.start_date - original.start_date;
              gantt.eachSuccessor(function(child){
                child.start_date = new Date(+child.start_date + diff);
                child.end_date = new Date(+child.end_date + diff);
                gantt.refreshTask(child.id, true);
              },id );
            }
            return true;
          });
          gantt.attachEvent("onAfterTaskDrag", function(id, mode, e){
            var modes = gantt.config.drag_mode;
            if(mode == modes.move ){
              gantt.eachSuccessor(function(child){
                child.start_date = gantt.roundDate(child.start_date);
                child.end_date = gantt.calculateEndDate(child.start_date, child.duration);
                gantt.updateTask(child.id);
              },id );
            }
          });
        }
        //Make resize marker for two columns

        gantt.attachEvent("onColumnResizeStart", function (ind, column) {
          if (!column.tree || ind == 0) return;

          setTimeout(function () {
            var marker = document.querySelector(".gantt_grid_resize_area");
            if (!marker) return;
            var cols = gantt.getGridColumns();
            var delta = cols[ind - 1].width || 0;
            if (!delta) return;

            marker.style.boxSizing = "content-box";
            marker.style.marginLeft = -delta + "px";
            marker.style.paddingRight = delta + "px";
          }, 1);
        });
        // This function auto-scrolls the chart to today's date minus the offset.
        gantt.attachEvent("onGanttRender", function () {
          var dateX = gantt.posFromDate(new Date());
          var scrollX = Math.max(Math.round(dateX - gantt.config.task_scroll_offset), 0);
          gantt.scrollTo(scrollX, 0);
        });


        // Disable lightbox for add task, add custom dialog.
        gantt.attachEvent("onBeforeLightbox", function(id){
          var task = gantt.getTask(id);
          if (task == null) return false;

          if(task.$new){
            if (!Drupal.settings.views_gantt.add_task) {
              gantt.deleteTask(task.id);
              return false;
            }
            Drupal.GanttDialog.open(
              Drupal.settings.basePath + 'node/add/' + Drupal.settings.views_gantt.task_type, 
              task['text'], 
              task.parent, Drupal.settings.views_gantt.project_id
            );
          
            gantt.deleteTask(task.id);
              
            return false;
          }
          return true;
        });
      });
    }
  }

  function refreshGantt() {
    if (Drupal.settings && Drupal.settings.views_gantt && Drupal.settings.views_gantt.view) {
      var settings = Drupal.settings.views_gantt.view.settings;

      // Search in settings view which need update.
      if (settings.view_name && settings.view_display_id) {
        var selector = '.view-dom-id-' + settings.view_dom_id,
            href = Drupal.settings.views_gantt.view.href,
            viewData = {};

        // Construct an object using the settings defaults and then overriding
        // with data specific to the link.
        $.extend(
          viewData,
          settings,
          Drupal.Views.parseQueryString(href),
          // Extract argument data from the URL.
          Drupal.Views.parseViewArgs(href, settings.view_base_path)
        );

        // For anchor tags, these will go to the target of the anchor rather
        // than the usual location.
        $.extend(viewData, Drupal.Views.parseViewArgs(href, settings.view_base_path));

        // Ajax settings for uping view.
        var ajax_settings = {
          url: Drupal.settings.views_gantt.ajax_path,
          submit: viewData,
          selector: selector
        };

        var ajax = new Drupal.ajax(false, false, ajax_settings);
        ajax.eventResponse(ajax, {});
      }
    }
  }

  function progressUpdate(id, gantt, dp) {
    task = gantt.getTask(id);

    children_hours_completed = 0;
    children = gantt.getChildren(id);
    if (children.length) {
      $.each(children, function(key, child_id) {
        child = gantt.getTask(child_id);
        progress = child.progress;
        duration = child.duration;
        hours_completed = duration * progress;
        children_hours_completed += hours_completed;
      });
      task.progress = children_hours_completed / task.duration;
      if (task.progress > 1) task.progress = 1;
      gantt.refreshTask(id);
      dp.setUpdated(id, true);
    }

    if (task['parent']) {
      progressUpdate(task['parent'], gantt, dp);
    }
  }

  function ganttUrl() {
    url = Drupal.settings.basePath + 'views_gantt/data.json?' + 
      'view=' + Drupal.settings.views_gantt.view.settings.view_name +
      '&display=' + Drupal.settings.views_gantt.view.settings.view_display_id +
      '&project=' + Drupal.settings.views_gantt.project_id; 

    if (Drupal.settings.views_gantt.exposed_input) {
      $.each(Drupal.settings.views_gantt.exposed_input, function(key, val) {
        if (val) {
          url += '&' + key + '=' + val;
        }
      });
    }

    return url;
  }
})(jQuery);

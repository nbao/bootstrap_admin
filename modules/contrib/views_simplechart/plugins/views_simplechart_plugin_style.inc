<?php

/**
 * @file
 * Contains the list style plugin.
 */

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 */
class ViewsSimplechartPluginStyle extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['chart_title'] = array('default' => 'Simple Chart');
    $options['chart_axis_mapping'] = array('default' => '');
    $options['chart_type_stacked'] = array('default' => 'no');
    $options['chart_type'] = array('default' => 'bar');
    $options['chart_legend_position'] = array('default' => 'bottom');
    $options['chart_width'] = array('default' => '400');
    $options['chart_height'] = array('default' => '300');

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['chart_title'] = array(
      '#title' => t('Chart Title'),
      '#type' => 'textfield',
      '#size' => '60',
      '#default_value' => $this->options['chart_title'],
    );
    $form['chart_axis_mapping'] = array(
      '#title' => t('Chart Axis Mapping'),
      '#type' => 'textfield',
      '#description' => t('Each axis need to be placed as comma(,) separtor.'),
      '#size' => '60',
      '#default_value' => $this->options['chart_axis_mapping'],
    );
    $form['chart_type'] = array(
      '#type' => 'radios',
      '#title' => t('Chart type'),
      '#options' => array(
        'BarChart' => t('Bar Chart'),
        'LineChart' => t('Line Chart'),
        'PieChart' => t('Pie Chart'),
        'ColumnChart' => t('Column Chart'),
        'Timeline' => t('Timeline'),
        'OrgChart' => t('Organization Chart'),
        'ScatterChart' => t('Scatter Chart'),
        'AreaChart' => t('Area Chart'),
        'SteppedAreaChart' => t('Stepped Area Chart'),
        'BubbleChart' => t('Bubble Chart'),
        'CandlestickChart' => t('Candlestick Chart'),
        'ComboChart' => t('Combo Chart'),
        'TreeMap' => t('Tree Map'),
        'Table' => t('Table'),
      ),
      '#default_value' => $this->options['chart_type'],
    );
    $form['chart_type_stacked'] = array(
      '#type' => 'radios',
      '#title' => t('Do you want Stack in Graph?'),
      '#options' => array('yes' => t('Yes'),'no' => t('No')),
      '#description' => t('This is applicable only for Bar and Column chart.'),
      '#default_value' => $this->options['chart_type_stacked'],
      '#states' => [
        'visible' => [
          [':input[name="style_options[chart_type]"]' => ['value' => 'PieChart']],
          [':input[name="style_options[chart_type]"]' => ['value' => 'BarChart']],
          [':input[name="style_options[chart_type]"]' => ['value' => 'ColumnChart']],
        ],
      ],
    );
    $form['chart_legend_position'] = array(
      '#type' => 'radios',
      '#title' => t('Chart Legend Position'),
      '#options' => array('left' => t('Left'),'right' => t('Right'),'top' => t('Top'),'bottom' => t('Bottom')),
      '#default_value' => $this->options['chart_legend_position'],
    );
    $form['chart_width'] = array(
      '#title' => t('Chart Width'),
      '#type' => 'textfield',
      '#size' => '60',
      '#default_value' => $this->options['chart_width'],
    );
    $form['chart_height'] = array(
      '#title' => t('Chart Height'),
      '#type' => 'textfield',
      '#size' => '60',
      '#default_value' => $this->options['chart_height'],
    );
    $form['chart_3d'] = [
      '#title' => t('Chart 3D'),
      '#type' => 'checkbox',
      '#default_value' => empty($this->options['chart_3d']) ? 0 : 1,
      '#states' => [
        'visible' => [
          [':input[name="style_options[chart_type]"]' => ['value' => 'PieChart']],
          [':input[name="style_options[chart_type]"]' => ['value' => 'BarChart']],
          [':input[name="style_options[chart_type]"]' => ['value' => 'ColumnChart']],
        ],
      ],
    ];
    $form['chart_pieHole'] = [
      '#title' => t('Chart Pie hole'),
      '#type' => 'textfield',
      '#attributes' => [
        'type' => 'number',
        'min' => 0.1,
        'max' => 1,
      ],
      '#description' => t('Making a Donut Chart exemple : 0.5'),
      '#default_value' => $this->options['chart_pieHole'],
      '#states' => [
        'visible' => [
          ':input[name="style_options[chart_type]"]' => ['value' => 'PieChart'],
        ],
      ],
    ];
  }

  function render() {
    $barchart = $this->render_fields($this->view->result);
    if(!empty($this->options['chart_axis_mapping'])){
      $chartdata[] = explode(',', $this->options['chart_axis_mapping']);
    }else{
      foreach ($this->display->display_options["fields"] as $fields){
        if(empty($fields["exclude"])) {
          $chartdata[0][] = !empty($fields['label']) ? $fields['label'] : '';
        }
      }
    }
    foreach($barchart as $row) {
      $chartdata[] = array_values($row);
    }
    $this->options['id'] = implode('_',[$this->view->name,$this->view->current_display]);
    // Allow other modules to alter chart data and option
    drupal_alter('simplechart_data', $chartdata, $this->view, $this->options);
    $chartdata = json_encode($chartdata, JSON_NUMERIC_CHECK);
    $chartdata = html_entity_decode($chartdata,ENT_QUOTES);
    if ($this->options['chart_type'] == 'Timeline') {
      $chartdata = preg_replace('/"(\d+)-(\d+)-(\d+)"/i', 'new Date(\'$1-$2-$3\')', $chartdata);
    }
    return theme('views_simplechart_graph', array('barchart' => $chartdata, 'metadata' => $this->options));
  }
}

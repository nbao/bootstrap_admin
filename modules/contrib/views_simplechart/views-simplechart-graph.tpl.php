<?php
  $options = [
    'legend' => $metadata['chart_legend_position'],
    'title' => $metadata['chart_title'],
    'allowHtml' => true
  ];
  switch ($metadata['chart_type'] ){
    case 'Table':
      $packages = ['table'];
      break;
    case 'Timeline':
      $packages = ['timeline'];
      break;
    case 'OrgChart':
      $packages = ['orgchart'];
      break;
    default :
      $packages = ['corechart'];
  }
  if(!empty( $metadata['chart_width'])){
    $options['width'] = $metadata['chart_width'];
  }
  if(!empty( $metadata['chart_height'])){
    $options['height'] = $metadata['chart_height'];
  }
  if(!empty( $metadata['colors'])){
    $options['colors'] = $metadata['colors'];
  }
  if(in_array($metadata['chart_type'], ['BarChart','ColumnChart'])){
    if($metadata['chart_type_stacked'] == 'yes') {
      $options['isStacked'] = TRUE;
    }
    if(!empty($metadata["chart_3d"])){
      $options['is3D'] = true;
      $packages[] = 'columnchart';
    }
  }
  if(in_array($metadata['chart_type'] ,['PieChart'])){
    if(!empty($metadata["chart_3d"])){
      $options['is3D'] = TRUE;
      $packages[] = 'piechart';
    }
    if(!empty($metadata["chart_pieHole"]) && is_numeric($metadata["chart_pieHole"])){
      $options['pieHole'] = $metadata["chart_pieHole"];
    }
  }
  if(!empty($metadata['id'])){
    $id = $metadata['id'];
  }
?>
<div id="views-simplechart-<?php print $id ?>" class="<?php print $classes ?>">
  <script type="text/javascript">
    google.charts.load('current', {'packages': ['<?php echo implode("','", $packages)?>']});
    google.charts.setOnLoadCallback(function(){
      var data = new google.visualization.arrayToDataTable(<?php print $barchart;?>);
      var options =  <?php echo json_encode($options) ?>;
      var container = document.getElementById('views-simplechart-graph-<?php print $id ?>');
      var chart = new google.visualization.<?php print $metadata['chart_type'];?>(container);
      chart.draw(data, options);
    });
  </script>
  <div id="views-simplechart-graph-<?php print $id ?>"></div>
</div>

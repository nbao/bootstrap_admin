<?php
/**
 * @file
 * Customize the display of a complete Quick add Field form.
 */
if(!empty($form['#value'])):
    $values = isset($form_state['multistep_values']['create_field_data']) ? $form_state['multistep_values']['create_field_data'] : array();
    $bundle_name = $form['#value']['bundle'];
    $entity_type = $form['#value']['type'];
    $bundle = field_extract_bundle($entity_type, $bundle_name);
    // Gather bundle information.
    $instances = field_info_instances($entity_type, $bundle);
    $extra_fields = field_info_extra_fields($entity_type, $bundle, 'form');

    module_load_include('inc', 'field_ui', 'field_ui.admin');
    module_load_include('inc', 'pathauto', 'pathauto');
    $max_weight = field_info_max_weight($entity_type, $bundle, 'form');
    switch ($entity_type){
      case 'data':
        $field_type_options = data_get_field_types();
        $widget_type_options = data_get_field_sizes();
        break;
      case 'data_custom':
        $field_type_options = _field_type_options();
        $widget_type_options = _widget_type_options();
        break;
      default:
        $field_type_options = field_ui_field_type_options();
        $widget_type_options = field_ui_widget_type_options(NULL, TRUE);
        break;
    }
    drupal_add_css(drupal_get_path('module', 'field_ui') . '/field_ui.css');
    drupal_add_js(drupal_get_path('module', 'quick_data') . '/quick_data.js');
    drupal_add_js(array('fieldWidgetTypes' => field_ui_widget_type_options()), 'setting');
    drupal_add_js(array('quick_data' => array('entity_type' => $entity_type)), array('type' => 'setting'));


  $field_labels = explode(PHP_EOL,$form['#value']['field_label']);
    if(empty($field_labels) || count($field_labels)==1){
      $field_labels = explode("\t",$form['#value']['field_label']);
    }
    if(!empty($field_labels)){

      $field_labels = array_unique($field_labels);
        $machine_name_count = $test_exist_machine_name = [];
        $max_lengh = 26;
        foreach ($field_labels as $index=>$field_label):
            $temp = explode('|',$field_label);
            if(!empty($temp[1])){
              $field_label = trim($temp[1]);
            }
            $field_type_default = 'text';
            $field_widget_default = 'text_textfield';
            if($entity_type == 'data_custom') {
              $field_type_default = 'varchar';
              $field_widget_default = 'varchar';
            }
            if(!empty($temp[2])) {
              $type_default = trim($temp[2]);
              if(!empty($field_type_options[$type_default]) && !empty($widget_type_options[$field_type_options[$type_default]])) {
                $field_type_default = $type_default;
                if(!empty($temp[3])){
                  $field_widget_default = trim($temp[3]);
                }else{
                  $field_widget_default = key($widget_type_options[$field_type_options[$type_default]]);
                }
              }
            }
            $machine_name = _convert_machine_name($temp[0],$max_lengh);
            if(!empty($machine_name)){
                if(!in_array($machine_name,$test_exist_machine_name)){
                    $test_exist_machine_name[$field_label] = $machine_name;
                }else{
                    //add _1 if there is double field name, count 1->9
                    $machine_name = substr($machine_name,0,$max_lengh-2);
                    if(!empty($machine_name_count[$machine_name])){
                        $machine_name_count[$machine_name] = 1;
                    }else{
                        $machine_name_count[$machine_name] += 1;
                    }
                    $machine_name = $machine_name .'_'. $machine_name_count[$machine_name];
                }
                if(empty($machine_name)) continue;

                if ($field_type_options && $widget_type_options) {
                    $form['create_field_data'][$machine_name]['label'] = array(
                        '#type' => 'hidden',
                        '#title' => $field_label,
                        '#description' => t('Enter Label of the field'),
                        '#value'=> trim($field_label),
                        '#name' => 'fields['.$machine_name.'][label]'
                    );

                    $form['create_field_data'][$machine_name]['field_name'] = array(
                        '#type' => 'hidden',
                        '#title' => trim($field_label),
                        // This field should stay LTR even for RTL languages.
                        '#field_prefix' => '<span dir="ltr">field_',
                        '#field_suffix' => '</span>&lrm;',
                        '#size' => 15,
                        // 32 characters minus the 'field_' prefix.
                        '#maxlength' => 26,
                        '#description' => t('A unique machine-readable name containing letters, numbers, and underscores.'),
                        '#value' => substr($machine_name,0,26),
                        '#attributes' => ['class' => ['form-text','atf-processed','machine-name-target'],'maxlength'=>26],
                        '#name' => 'fields['.$machine_name.'][field_name]',
                        '#entity_type' => $entity_type,

                    );

                    $form['create_field_data'][$machine_name]['type'] = array(
                        '#type' => 'select',
                        '#title' => t('Type of new field'),
                        '#options' => $field_type_options,
                        '#title_display' => 'invisible',
                        '#empty_option' => t('- Select a field type -'),
                        '#description' => t('Type of data to store.'),
                        '#required' => TRUE,
                        '#value' => $field_type_default,
                        '#attributes' => ['class' => ['field-type-select']],
                        '#name' => 'fields['.$machine_name.'][type]',
                        '#entity_type' => $entity_type,
                    );

                    $form['create_field_data'][$machine_name]['widget_type'] = array(
                        '#type' => 'select',
                        '#title' => t('Widget for new field'),
                        '#title_display' => 'invisible',
                        '#options' => $widget_type_options,
                        '#empty_option' => t('- Select a widget -'),
                        '#description' => t('Form element to edit the data.'),
                        '#cell_attributes' => array('colspan' => 3),
                        '#ajax' => array(
                            'callback' => 'quick_field_update_field_form',
                            'wrapper' => 'dropdown_second_replace',
                            'method' => 'replace',
                            'effect' => 'fade',
                        ),
                        '#required' => TRUE,
                        '#name' => 'fields['.$machine_name.'][widget_type]',
                        '#value' => $field_widget_default,
                        '#attributes' => ['class' => ['widget-type-select']],
                    );
                }
            }

        endforeach;
    }

?>


<table class="table table-striped">
    <thead>
        <tr>
            <th><?php echo t('Label')?></th>
            <th><?php echo t('Machine name')?></th>
            <th><?php echo t('Field type')?></th>
            <th><?php echo t('Widget')?></th>
        </tr>
    </thead>
    <tbody>
    <?php
    $i = 1;
    $check_error = [];
    foreach ($form['create_field_data'] as $field_name=>$field_info):
        $error = '';
        if(in_array($field_info['field_name']['#value'],$check_error)){
            $error = 'error';
        }
        else $check_error[] = $field_info['field_name']['#value'];
    ?>
  <tr id = "<?php echo $field_name?>" class = "add-new <?php echo $i%2 ?'odd':'even'; echo $error?>">
    <td>
        <?php
        print drupal_render($field_info['label']);
        print $field_info['label']['#value'];
        ?>
    </td>
    <td>
        <small class = "field-machine-name">
            <span class = "machine-name-field">field_</span>
            <?php
            print drupal_render($field_info['field_name']);
            ?>
            <span class = "machine-name-value"><?php print $field_info['field_name']['#value']; ?></span>
            <span class = "admin-link machine-name-value"><a href = "#"><?php echo t('Edit')?></a></span>
        </small>
    </td>
    <td>
        <?php print drupal_render($field_info['type']); ?>
    </td>
    <td colspan = "3">
     <?php print drupal_render($field_info['widget_type']); ?>
    </td>
  </tr>
        <?php
    $i++;
    endforeach;
    unset($form['create_field_data']);
    ?>
   </tbody>
</table>

<?php
endif;
  // Print out the main part of the form.
  print drupal_render_children($form);

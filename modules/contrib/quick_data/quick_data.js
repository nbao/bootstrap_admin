/**
 * @file
 * Attaches the behaviors for the SN QUICK Add field data module.
 */

(function($) {
    Drupal.behaviors.quick_data = {
        attach: function (context, settings) {
            if(settings.quick_data.entity_type != "data"){
              $(".widget-type-select option:selected").parent().siblings().hide();
              $(".field-type-select").change(function() {
                var type_label = $(this).find(":selected").text();
                $(this).closest("tr").find(".widget-type-select").find("optgroup, option").hide().filter("[label='" + type_label+ "'], [label='" + type_label + "'] > *").show().find("option:first").attr("selected","selected");
              });
            }
            $(".field-machine-name a").click(function(event) {
              event.preventDefault();
              $(this).closest("td").find("input").prop("type", "text");
              $(this).parents("small").find(".machine-name-value").remove();
            });
        }
    };
})(jQuery);

<?php

/**
 * Define a WorkCalendar object with its operations.
 */
class WorkCalendar extends Entity {

  /**
   * @defgroup wc_week_days_constants Constants to represent week days
   * @{
   * Constants that represent bit numbers to compound the week byte.
   *
   * The week byte stores which week days are working days in the calendar.
   */
  const SUNDAY    = 1;   // 00000001
  const MONDAY    = 2;   // 00000010
  const TUESDAY   = 4;   // 00000100
  const WEDNESDAY = 8;   // 00001000
  const THURSDAY  = 16;  // 00010000
  const FRIDAY    = 32;  // 00100000
  const SATURDAY  = 64;  // 01000000
  /**
   * @} End of "defgroup wc_week_days_constants".
   */

  public $name;
  public $label;
  public $description;
  public $week = 0;
  public $week_days;
  public $holidays;

  /**
   * Initialize object attributes and decode `week`.
   */
  public function __construct(array $values = array(), $entityType = 'work_calendar') {
    parent::__construct($values, $entityType);
    $this->week_days = array();
    foreach ($this->dayCodes() as $name => $code) {
      if ($this->week & $code) {
        $this->week_days[$name] = $name;
      }
    }
  }

  /**
   * Returns whether the entity is locked, thus may not be deleted or renamed.
   *
   * Entities provided in code are automatically treated as locked, as well
   * as any fixed profile type.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }

  /**
   * Encodes given week days into week byte.
   */
  public function updateWeek($week_days) {
    $codes = $this->dayCodes();
    $this->week = 0;
    foreach ($week_days as $name) {
      if (!empty($name)) {
        $this->week = $this->week | $codes[$name];
      }
    }
    $this->week_days = $week_days;
  }

  /**
   * Map week day names to our internal bits.
   */
  private static function dayCodes() {
    static $codes;
    if (empty($codes)) {
      foreach (date_week_days_untranslated() as $name) {
        $codes[$name] = constant('self::' . strtoupper($name));
      }
    }
    return $codes;
  }

  /**
   * Returns the untranslated week day name for the requested date.
   *
   * @param $date DateObject or date string.
   */
  private static function weekDayName($date) {
    $week_day = date_day_of_week($date);
    $names = date_week_days_untranslated();
    return $names[$week_day];
  }

  /**
   * Returns start and end date for a year or month period.
   */
  private static function periodDates($year, $month = NULL) {
    if (is_null($month)) {
      $start = "$year-01-01 00:00:00";
      $end = "$year-12-31 00:00:00";
    }
    else {
      $start = "$year-$month-01 00:00:00";
      $days = date_days_in_month($year, $month);
      $end = "$year-$month-$days 00:00:00";
    }
    return array($start, $end);
  }

  /**
   * Returns dates in a period corresponding to requested week days.
   *
   * Internally it builds a rrule to do the job.
   */
  private static function weekDaysDates($start, $end, $week_days, $holidays = []) {
    // Transform $week_days to a rrule "byday" string. Example: SU,MO,TU,WE.
    // date_repeat api wants the byday array to start by the week day of $start.
    $byday = array();
    // Convert $week_days to a key based array and find the index of the week
    // day of start in order to assign bydays starting from this index.
    $week_days = array_values($week_days);
    $index = array_search(self::weekDayName($start), $week_days);
    $count = count($week_days);
    for ($offset = 0; $offset < $count; $offset++) {
      $wday = $week_days[($index + $offset) % $count];
      $byday[] = strtoupper(substr($wday, 0, 2));
    }

    // Request date_repeat to run the rrule.
    $rrule = 'RRULE:FREQ=WEEKLY;INTERVAL=1;BYDAY=' . implode(',', $byday) . ';';
    // Don't require date_repeat module to be enabled. Just use its api.
    module_load_include('module', 'date_repeat', 'date_repeat');
    module_load_include('inc', 'date_repeat', 'date_repeat_calc');
    $dates = date_repeat_calc($rrule, (string)$start, (string)$end);

    // Strip hours from dates.
    $reset = false;
    foreach ($dates as $key => &$date) {
      $date = substr($date, 0, 10);
      if(!empty($holidays) && is_array($holidays) && in_array($date,$holidays)){
      	unset($dates[$key]);
      	$reset = true;
      }
    }
    if($reset){
	    $dates = array_values($dates);
    }

    // Special case. First day is always included in despite of the rrule.
    // We need to exclude it by hand if neccesary.
    $week_day = self::weekDayName($dates[0]);
    if (!in_array($week_day, $week_days)) {
      array_shift($dates);
    }

    return $dates;
  }

  /**
   * Returns a list of opening days for the requested period.
   */
  public function getOpenDaysInPeriod($start, $end) {
    $week_days = $this->week_days;
    $holidays = self::holidays($this->holidays);
    $dates = self::weekDaysDates($start, $end, $week_days, $holidays);
    return $dates;
  }

  /**
   * Returns a list of opening days for the requested year or month.
   */
  public function getOpenDays($year, $month = NULL) {
    list($start, $end) = self::periodDates($year, $month);
    return $this->getOpenDaysInPeriod($start, $end);
  }

  /**
   * Returns a list of closed days for the requested period.
   */
  public function getClosedDaysInPeriod($start, $end) {
    $week_days = array_diff(date_week_days_untranslated(), $this->week_days);
    $holidays = self::holidays($this->holidays);
    $dates = self::weekDaysDates($start, $end, $week_days, $holidays);
    return $dates;
  }

  /**
   * Returns a list of closed days for the requested year or month.
   */
  public function getClosedDays($year, $month = NULL) {
    list($start, $end) = self::periodDates($year, $month);
    return $this->getClosedDaysInPeriod($start, $end);
  }

  /**
   * Returns boolean indicating wether the requested day the business is open.
   */
  public function isOpenDay($year, $month, $day) {
    if(self::isHoliday($year, $month, $day)){
      return false;
    };
    $date = new DateObject("$year-$month-$day 00:00:00");
    $week_day = self::weekDayName($date);
    return in_array($week_day, $this->week_days);
  }

  /**
   * Returns boolean indicating whether the requested day the business is closed.
   */
  public function isClosedDay($year, $month, $day) {
    if(self::isHoliday($year, $month, $day)){
      return true;
    };
    $date = new DateObject("$year-$month-$day 00:00:00");
    $week_day = self::weekDayName($date);
    return !in_array($week_day, $this->week_days);
  }

  /*
   * Convert holidays text to array DateTime current year
   * Returns holidays array
   */
  public function holidays($holidays_text = '', $year = FALSE, $easter_date = TRUE) {
	$holidays = null;
  	if(!empty($holidays_text)){
  		$holidays = $holidays_text;
    }
    $convert_holiday_text = !empty($holidays)?explode(PHP_EOL, $holidays):null;
  	if(!empty($convert_holiday_text)){
	    $holidays = [];
  		foreach ($convert_holiday_text as $holiday_date){
		    $holiday_date = trim($holiday_date);
		    if(empty($holiday_date)){
          continue;
        }
		    $test_date_from = explode(' ', $holiday_date);
		    if(!empty($test_date_from[1])){
			    $holiday_date_from[] = $test_date_from;
			    continue;
		    }
  			$test_date_us = explode('/', $holiday_date);
  			if(!empty($test_date_us[1])){
			    $holiday_date = implode('-', array_reverse($test_date_us));
		    }
  			if(!$year) $year = date('Y');
		    $holiday_date .= '-'.$year;
  			$datetime = strtotime($holiday_date);
  			if(!empty($datetime)){
			    $holidays[] = date('Y-m-d',$datetime);
		    }
	    }
	    if(!empty($holiday_date_from)){
  			foreach ($holiday_date_from as $date_from){
			    foreach ($date_from as &$date_to){
				    $test_date_us = explode('/', $date_to);
				    if(!empty($test_date_us[1])){
					    $date_to = implode('-', array_reverse($test_date_us));
				    }
				    $date_to .= '-'.date('Y');
			    }
			    $end_day = new DateTime($date_from[1]);
			    $period = new DatePeriod(
				    new DateTime($date_from[0]),
				    new DateInterval('P1D'),
				    $end_day->modify('+1 day')
			    );
			    foreach( $period as $date){
				    $holidays[] = $date->format('Y-m-d');
			    }
		    }
	    }
    }
    if($easter_date){
      $holidays[] = $easterDate = date('Y-m-d',easter_date($year));
      //Holiday that depend on Easter
      $holidays[] = date('Y-m-d',strtotime("$easterDate +1 days"));//Easter Monday
      //$holidays[] = date('Y-m-d',strtotime("$easterDate +50 days"));//Pentecost
      $holidays[] = date('Y-m-d',strtotime("$easterDate +39 days"));//ascension
    }
    return $holidays;
  }
  /**
   * Returns boolean indicating whether the requested day the holiday.
   */
  public function isHoliday($year, $month, $day, $easter = TRUE) {
    $date = new DateObject("$year-$month-$day 00:00:00");
    $holidays = self::holidays($this->holidays, $year, $easter);
    if(empty($holidays)) return false;
    return in_array($date->format("Y-m-d"),$holidays);
  }
}

<?php

/**
 * Implements hook_schema().
 */
function work_calendar_schema() {
  $schema = array();
  $schema['work_calendar'] = array(
    'description' => 'Stores work calendars.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary Key: Unique work calendar ID.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The machine name of this work calendar.',
      ),
      'label' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The label of this work calendar.',
      ),
      'description' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'medium',
        'description' => 'A brief description of this work calendar.',
      ),
      'week' => array(
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'A bitwise representation of opening week days.',
      ),
      'holidays' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'medium',
        'description' => 'List of holidays.',
      ),
    ) + entity_exportable_schema_fields(),

    'primary key' => array('id'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function work_calendar_uninstall() {
  variable_del('work_calendar_default');
}

/**
 * Implements hook_update().
 * Add new columns holidays to work_calendar.
 */
function work_calendar_update_7001() {
  db_add_field('work_calendar', 'holidays', ['type' => 'text']);
}

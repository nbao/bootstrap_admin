<?php
/**
 * @file
 * Template to display a datatable.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 */
?>
<?php
$print_multi_filter = function ($options, $id, $field, $label) {
  if (!empty($options['info'][$field]) && $options['info'][$field]['multi_filter'] != 'hidden') {
    $field_info = field_info_field($field);
    $element['#attributes'] = [
      'id' => $id,
      'placeholder' => html_entity_decode(strip_tags($label), ENT_QUOTES),
      'name' => $field,
      'class' => ["form-control", "search"],
      'autocomplete' => 'off',
    ];
    if (isset($field_info['settings']['allowed_values'])) {
      //cas taxonomy list
      if (!empty($field_info['settings']['allowed_values'][0]['vocabulary'])) {
        $voc = taxonomy_vocabulary_machine_name_load($field_info['settings']['allowed_values'][0]['vocabulary']);
        $tree = taxonomy_get_tree($voc->vid);
        $list_allowed = [];
        foreach ($tree as $term) {
          $list_allowed[$term->name] = $term->name;
        }
        $field_info['settings']['allowed_values_function'] = '';
      }
      else {
        $list_allowed = list_allowed_values($field_info);
        //in case of boolean not define label value
        if ($field_info['type'] == 'list_boolean') {
          foreach ($list_allowed as $key => &$value) {
            if (empty($value)) {
              unset($list_allowed[$key]);
            }
          }
        }
        if (!empty($list_allowed)) {
          $list_allowed = array_combine($list_allowed, $list_allowed);
        }
      }
      if (empty($list_allowed)) {
        return '';
      }
      $empty_option = ['' => t('- None -')];
      $element['#options'] = $empty_option + $list_allowed;
      $element['#type'] = 'select';
    }
    else {
      $element['#type'] = 'textfield';
      $element['#autocomplete_path'] = FALSE;
    }
    $filter = render($element);
    return $filter;
  }
};

$options = $view->style_plugin->options;
if (!empty($options["info"]["views_bulk_operations"])) {
  unset($options["info"]["views_bulk_operations"]);
}
?>
<table id="<?php print $id ?>"
       class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!empty($title)) : ?>
    <caption><?php print $title; ?></caption>
  <?php endif; ?>
  <thead>
  <?php
  if (!empty($header['views_bulk_operations']) && $options['elements']['multi_filter'] && $options['elements']['multi_filter_choice'] == 'header') {
    echo "<tr><th class='views_bulk_operations' colspan='" . count($header) . "'>{$header['views_bulk_operations']}</th></tr>";
  }
  ?>
  <tr>
    <?php foreach ($header as $field => $label):
      $field_name_info[$field] = field_info_field($field);
      ?>
      <th class="views-field views-field-<?php print $fields[$field]; ?>"
          aria-label="<?php echo html_entity_decode(strip_tags($label), ENT_QUOTES) ?>">
        <?php if ($field == 'views_bulk_operations' && $options['elements']['multi_filter_choice'] == 'header') {
          continue;
        }
        print "<label for='search-{$fields[$field]}' class='{$header_classes[$field]}'>$label</label>"; ?>
        <?php if ($options['elements']['multi_filter'] && $options['elements']['multi_filter_choice'] == 'header') {
          echo $print_multi_filter($options, $fields[$field], $field, $label);
        }
        ?>
      </th>
    <?php endforeach; ?>
  </tr>
  </thead>
  <tbody>
  <?php if (!empty($rows)) {
    foreach ($rows as $count => $row):?>
      <tr class="<?php print !empty( $row_classes[$count])?implode(' ', $row_classes[$count]) : ''; ?>">
        <?php foreach ($row as $field => $content):?>
          <td <?php if ($field_classes[$field][$count]): ?> class="<?php print $field_classes[$field][$count]; ?>"<?php endif; ?> <?php print drupal_attributes($field_attributes[$field][$count]); ?>>
            <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php
    endforeach;
  } ?>
  </tbody>
  <?php
  if ($options['elements']['multi_filter'] && $options['elements']['multi_filter_choice'] == 'footer') { ?>
    <tfoot>
    <tr class="multi_filter">
      <?php foreach ($header as $field => $label): ?>
        <th>
          <?php echo $print_multi_filter($options, $fields, $field, $label) ?>
        </th>
      <?php endforeach; ?>
    </tr>
    </tfoot>
  <?php } ?>
</table>

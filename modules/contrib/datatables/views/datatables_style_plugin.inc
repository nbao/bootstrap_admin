<?php
/**
 * @file
 * Contains the datatables style plugin.
 */

/**
 * Style plugin to render each item as a row in a datatables.
 *
 * @ingroup views_style_plugins
 */
class datatables_style_plugin extends views_plugin_style_table {
  /**
   * Implements parent::option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();

    unset($options['sticky']);
    unset($options['override']);

    $options['elements'] = array(
      'default' => array(
        'search_box' => TRUE,
        'table_info' => TRUE,
        'save_state' => FALSE,
      ),
    );
    $options['layout'] = array(
      'default' => array(
        'autowidth' => FALSE,
        'themeroller' => FALSE,
        'dom' => '',
      ),
    );
    $options['pages'] = array(
      'default' => array(
        'pagination_style' => 0,
        'lengthChange' => 0,
        'display_length' => 20,
      ),
    );
    $options['hidden_columns'] = array(
      'default' => array(),
    );
    return $options;
  }

  /**
   * Implements parent::options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['#theme'] = 'views_datatables_style_plugin_table';

    // Check if fields have been added.  Table style plugin will set
    // error_markup if fields are not added.
    // @see views_plugin_style_table::options_form()
    if (isset($form['error_markup'])) {
      return;
    }

    unset($form['sticky']);
    unset($form['override']);

    $form['description_markup']['#markup'] = '<div class="description form-item">' . t('DataTables works best if you set the pager to display all items, since DataTabels contains its own pager implementation.<br/><br/>Place fields into columns; you may combine multiple fields into the same column. If you do, the separator in the column specified will be used to separate the fields. Check the sortable box to make that column click sortable, and check the default sort radio to determine which column will be sorted by default, if any. You may control column order and field labels in the fields section.') . '</div>';

    $form['elements'] = array(
      '#type' => 'fieldset',
      '#title' => t('Widgets & Elements'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['elements']['search_box'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable the search filter box.'),
      '#default_value' => !empty($this->options['elements']['search_box']),
      '#description' => 'The search filter box allows users to dynamically filter the results in the table.  Disabling this option will hide the search filter box from users.',
    );
    $form['elements']['table_info'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable the table information display.'),
      '#default_value' => !empty($this->options['elements']['table_info']),
      '#description' => t('This shows information about the data that is currently visible on the page, including information about filtered data if that action is being performed.'),
    );
    $form['elements']['table_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Add the table class.'),
      '#default_value' => !empty($this->options['elements']['table_class'])?$this->options['elements']['table_class']:'',
      '#description' => t('Exemple for theme bootstrap: table table-striped table-bordered table-hover'),
    );

    $form['elements']['style'] = array(
      '#type' => 'select',
      '#title' => t('Theme Style'),
      '#default_value' => isset($this->options['elements']['style']) ? $this->options['elements']['style'] : 'jquery.dataTables',
      '#options' => array(
        'jquery.dataTables' => t('(Default)'),
        'bootstrap' => t('Bootstrap 3'),
        'bootstrap4' => t('Bootstrap 4'),
        'foundation' => t('Foundation'),
        'jqueryui' => t('jQuery UI'),
        'material' => t('Material'),
        'semanticui' => t('Semantic UI'),
        'uikit' => t('UI Kit'),

      ),
      '#description' => t('Selects which pagination style should be used.'),
    );

    $form['elements']['bootstrap']['table-striped'] = array(
      '#type' => 'checkbox',
      '#title' => t('Striped Rows'),
      '#default_value' => !empty($this->options['elements']['bootstrap']['table-striped']),
      '#states' => array(
        // Only show this field when the value of type is sell.
        'visible' => array(
          array(
            ':input[name="style_options[elements][style]"]' =>   array('value' => 'bootstrap'),
            ':input[name="style_options[elements][style]"]' =>   array('value' => 'bootstrap4'),
          ),
        ),
      ),
    );

    $form['elements']['bootstrap']['table-bordered'] = array(
      '#type' => 'checkbox',
      '#title' => t('Bordered Table'),
      '#default_value' => !empty($this->options['elements']['bootstrap']['table-bordered']),
      '#states' => array(
        // Only show this field when the value of type is sell.
        'visible' => array(
          array(
            ':input[name="style_options[elements][style]"]' =>   array('value' => 'bootstrap'),
            ':input[name="style_options[elements][style]"]' =>   array('value' => 'bootstrap4'),
          ),
        ),
      ),
    );

    $form['elements']['bootstrap']['table-hover'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hover Rows'),
      '#default_value' => !empty($this->options['elements']['bootstrap']['table-hover']),
      '#states' => array(
        // Only show this field when the value of type is sell.
        'visible' => array(
          array(
            ':input[name="style_options[elements][style]"]' =>   array('value' => 'bootstrap'),
            ':input[name="style_options[elements][style]"]' =>   array('value' => 'bootstrap4'),
          ),
        ),
      ),
    );

    $form['elements']['bootstrap']['table-condensed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Condensed Table'),
      '#default_value' => !empty($this->options['elements']['bootstrap']['table-condensed']),
      '#states' => array(
        // Only show this field when the value of type is sell.
        'visible' => array(
          array(
            ':input[name="style_options[elements][style]"]' =>   array('value' => 'bootstrap'),
            ':input[name="style_options[elements][style]"]' =>   array('value' => 'bootstrap4'),
          ),
        ),
      ),
    );

    $form['elements']['save_state'] = array(
      '#type' => 'checkbox',
      '#title' => t('Save State'),
      '#default_value' => !empty($this->options['elements']['save_state']),
      '#description' => t("DataTables can use cookies in the end user's web-browser in order to store it's state after each change in drawing. What this means is that if the user were to reload the page, the table should remain exactly as it was (length, filtering, pagination and sorting)"),
    );
	  $directories = glob(_datatables_get_path() . DIRECTORY_SEPARATOR . '*', GLOB_ONLYDIR);
      if (!empty($directories)) {
        $form['extras'] = array(
          '#type' => 'fieldset',
          '#title' => t('Extensions'),
	        '#description' => t('View exemple: ') . '<a href="https://datatables.net/extensions/index">' . t('Extensions') . '</a>',
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        );
        foreach ($directories as $directory) {
	        $temp = explode(DIRECTORY_SEPARATOR, $directory);
	        $temp_nameDir = explode('-', end($temp));
	        $nameDir = $temp_nameDir[0];
	        $version = !empty($temp_nameDir[1]) ? $temp_nameDir[1] : '';
	        //exception
	        if ($nameDir == 'DataTables') continue;
	        if ($nameDir == 'KeyTable') $nameDir = 'keys';

          $form['extras'][$nameDir] = array(
            '#type' => 'checkbox',
	          '#title' => t($nameDir) . ' ' . t('version') . ' ' . $version,
            '#default_value' => !empty($this->options['extras'][$nameDir]),
	          '#description' => t('View exemple: ') . '<a href="https://datatables.net/extensions/' . strtolower($nameDir) . '/examples/">' . $nameDir . '</a>',
          );
          if($nameDir=='FixedColumns'){

            $form['extras']['leftColumns'] = array(
              '#type' => 'textfield',
              '#title' => t('Left columns'),
              '#default_value' => !empty($this->options['extras']['leftColumns'])?$this->options['extras']['leftColumns']:'',
              '#description' => t('For Extentions: FixedColumns'),
              '#states' => array(
                'visible' => array(
                  ':input[name="style_options[extras][FixedColumns]"]' =>  array('checked' => TRUE),
                ),
              ),
            );
            $form['extras']['rightColumns'] = array(
              '#type' => 'textfield',
              '#title' => t('Right columns'),
              '#default_value' => !empty($this->options['extras']['rightColumns'])?$this->options['extras']['rightColumns']:'',
              '#description' => t('For Extentions: FixedColumns'),
              '#states' => array(
                'visible' => array(
                  ':input[name="style_options[extras][FixedColumns]"]' =>  array('checked' => TRUE),
                ),
              ),
            );
          }
          if($nameDir=='Buttons'){
            $form['extras']['customButtons'] = array(
              '#type' => 'textarea',
              '#title' => t('Custom buttons'),
              '#default_value' => !empty($this->options['extras']['customButtons'])?$this->options['extras']['customButtons']:'',
	            '#description' => t('For Extentions') . ': <a target="_blank" href="https://datatables.net/extensions/buttons/examples/initialisation/custom.html">Buttons</a>. exemple json validated: ["copy","csv","copyHtml5","excelHtml5","csvHtml5","pdfHtml5","print","colvis"]',
              '#states' => array(
                'visible' => array(
                  ':input[name="style_options[extras][Buttons]"]' =>  array('checked' => TRUE),
                ),
              ),
            );
          }
	        if ($nameDir == 'RowReorder') {
		        $form['extras']['customRowReorder'] = array(
			        '#type' => 'textarea',
			        '#title' => t('Custom RowReorder'),
			        '#default_value' => !empty($this->options['extras']['customRowReorder']) ? $this->options['extras']['customRowReorder'] : '',
			        '#description' => t('For Extentions:') . ' <a target="_blank" href="https://datatables.net/extensions/rowreorder/examples/initialisation/responsive.html">Mobile support</a>. exemple json validated: {selector: "td:nth-child(2)"}',
			        '#states' => array(
				        'visible' => array(
					        ':input[name="style_options[extras][RowReorder]"]' => array('checked' => TRUE),
				        ),
			        ),
		        );
	        }
	        if ($nameDir == 'RowGroup') {
		        $form['extras']['customRowGroup'] = array(
			        '#type' => 'textarea',
			        '#title' => t('Custom RowReorder'),
			        '#default_value' => !empty($this->options['extras']['customRowGroup']) ? $this->options['extras']['customRowGroup'] : '',
			        '#description' => t('For Extentions: <a target="_blank" href="https://datatables.net/extensions/rowgroup/examples/initialisation/endRender.html">End grouping only</a>. exemple json validated: {dataSrc: 2}'),
			        '#states' => array(
				        'visible' => array(
					        ':input[name="style_options[extras][RowGroup]"]' => array('checked' => TRUE),
				        ),
			        ),
		        );
	        }
	        if ($nameDir == 'Responsive') {
		        $form['extras']['customResponsive'] = array(
			        '#type' => 'textarea',
			        '#title' => t('Custom Responsive'),
			        '#default_value' => !empty($this->options['extras']['customResponsive']) ? $this->options['extras']['customResponsive'] : '',
			        '#description' => t('For Extentions: <a target="_blank" href="https://datatables.net/extensions/responsive/examples/child-rows/disable-child-rows.html">Disable child rows</a>. exemple json validated: {details: false}'),
			        '#states' => array(
				        'visible' => array(
					        ':input[name="style_options[extras][Responsive]"]' => array('checked' => TRUE),
				        ),
			        ),
		        );
	        }
          if($nameDir=='Scroller'){
            $form['extras']['scrollY'] = array(
              '#type' => 'textfield',
              '#title' => t('Custom Scroller Y'),
              '#default_value' => !empty($this->options['Scroller']['scrollY'])?$this->options['Scroller']['scrollY']:200,
              '#description' => t('For Extentions: <a target="_blank" href="https://www.datatables.net/extensions/scroller/examples/initialisation/simple.html">Scroller</a>.'),
              '#states' => array(
                'visible' => array(
                  ':input[name="style_options[extras][Scroller]"]' =>  array('checked' => TRUE),
                ),
              ),
            );
          }
          if($nameDir=='Select'){
	          $form['extras']['customSelect'] = array(
              '#type' => 'radios',
              '#title' =>  t('Select style') ,
		          '#default_value' => '',
              '#options' => array(
	              '' => t('Default'),
	              '{"style": "single"}' => t('Single item selection'),
	              '{"style": "multi"}' => t('Multi item selection'),
	              '{"style": "os"}' => t('Operating System style selection'),
              ),
              '#states' => array(
                'visible' => array(
                  ':input[name="style_options[extras][Select]"]' =>  array('checked' => TRUE),
                ),
              ),
            );
	          if(!empty($this->options['extras']['customSelect'])){
              $form['extras']['customSelect'] ['#title'] .= drupal_json_decode($this->options['extras']['customSelect'])['style'];
              $form['extras']['customSelect'] ['#default_value'] = $this->options['extras']['customSelect'];
            }
          }
          if($nameDir=='FixedHeader'){
            $form['extras']['FixedHeader_header'] = array(
              '#type' => 'checkbox',
              '#title' => t('Header fixed'),
              '#default_value' => !empty($this->options['extras']['FixedHeader_header']),
              '#description' => t('For FixedHeader: <a target="_blank" href="https://datatables.net/extensions/fixedheader/examples/options/header_footer.html">Header and footer fixed</a>. <br><b>Attention </b>Header fixed is not <a href="https://datatables.net/download/compatibility">compatible</a> with FixedColumns'),
              '#states' => array(
                'visible' => array(
                  ':input[name="style_options[extras][FixedHeader]"]' =>  array('checked' => TRUE),
                ),
              ),
            );
            $form['extras']['FixedHeader_footer'] = array(
              '#type' => 'checkbox',
              '#title' => t('Footer fixed'),
              '#default_value' => !empty($this->options['extras']['FixedHeader_footer']),
              '#description' => t('For FixedHeader: <a target="_blank" href="https://datatables.net/extensions/fixedheader/examples/options/header_footer.html">Header and footer fixed</a>. Compatible with FixedColumns'),
              '#states' => array(
                'visible' => array(
                  ':input[name="style_options[extras][FixedHeader]"]' =>  array('checked' => TRUE),
                ),
              ),
            );
          }

        }
      }

    $form['layout'] = array(
      '#type' => 'fieldset',
      '#title' => t('Layout and Display'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['layout']['autowidth'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable auto-width calculation.'),
      '#default_value' => !empty($this->options['layout']['autowidth']),
      '#description' => t('Enable or disable automatic column width calculation. This can be disabled as an optimisation (it takes some time to calculate the widths).'),
    );

    $form['layout']['themeroller'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable jQuery UI ThemeRoller Support'),
      '#default_value' => !empty($this->options['layout']['themeroller']),
      '#description' => t("Create markup and classes to support jQuery UI's ThemeRoller"),
    );

    $form['layout']['dom'] = array(
      '#type' => 'textfield',
      '#title' => t('Set DOM Initialization Parameter'),
      '#default_value' => $this->options['layout']['dom'],
      '#description' => t("Use the sDOM parameter to rearrange the interface elements. See the <a href='@sdom_documentation_url'>Datatables sDOM documentation</a> for details on how to use this feature", array('@sdom_documentation_url' => 'http://www.datatables.net/examples/basic_init/dom.html')),
    );

    $form['lang'] = array(
      '#type' => 'fieldset',
      '#title' => t('Language'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['lang']['language'] = array(
       '#type' => 'select',
       '#title' => t('Language'),
       '#default_value' => isset($this->options['lang']['language']) ? $this->options['lang']['language'] : 0,
       '#options' => array(
              0 => t('English'),
            'Afrikaans'	=>	t('Afrikaans'),
            'Albanian'	=>	t('Albanian'),
            'Arabic'	=>	t('Arabic'),
            'Azerbaijan'	=>	t('Azerbaijan'),
            'Bangla'	=>	t('Bangla'),
            'Bulgarian'	=>	t('Bulgarian'),
            'Catalan'	=>	t('Catalan'),
            'Chinese-traditional'	=>	t('Chinese traditional'),
            'Chinese'	=>	t('Chinese'),
            'Croatian'	=>	t('Croatian'),
            'Czech'	=>	t('Czech'),
            'Danish'	=>	t('Danish'),
            'Dutch'	=>	t('Dutch'),
            'Estonian'	=>	t('Estonian'),
            'Filipino'	=>	t('Filipino'),
            'Finnish'	=>	t('Finnish'),
            'French'	=>	t('French'),
            'Galician'	=>	t('Galician'),
            'Georgian'	=>	t('Georgian'),
            'German'	=>	t('German'),
            'Greek'	=>	t('Greek'),
            'Gujarati'	=>	t('Gujarati'),
            'Hebrew'	=>	t('Hebrew'),
            'Hindi'	=>	t('Hindi'),
            'Hungarian'	=>	t('Hungarian'),
            'Icelandic'	=>	t('Icelandic'),
            'Indonesian-Alternative' => t('Indonesian Alternative'),
            'Indonesian'	=>	t('Indonesian'),
            'Irish'	=>	t('Irish'),
            'Italian'	=>	t('Italian'),
            'Japanese'	=>	t('Japanese'),
            'Korean'	=>	t('Korean'),
            'Latvian'	=>	t('Latvian'),
            'Lithuanian'	=>	t('Lithuanian'),
            'Macedonian'	=>	t('Macedonian'),
            'Malay'	=>	t('Malay'),
            'Norwegian'	=>	t('Norwegian'),
            'Persian'	=>	t('Persian'),
            'Polish'	=>	t('Polish'),
            'Portuguese-Brasil'	=> t('Portuguese Brasil'),
            'Portuguese'	=>	t('Portuguese'),
            'Romanian'	=>	t('Romanian'),
            'Russian'	=>	t('Russian'),
            'Serbian'	=>	t('Serbian'),
            'Slovak'	=>	t('Slovak'),
            'Slovenian'	=>	t('Slovenian'	),
            'Spanish'	=>	t('Spanish'	),
            'Swahili'	=>	t('Swahili'	),
            'Swedish'	=>	t('Swedish'	),
            'Tamil'	=>	t('Tamil'),
            'Thai'	=>	t('Thai'),
            'Turkish'	=>	t('Turkish'),
            'Ukranian'	=>	t('Ukranian'),
            'Urdu'	=>	t('Urdu'),
            'Uzbek'	=>	t('Uzbek'),
            'Vietnamese'	=>	t('Vietnamese'),
        ),
        '#description' => t('Selects which Language style should be used.'),
     );

    $form['pages'] = array(
      '#type' => 'fieldset',
      '#title' => t('Pagination and Page Length'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['pages']['pagination_style'] = array(
      '#type' => 'select',
      '#title' => t('Pagination Style'),
      '#default_value' => isset($this->options['pages']['pagination_style']) ? $this->options['pages']['pagination_style'] : 0,
      '#options' => array(
        0 => t('Two-Button (Default)'),
        'full_numbers' => t('Full Numbers'),
        'full' => t('Full'),
        'simple_numbers' => t('Simple & numbers'),
        'simple' => t('Simple'),
        'numbers' => t('Numbers'),
        'first_last_numbers' => t('First & Last buttons'),
        'no_pagination' => t('No Pagination'),
      ),
      '#description' => t('Selects which pagination style should be used.'),
    );

    $form['pages']['lengthChange'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Length Change Selection Box'),
      '#default_value' => !empty($this->options['pages']['lengthChange']),
      '#description' => t('Enable or page length selection menu'),
    );
	$form['elements']['multi_filter'] = array(
	  '#type' => 'checkbox',
	  '#title' => t('Enable multi filter.'),
	  '#default_value' => !empty($this->options['elements']['multi_filter']),
	  '#description' => 'The multi filter boxes allow users to dynamically filter each column in the table.  Disabling this option will hide the multi filter boxes from users.',
	);
    $form['elements']['multi_filter_choice'] = array(
      '#type' => 'radios',
      '#title' => t('Header of Footer'),
      '#default_value' => !empty($this->options['elements']['multi_filter_choice'])?$this->options['elements']['multi_filter_choice']:'footer',
      '#options' => array(
        'footer'=>t('Footer'),
        'header'=>t('Header'),
      )
    );

    $form['pages']['display_length'] = array(
      '#type' => 'textfield',
      '#title' => t('Default Page Length'),
      '#description' => t('Default number of records to show per page. May be adjusted by users if Length Selection is enabled'),
      '#default_value' => isset($this->options['pages']['display_length']) ? $this->options['pages']['display_length'] : 20,
      '#element_validate' => array('element_validate_integer'),
    );

    $columns = $this->sanitize_columns($this->options['columns']);
    foreach ($columns as $column_name => $column_label) {
      $form['hidden_columns'][$column_name] = array(
        '#type' => 'select',
        //'#title' => check_plain($column_label),
        '#default_value' => isset($this->options['hidden_columns'][$column_name]) ? $this->options['hidden_columns'][$column_name] : 0,
        '#options' => array(
          0 => 'Visible',
          'hidden' => 'Hidden',
          'expandable' => 'Hidden and Expandable',
        ),
      );

      //show button option columns visibilyty https://datatables.net/extensions/buttons/examples/styling/bootstrap.html
      $safe = str_replace(array('][', '_', ' '), '-', $column_name);
      // The $id of the column for dependency checking.
      //@toto customize option coloumns visibility in researche
      $id = 'edit-style-options-columns-' . $safe;
      $option_default = (!empty($this->options['info'][$column_name]['colvis'])) ? $this->options['info'][$column_name]['colvis'] : $option_default = 'presist';
      $form['info'][$column_name]['colvis'] = array(
        '#type' => 'select',
        '#options' => array(
          'persist' => t('Always Show'),
          'essential' => t('Shown with Option to Hide'),
          'optional-always' => t('Hide with Option to Show'),
        ),
        '#default_value' => $option_default,
        '#dependency' => array($id => array($column_name)),
      );

      //option multi filter
      $form['info'][$column_name]['multi_filter'] = array(
        '#type' => 'select',
        //'#title' => check_plain($column_label),
        '#default_value' => isset($this->options['info'][$column_name]['multi_filter']) ? $this->options['info'][$column_name]['multi_filter'] : 0,
        '#options' => array(
          0 => 'Visible',
          'hidden' => 'Hidden',
        ),
      );
    }

    //support ajax or server-side
    $form['ajax'] = array(
      '#type' => 'fieldset',
      '#title' => t('Ajax'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['ajax']['url'] = array(
            '#type' => 'textfield',
            '#title' => t('URL source'),
            '#default_value' => !empty($this->options['ajax']['url'])?$this->options['ajax']['url']:'',
            '#description' => t('Use <b>/'.DATATABLES_JSON_PATH.'/'.$this->view->name.'/'.$this->display->id.'</b> to generate data json or customize your output json (server Side) <a target="_blank" href="https://datatables.net/examples/ajax/simple.html">ajax</a>'),
    );
    if(module_exists('search_api') && module_exists('elasticsearch_connector')){

      $this->view->build();
      if(!empty($this->view->query) && $this->view->query instanceof SearchApiViewsQuery){
        $index = $this->view->query->getSearchApiQuery()->getIndex();
      }
      $type_name = !empty($index->machine_name)?$index->machine_name:'';
      $index_name = !empty($index->options['index_name']['index'])?$index->options['index_name']['index']:'';
      $form['ajax']['elasticsearch'] = array(
        '#type' => 'checkbox',
        '#title' => t('Support Elasticsearch'),
        '#default_value' => !empty($this->options['ajax']['elasticsearch']),
        '#description' => t('Download : <a target="_blank" href="https://github.com/pidupuis/elastic-datatables">elastic-datatables jquery</a> to libraies , URL source below looks like '.$_SERVER['HTTP_HOST'].':9200'),
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[ajax][url]"]' =>  array('filled' => TRUE),
          ),
        ),
      );
      //@todo index and type can be fill automatic by views
      $form['ajax']['elasticsearch_index'] = array(
        '#type' => 'textfield',
        '#title' => t('Index'),
        '#default_value' => !empty($this->options['ajax']['elasticsearch_index'])?$this->options['ajax']['elasticsearch_index']:$index_name,
        '#description' => t('Elasticsearch index: '.$index_name),
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[ajax][elasticsearch]"]' =>  array('checked' => TRUE),
          ),
        ),
      );
      $form['ajax']['elasticsearch_type'] = array(
        '#type' => 'textfield',
        '#title' => t('Type'),
        '#default_value' => !empty($this->options['ajax']['elasticsearch_type'])?$this->options['ajax']['elasticsearch_type']:$type_name,
        '#description' => t('Elasticsearch type: '.$type_name),
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[ajax][elasticsearch]"]' =>  array('checked' => TRUE),
          ),
        ),
      );
    }

    $form['ajax']['columns'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ajax data source (objects)'),
      '#default_value' => !empty($this->options['ajax']['columns']),
      '#description' => t('DataTables use objects as the data source for each row, exemple: <a target="_blank" href="https://datatables.net/examples/ajax/objects.html">Ajax data source (objects)</a>'),
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[ajax][url]"]' =>  array('filled' => TRUE),
        ),
      ),
    );

    $form['ajax']['deferRender'] = array(
      '#type' => 'checkbox',
      '#title' => t('Deferred rendering for speed'),
      '#default_value' => !empty($this->options['ajax']['deferRender']),
      '#description' => t('When working with large data sources, you might seek to improve the speed at which DataTables runs exemple: <a target="_blank" href="https://datatables.net/examples/ajax/defer_render.html">Deferred rendering for speed</a>'),
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[ajax][url]"]' =>  array('filled' => TRUE),
        ),
      ),
    );

    $form['ajax']['deferLoading'] = array(
      '#type' => 'textfield',
      '#title' => t('Deferred loading of data'),
      '#default_value' => !empty($this->options['ajax']['deferLoading'])?$this->options['ajax']['deferLoading']:'',
      '#description' => t('Numbers of data is available and that it should wait for under interaction before ajax call exemple: <a target="_blank" href="https://datatables.net/examples/server_side/defer_loading.html">Deferred loading of data</a>'),
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[ajax][url]"]' =>  array('filled' => TRUE),
        ),
      ),
    );
    $form['ajax']['serverSide'] = array(
      '#type' => 'checkbox',
      '#title' => t('Server-side processing'),
      '#default_value' => !empty($this->options['ajax']['serverSide']),
      '#description' => t(' if you are working with seriously large databases, you might want to consider using the server-side options that DataTables provides exemple: <a target="_blank" href="https://datatables.net/examples/server_side/simple.html">Server-side processing</a>'),
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[ajax][url]"]' =>  array('filled' => TRUE),
        ),
      ),
    );

    $form['ajax']['processing'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use processing'),
      '#default_value' => !empty($this->options['ajax']['processing']),
      '#description' => t('For ajax config exemple: <a target="_blank" href="https://datatables.net/examples/ajax/deep.html">Nested object data</a>'),
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[ajax][url]"]' =>  array('filled' => TRUE),
        ),
      ),
    );

    $form['ajax']['type'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use POST for Server-side processing'),
      '#default_value' => !empty($this->options['ajax']['type']),
      '#description' => t('By default DataTables use HTTP GET request. However, there are times when you might wish to use POST: <a target="_blank" href="https://datatables.net/examples/server_side/post.html">POST data</a>'),
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[ajax][serverSide]"]' => array('checked' => TRUE),
        ),
      ),
    );
  }
}

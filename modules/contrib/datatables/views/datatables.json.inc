<?php

/**
 * @file
 * Get JSON data to populate datatables.
 */

/**
 * Build Json Data.
 */
function datatables_json($view_name = NULL, $view_display = NULL)
{

  // Bail out if no view_name or view_display is passed.
  if (empty($view_name) && empty($view_display)) {
    return;
  }

  // Get the view and check access.
  $view = views_get_view($view_name);

  if ($view && $view->access($view_display)) {
    $view->set_display($view_display);
    $view->display_handler->set_option('datatables_ajax', TRUE);
    $items_per_page = $view->get_items_per_page();
    $pager = $view->display_handler->options['pager'];
    //limit items
    $limit = 10000;
    $view->set_items_per_page($limit);
    if ($items_per_page)
      $view->set_offset($items_per_page);
    //change type full to some because search api views only support 10 items
    $pager['type'] = 'some';
    $view->display_handler->set_option('pager', $pager);
    $view->preview($view_display);

    $options = $view->style_plugin->options;
    $handler = $view->style_plugin;
    $fields = &$view->field;
    $columns = $handler->sanitize_columns($options['columns'], $fields);
    $field_col = array_values($columns);
    $column_index = array_flip($field_col);
    $renders = $handler->render_fields($view->result);
    $data = [];
    // Render each field into its appropriate column.
    foreach ($columns as $field => $column) {
      foreach ($view->result as $num => $row) {
        if (!empty($fields[$field]) && empty($fields[$field]->options['exclude'])) {
          $field_output = $renders[$num][$field];
          $element_type = $fields[$field]->element_type(TRUE, TRUE);
          if ($element_type) {
            $field_output = '<' . $element_type . '>' . $field_output . '</' . $element_type . '>';
          }

          // Don't bother with separators and stuff if the field does not show up.
          if (empty($field_output) && !empty($data[$num][$column_index[$column]])) {
            continue;
          }

          // Place the field into the column, along with an optional separator.
          if (!empty($data[$num][$column_index[$column]])) {
            if (!empty($options['info'][$column]['separator'])) {
              $data[$num][$column_index[$column]] .= filter_xss_admin($options['info'][$column]['separator']);
            }
          } else {
            $data[$num][$column_index[$column]] = '';
          }
          $data[$num][$column_index[$column]] .= $field_output;
        }
      }
    }
    drupal_json_output(['data' => $data]);
    drupal_exit();
  }
}

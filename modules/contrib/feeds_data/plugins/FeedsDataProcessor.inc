<?php

/**
 * @file
 * Contains FeedsDataProcessor.
 */

// Update modes for existing items.
define('FEEDS_DATA_SKIP_EXISTING', 0);
define('FEEDS_DATA_REPLACE_EXISTING', 1);
define('FEEDS_DATA_UPDATE_EXISTING', 2);

// Options for handling content in Drupal but not in source data.
define('FEEDS_DATA_SKIP_NON_EXISTENT', 'skip');
define('FEEDS_DATA_DELETE_NON_EXISTENT', 'delete');

/**
 * Creates and updates a data table from feed items.
 *
 * We cannot extend abstract class FeedsProcessor because it assumes the
 * target is an entity which may not be true for generic Data tables.
 */
class FeedsDataProcessor extends FeedsPlugin {

  /**
   * {@inheritdoc}
   */
  public function pluginType() {
    return 'processor';
  }

  /*
   * Report number of items that can be processed per call.
   *
   * 0 means 'unlimited'.
   *
   * If a number other than 0 is given, Feeds parsers that support batching
   * will only deliver this limit to the processor.
   *
   * @see FeedsSource::getLimit()
   * @see FeedsCSVParser::parse()
   */
  public function getLimit() {
    return variable_get('feeds_process_limit', 50);
  }

  /**
   * Get the Data table for this processor.
   *
   * @return DataTable
   *   The DataTable object if found, otherwise FALSE.
   */
  protected function getDataTable() {
    $tables = &drupal_static(__METHOD__, array());
    if (!isset($tables[$this->id])) {
      $tables[$this->id] = !empty($this->config['data_table']) ? data_get_table($this->config['data_table']) : FALSE;
    }
    return $tables[$this->id];
  }

  /**
   * Get the primary key columns for the Data table.
   *
   * @return array
   *   An array of primary key columns for the Data table if fount,
   *   otherwise FALSE.
   */
  protected function getPrimaryKeys() {
    $keys = &drupal_static(__METHOD__, array());
    if (!isset($keys[$this->id])) {
      $table = $this->getDataTable();
      $keys[$this->id] = !empty($table) ? $table->table_schema['primary key'] : FALSE;
    }
    return $keys[$this->id];
  }

  /**
   * Process the result of the parsing stage.
   *
   * @param FeedsSource $source
   *   Source information about this import.
   * @param FeedsParserResult $parser_result
   *   The result of the parsing stage.
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    $table = $this->getDataTable();
    $primary_keys = $this->getPrimaryKeys();

    $state = $source->state(FEEDS_PROCESS);
    if (!isset($state->removeList) && $parser_result->items) {
      $this->initRowsToBeRemoved($source, $state);
    }

    while ($item = $parser_result->shiftItem()) {
      // Check if this item already exists. If it is still included in the feed,
      // it must not be removed on clean.
      $keys = $this->existingRowKeys($source, $parser_result);
      if (!empty($keys)) {
        unset($state->removeList[$keys]);
      }

      // If it exists, and we are not updating, continue on to the next item.
      $skip_existing = $this->config['update_existing'] == FEEDS_DATA_SKIP_EXISTING;
      if (!empty($keys) && $skip_existing) {
        continue;
      }

      try {
        $hash = $this->hash($item);
        $changed = ($hash !== $this->getHash($keys));
        $force_update = $this->config['skip_hash_check'];

        // Do not proceed if the item exists, has not changed, and we're not
        // forcing the update.
        if (!empty($keys) && !$changed && !$force_update) {
          continue;
        }

        if (!empty($keys)) {
          // Load an existing entity.
          $keys_array = array_combine($primary_keys, explode('|', $keys));
          $row = $this->rowLoad($source, $keys_array);

          // The feeds_data_item table is always updated with the info for the
          // most recently processed row.
          $this->newItemInfo($row, $source->feed_nid, $hash);
          $row->feeds_data_item->is_new = FALSE;
        }
        else {
          // Build a new row.
          $row = $this->newRow($source);
          $this->newItemInfo($row, $source->feed_nid, $hash);
        }

        // Set property and field values.
        $this->map($source, $parser_result, $row);

        // Allow modules to alter the row before saving.
        module_invoke_all('feeds_data_presave', $source, $row, $item, $keys);
        if (module_exists('rules')) {
          rules_invoke_event('feeds_import_'. $source->importer()->id, $row);
        }

        // Enable modules to skip saving at all.
        if (!empty($row->feeds_data_item->skip)) {
          continue;
        }

        // This will throw an exception on failure.
        $this->rowSave($row);

        // Save the data feed item info.
        foreach ($primary_keys as $key) {
          $save_keys[$key] = $row->{$key};
        }
        feeds_data_item_info_save($row, implode('|', $save_keys));

        // Track progress.
        if (empty($keys)) {
          $state->created++;
        }
        else {
          $state->updated++;
        }
      }
      catch (Exception $e) {
        // Something bad happened, log it.
        $state->failed++;
        drupal_set_message($e->getMessage(), 'warning');
        $message = $this->createLogMessage($e, $row, $item);
        $source->log('import', $message, array(), WATCHDOG_ERROR);
      }
    }

    // Set messages if we're done.
    if ($source->progressImporting() != FEEDS_BATCH_COMPLETE) {
      return;
    }

    // Remove not included items if needed.
    // It depends on the implementation of the clean() method what will happen
    // to items that were no longer in the source.
    $this->clean($state);
    $messages = array();
    if ($state->created) {
      $messages[] = array(
        'message' => format_plural($state->created, 'Created @number row.', 'Created @number rows.', array('@number' => $state->created)),
      );
    }
    if ($state->updated) {
      $messages[] = array(
        'message' => format_plural($state->updated, 'Updated @number row.', 'Updated @number rows.', array('@number' => $state->updated)),
      );
    }
    if ($state->deleted) {
      $messages[] = array(
        'message' => format_plural($state->deleted, 'Removed @number row.', 'Removed @number rows.', array('@number' => $state->deleted)),
      );
    }
    if ($state->failed) {
      $messages[] = array(
        'message' => format_plural($state->failed, 'Failed importing @number row.', 'Failed importing @number rows.', array('@number' => $state->failed)),
        'level' => WATCHDOG_ERROR,
      );
    }
    if (empty($messages)) {
      $messages[] = array(
        'message' => t('There are no new rows.'),
      );
    }
    foreach ($messages as $message) {
      drupal_set_message($message['message']);
      $source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
    }
  }

  /**
   * Initialize the list of rows to remove from all previously imported rows.
   *
   * @param FeedsSource $source
   *   Source information about this import.
   * @param FeedsState $state
   *   The FeedsState object for the given stage.
   */
  protected function initRowsToBeRemoved(FeedsSource $source, FeedsState $state) {
    $state->removeList = array();
    // We fill it only if needed.
    if (!isset($this->config['update_non_existent']) || $this->config['update_non_existent'] == FEEDS_DATA_SKIP_NON_EXISTENT) {
      return;
    }

    $table = $this->getDataTable();
    $primary_keys = $this->getPrimaryKeys();

    // Build base select statement.
    $select = db_select($this->config['data_table'], 'dt');
    foreach ($primary_keys as $i => $key) {
      $select->addField('dt', $key);
      $primary_keys[$i] = 'dt.' . $key;
    }

    // Join the feeds_data_item table.
    $keys_sql = 'CONCAT(' . implode(", '|', ", $primary_keys) . ')';
    $select->join('feeds_data_item', 'dfi', "dfi.data_keys = $keys_sql AND dfi.data_table = :data_table", array(
      ':data_table' => $table->name,
    ));

    $select->condition('dfi.id', $this->id);
    $select->condition('dfi.feed_nid', $source->feed_nid);
    $results = $select->execute();

    // If not found during process, existing rows will be deleted.
    foreach ($results as $result) {
      $result_array = (array) $result;
      $state->removeList[implode('|', $result_array)] = $result_array;
    }
  }

  /**
   * Retrieve the target row's existing keys if available.
   *
   * @param FeedsSource $source
   *   The feeds source that spawns this row.
   * @param FeedsParserResult $result
   *   A FeedsParserResult object.
   * @return string
   *   The list of keys separated by '|' for the row if found, FALSE otherwise.
   */
  protected function existingRowKeys(FeedsSource $source, FeedsParserResult $result) {
    $table = $this->getDataTable();
    $primary_keys = $this->getPrimaryKeys();

    $parser = feeds_importer($this->id)->parser;
    $values = array();
    foreach ($this->config['mappings'] as $mapping) {
      if (in_array($mapping['target'], $primary_keys)) {
        // Invoke the parser's getSourceElement to retrieve the value for this
        // mapping's source.
        $values[$mapping['target']] = $parser->getSourceElement($source, $result, $mapping['source']);
      }
    }

    $select = db_select($table->name, 'dt');
    $select->addExpression('CONCAT(' . implode(", '|', ", $primary_keys) . ')', 'concat_keys');
    foreach ($values as $key => $value) {
      $select->condition($key, $value);
    }

    if ($keys = $select->execute()->fetchField()) {
      return $keys;
    }
    return '';
  }

  /**
   * Get mappings.
   */
  public function getMappings() {
    return isset($this->config['mappings']) ? $this->config['mappings'] : array();
  }

  /**
   * Get a list of possible mapping targets for this processor.
   *
   * @ingroup mappingapi
   *
   * @return array
   *   An array of mapping targets.
   */
  public function getMappingTargets() {
    $table = $this->getDataTable();
    if (!$table) {
      $url = url('admin/structure/feeds/' . $this->id . '/settings/' . $plugin_key);
      drupal_set_message(t('Please <a href="@url">select a data table</a>.', array('@url' => $url)), 'warning', FALSE);
      return array();
    }

    $targets = array();
    foreach ($table->table_schema['fields'] as $field_name => $field) {
      $primary_key = in_array($field_name, $table->table_schema['primary key']);
      $targets[$field_name] = array(
        'name' => !empty($table->meta['fields'][$field_name]['label']) ? $table->meta['fields'][$field_name]['label'] : $field_name,
        'description' => $field['description'] . ($primary_key ? ' (Primary key)' : ''),
        'primary_key' => $primary_key,
      );
    }

    return $targets;
  }

  /**
   * Execute mapping on an item.
   *
   * This method encapsulates the central mapping functionality. When an item is
   * processed, it is passed through map() where the properties of $source_item
   * are mapped onto $target_item following the processor's mapping
   * configuration.
   *
   * For each mapping FeedsParser::getSourceElement() is executed to retrieve
   * the source element, then the target item value is populated. Alternatively
   * a hook_x_targets_alter() may have specified a callback for a mapping target
   * in which case the callback is asked to populate the target item.
   *
   * @ingroup mappingapi
   *
   * @param FeedsSource $source
   *   The feeds source that spawns this row.
   * @param FeedsParserResult $result
   *   A FeedsParserResult object
   * @param object $target_item
   *   The target item to populate.
   *
   * @see hook_feeds_parser_sources_alter()
   * @see hook_feeds_processor_targets_alter()
   */
  protected function map(FeedsSource $source, FeedsParserResult $result, $target_item = NULL) {
    // Static cache $targets as getMappingTargets() may be an expensive method.
    $sources = &drupal_static(__METHOD__ . '::sources', array());
    if (!isset($sources[$this->id])) {
      $sources[$this->id] = feeds_importer($this->id)->parser->getMappingSources();
    }

    $targets = &drupal_static(__METHOD__ . '::targets', array());
    if (!isset($targets[$this->id])) {
      $targets[$this->id] = $this->getMappingTargets();
    }

    $parser = feeds_importer($this->id)->parser;
    if (empty($target_item)) {
      $target_item = array();
    }

    // Many mappers add to existing fields rather than replacing them. Hence we
    // need to clear target elements of each item before mapping in case we are
    // mapping on a prepopulated item such as an existing node.
    foreach ($this->config['mappings'] as $mapping) {
      if (isset($targets[$this->id][$mapping['target']]['real_target'])) {
        $target_item->{$targets[$this->id][$mapping['target']]['real_target']} = NULL;
      }
      else {
        $target_item->{$mapping['target']} = NULL;
      }
    }

    // This is where the actual mapping happens: For every mapping we envoke
    // the parser's getSourceElement() method to retrieve the value of the source
    // element and set it on the target item.
    //
    // Use the mapping callback if specified.
    self::loadMappers();
    foreach ($this->config['mappings'] as $mapping) {
      // Retrieve source element's value from parser.
      if (isset($sources[$this->id][$mapping['source']]) &&
          is_array($sources[$this->id][$mapping['source']]) &&
          isset($sources[$this->id][$mapping['source']]['callback']) &&
          function_exists($sources[$this->id][$mapping['source']]['callback'])) {
        $callback = $sources[$this->id][$mapping['source']]['callback'];
        $value = $callback($source, $result, $mapping['source']);
      }
      else {
        $value = $parser->getSourceElement($source, $result, $mapping['source']);
      }

      // Map the source element's value to the target.
      if (isset($targets[$this->id][$mapping['target']]) &&
          is_array($targets[$this->id][$mapping['target']]) &&
          isset($targets[$this->id][$mapping['target']]['callback']) &&
          function_exists($targets[$this->id][$mapping['target']]['callback'])) {
        $callback = $targets[$this->id][$mapping['target']]['callback'];

        // All target callbacks expect an array.
        if (!is_array($value)) {
          $value = array($value);
        }

        $callback($source, $target_item, $mapping['target'], $value, $mapping);
      }
      else {
        $target_item->{$mapping['target']} = $value;
      }
    }

    return $target_item;
  }

  /**
   * Deletes rows which were not found during processing.
   *
   * @param FeedsState $state
   *   The FeedsState object for the given stage.
   */
  protected function clean(FeedsState $state) {
    if (!isset($this->config['update_non_existent']) || $this->config['update_non_existent'] == feeds_data_SKIP_NON_EXISTENT) {
      return;
    }

    $table = $this->getDataTable();
    foreach ($state->removeList as $keys) {
      $this->rowDelete($keys);
      feeds_data_item_info_delete($table->name, implode('|', $keys));
      $state->deleted ++;
    }
  }

  /**
   * Adds Data Feeds specific information on $row->feeds_data_item.
   *
   * @param $row
   *   The row object to be populated with new item info.
   * @param $feed_nid
   *   The feed nid of the source that produces this row.
   * @param $hash
   *   The fingerprint of the source item.
   */
  protected function newItemInfo($row, $feed_nid, $hash = '') {
    $table = $this->getDataTable();
    $row->feeds_data_item = (object) array(
      'is_new' => TRUE,
      'data_keys' => '',
      'data_table' => $table->name,
      'id' => $this->id,
      'feed_nid' => $feed_nid,
      'imported' => REQUEST_TIME,
      'hash' => $hash,
    );
  }

  /**
   * Loads existing row information and places it on $row->feeds_item.
   *
   * @param object $row
   *   The row object to load item info for.
   * @return boolean
   *   TRUE if item info could be loaded, FALSE if not.
   */
  protected function loadItemInfo($row) {
    $table = $this->getDataTable();
    $primary_keys = $this->getPrimaryKeys();

    foreach ($primary_keys as $key) {
      if (isset($row->{$key})) {
        $keys[] = $row->{$key};
      }
    }

    if ($item_info = feeds_data_item_info_load($table->name, implode('|', $keys))) {
      $row->feeds_data_item = $item_info;
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Creates a new row object in memory and returns it.
   *
   * @param FeedsSource $source
   *   The feeds source that spawns this row.
   * @return
   *   A new row object.
   */
  protected function newRow(FeedsSource $source) {
    $primary_keys = $this->getPrimaryKeys();
    $row = new stdClass();
    foreach ($primary_keys as $key) {
      // Add Make sure primary keys are populated.
      $row->{$key} = '';
    }
    return $row;
  }

  /**
   * Loads an existing row.
   *
   * @param FeedsSource $source
   *   The feeds source that spawns this row.
   * @param array $keys
   *   An associative array of keys for row that should be loaded.
   *
   * @return
   *   An existing row object.
   */
  protected function rowLoad(FeedsSource $source, $keys) {
    $table = $this->getDataTable();
    $select = db_select($table->name, 'dt')->fields('dt');
    foreach ($keys as $key => $value) {
      $select->condition($key, $value);
    }
    return $select->execute()->fetchObject();
  }

  /**
   * Delete a row.
   *
   * @param array $keys
   *   The row keys to delete..
   */
  protected function rowDelete($keys) {
    $table = $this->getDataTable();
    $primary_keys = $this->getPrimaryKeys();

    $delete = db_delete($table->name);
    foreach ($primary_keys as $key) {
      if (!isset($keys[$key])) {
        // Don't delete anything if we don't have all the necessary keys.
        return FALSE;
      }
      $delete->condition($key, $keys[$key]);
    }
    $delete->execute();
  }

  /**
   * Save a row.
   *
   * @param object $row
   *   The row object to save.
   *
   * @throws DataFeedsException
   */
  public function rowSave($row) {
    $table = $this->getDataTable();
    $primary_keys = (!$row->feeds_data_item->is_new) ? $this->getPrimaryKeys() : array();
    $status = drupal_write_record($table->name, $row, $primary_keys);
    if ($status == FALSE) {
      throw new FeedsDataException(t('Could not write to the %table table.', array('%table' => $table->name)));
    }
  }

  /**
   * Declare default configuration.
   */
  public function configDefaults() {
    return array(
      'mappings' => array(),
      'data_table' => NULL,
      'update_existing' => FEEDS_DATA_REPLACE_EXISTING,
      'update_non_existent' => FEEDS_DATA_SKIP_NON_EXISTENT,
      'skip_hash_check' => FALSE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function configForm(&$form_state) {
    $form = array();

    $table_options = array();
    $tables = data_get_all_tables();
    foreach ($tables as $table) {
      $table_options[$table->name] = check_plain($table->title);
    }

    $form['data_table'] = array(
      '#type' => 'select',
      '#title' => t('Data Table'),
      '#description' => t('Select the Data table to import rows.'),
      '#required' => TRUE,
      '#options' => $table_options,
      '#default_value' => $this->config['data_table'],
    );

    $form['update_existing'] = array(
      '#type' => 'radios',
      '#title' => t('Update existing rows'),
      '#description' => t('Existing rows will be determined using mappings that are a "unique target".'),
      '#options' => array(
        FEEDS_DATA_SKIP_EXISTING => t('Do not update existing rows'),
        FEEDS_DATA_REPLACE_EXISTING => t('Replace existing rows'),
        FEEDS_DATA_UPDATE_EXISTING => t('Update existing rows'),
      ),
      '#default_value' => $this->config['update_existing'],
    );

    $form['skip_hash_check'] = array(
      '#type' => 'checkbox',
      '#title' => t('Skip hash check'),
      '#description' => t('Force update of items even if item source data did not change.'),
      '#default_value' => $this->config['skip_hash_check'],
    );

    $form['update_non_existent'] = array(
      '#type' => 'radios',
      '#title' => t('Action to take when previously imported rows are missing in the feed'),
      '#description' => t('Select how rows previously imported and now missing in the feed should be updated.'),
      '#options' => array(
        FEEDS_DATA_SKIP_NON_EXISTENT => t('Skip non-existent rows'),
        FEEDS_DATA_DELETE_NON_EXISTENT => t('Delete non-existent rows'),
      ),
      '#default_value' => $this->config['update_non_existent'],
    );

    return $form;
  }

  /**
   * Counts the number of items imported by this processor.
   */
  public function itemCount(FeedsSource $source) {
    return db_query("SELECT count(*) FROM {feeds_data_item} WHERE id = :id AND feed_nid = :feed_nid", array(':id' => $this->id, ':feed_nid' => $source->feed_nid))->fetchField();
  }

  /**
   * Create MD5 hash of item and mappings array.
   *
   * Include mappings as a change in mappings may have an affect on the item
   * produced.
   *
   * @return Always returns a hash, even with empty, NULL, FALSE:
   *  Empty arrays return 40cd750bba9870f18aada2478b24840a
   *  Empty/NULL/FALSE strings return d41d8cd98f00b204e9800998ecf8427e
   */
  protected function hash($item) {
    return hash('md5', serialize($item) . serialize($this->config['mappings']));
  }

  /**
   * Retrieves the MD5 hash of a row from the database.
   *
   * @param string $keys
   *   An list of row keys separated by '|'.
   *
   * @return string
   *   Empty string if no item is found, hash otherwise.
   */
  protected function getHash($keys) {
    $table = $this->getDataTable();
    $query = db_query("SELECT hash FROM {feeds_data_item} WHERE data_table = :data_table AND data_keys = :keys", array(
      ':data_table' => $table->name,
      ':keys' => $keys,
    ));
    if ($hash = $query->fetchField()) {
      return $hash;
    }
    return '';
  }

  /**
   * Creates a log message for when an exception occured during import.
   *
   * @param Exception $e
   *   The exception that was throwned during processing the item.
   * @param $row
   *   The row object.
   * @param $item
   *   The parser result for this row.
   *
   * @return string
   *   The message to log.
   */
  protected function createLogMessage(Exception $e, $row, $item) {
    include_once DRUPAL_ROOT . '/includes/utility.inc';
    $message = $e->getMessage();
    $message .= '<h3>Original item</h3>';
    $message .= '<pre>' . drupal_var_export($item). '</pre>';
    $message .= '<h3>Row</h3>';
    $message .= '<pre>' . drupal_var_export($row) . '</pre>';
    return $message;
  }

  /**
   * We don't support expiry.
   */
  public function expiryTime() {
    return FEEDS_EXPIRE_NEVER;
  }
}

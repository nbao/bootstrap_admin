<?php
/**
 * @file
 * Displays a Bootstrap-style field horizontal
 *
 * Available variables:
 * - $is_empty: Boolean: any content to render at all?
 * - $classes: The classes set on the group div.
 * - $panels: An array of panels containing content.
 * - $index: The index of the active tab/pane.
 *
 * @ingroup themeable
 */
?>
<?php if (!empty($items)): ?>
  <div id="<?php print $wrapper_id; ?>" class="<?php print $wrapper_classes; ?>">
    <?php
    uasort($items, 'element_sort');
    if (!empty($panel_classes)) {
      echo '<div class="' . $panel_classes . '">';
    }
    if (!empty($title)) {
      echo '<a href="#' . $wrapper_id . '-body" ' . $title_attributes . '>' . $title . '</a>';
      echo '<div  id="' . $wrapper_id . '-body" class="form-horizontal-body panel-body collapse in">';
    }

    foreach ($items as $field_name => $item) {
      if (isset($item['#access']) && $item['#access'] == false) continue;
      if (empty($item)) {
        if (!empty($variables["group"]->format_settings["instance_settings"]["keep_empty_value"])) {
          $field_info = field_info_instance($group->entity_type, $field_name, $group->bundle);
          $item = [
            '#label_title' => $field_info['label'],
            'element' => ['#title' => $field_info['label'], '#label_attributes' => ['class' => [$label_classes]]],
            '#field_name' => $field_name,
            '#entity_type' => $group->entity_type,
            '#bundle' => $group->bundle,
            '#view_mode' => $group->mode
          ];
        } else {
          continue;
        }
      }
      $fields_ds_settings = [];
      if (module_exists('ds') && !empty($item['#entity_type']) && !empty($item['#view_mode'])) {
        // Add Display Suite fields and settings.
        $ds_settings = ds_get_field_settings($item['#entity_type'], $item['#bundle'], $item['#view_mode']);
        if (!empty($ds_settings[$item['#field_name']])) {
          $fields_ds_settings = $ds_settings[$item['#field_name']];
        }
      }
      $label_title = _hide_label($item, $label_classes, $fields_ds_settings);
      $outer_element = 'div';
      $outer_classes = $row_classes;
      $outer_attr = '';
      if (!empty($fields_ds_settings['formatter_settings']['ft']['ow-el'])) {
        $outer_element = $fields_ds_settings['formatter_settings']['ft']['ow-el'];
      }
      if (!empty($fields_ds_settings['formatter_settings']['ft']['ow-cl'])) {
        $outer_classes .= ' ' . $fields_ds_settings['formatter_settings']['ft']['ow-cl'];
      }
      if (!empty($fields_ds_settings['formatter_settings']['ft']['ow-at'])) {
        $outer_attr = $fields_ds_settings['formatter_settings']['ft']['ow-at'];
      }
      $input_classes_warpper = $input_classes." fieldgroup-horizontal-value";
      if (!empty($item['#type']) && in_array($item['#type'], ['fieldset'])) {
        $childs = element_children($item);

        if (!empty($childs)) {
          $fieldsets = [];
          foreach ($childs as $field_name_group) {
            $fieldset_childs = element_children($item[$field_name_group]);
            if (count($fieldset_childs) >= 2) {
              $fieldset = drupal_render($item[$field_name_group]);
            } else {
              $fieldset = '';
              $fieldset .= "<$outer_element class='$outer_classes $field_name_group' $outer_attr>";
              $fieldset .= _hide_label($item[$field_name_group], $label_classes);
              $fieldset .= "  <div class='$input_classes_warpper'>" . drupal_render($item[$field_name_group]) . "</div>";
              $fieldset .= "</$outer_element>";
            }
            if (!empty($items[$field_name][$field_name_group])) {
              $weight = $items[$field_name][$field_name_group]['#weight'];
            } elseif (!empty($item[$field_name_group])) {
              $weight = $item[$field_name_group]['#weight'];
            }
            $fieldsets[$field_name_group] = [
              '#markup' => $fieldset,
              '#weight' => $weight
            ];
            unset($item[$field_name_group]);
          }
          $item['fieldset_' . $field_name]['#markup'] = drupal_render($fieldsets);
        }
        $label_title = '';
        $input_classes_warpper = "";
      }
      $render = drupal_render($item);
      if (empty($render) && !empty($variables["group"]->format_settings["instance_settings"]["keep_empty_value"])) {
        $render = '<span class="empty-value"></span>';
      }
      if (!empty($render)) {
        if (!empty($item['#field_name'])) $outer_classes .= ' ' . drupal_html_class('field_name_' . $item['#field_name']);
        echo "<$outer_element class='$outer_classes' $outer_attr>";
        echo $label_title;
        echo "  <div class='$input_classes_warpper'>$render</div>";
        echo "</$outer_element>";
      }
    }
    if (!empty($title)) {
      echo '</div>';
    }
    if (!empty($panel_classes)) {
      echo '</div>';
    }
    ?>
  </div>
<?php endif ?>


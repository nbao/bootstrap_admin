<?php

/**
 * A handler for proper "date" fields.
 */
class views_handler_field_data_date extends views_handler_field_date
{

  /**
   * +   * {@inheritdoc}
   * +   */
  function get_value($values, $field = NULL)
  {
    $value = parent::get_value($values, $field);
    if ($field === NULL) {
      $value = strtotime($value);
    }
    return $value;
  }

}
